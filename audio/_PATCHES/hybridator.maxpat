{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 0.0, 45.0, 1305.0, 962.0 ],
		"bgcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 15,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"workspacedisabled" : 1,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 623.0, 1795.0, 69.0, 20.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 1036.0, 883.704346, 50.0, 33.0 ],
					"style" : "",
					"text" : "dry bypass"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1171.55957, 402.537659, 83.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1171.833374, 414.435455, 83.0, 20.0 ],
					"style" : "",
					"text" : "score presets"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-48",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1559.0, 26.666668, 153.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 969.55957, 12.268829, 153.0, 22.0 ],
					"style" : "",
					"text" : "readappend @name audio"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 2,
					"fontsize" : 33.0,
					"id" : "obj-133",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1010.333374, 482.0, 163.0, 43.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 488.0, 12.268829, 163.0, 43.0 ],
					"style" : "",
					"text" : "Hybridator"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 908.547852, 1925.0, 45.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1191.672974, 746.935425, 45.0, 20.0 ],
					"style" : "",
					"text" : "Hybrid"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 686.059631, 1925.0, 66.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1075.55957, 746.935425, 71.0, 20.0 ],
					"style" : "",
					"text" : "Target Dry"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 518.071472, 1925.0, 71.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 974.5, 746.935425, 71.0, 20.0 ],
					"style" : "",
					"text" : "Source Dry"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.541176, 0.815686, 0.913725, 1.0 ],
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 84.0, 129.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"visible" : 1,
						"boxes" : [  ],
						"lines" : [  ]
					}
,
					"patching_rect" : [ 1279.0, 482.0, 100.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1010.833374, 667.120972, 65.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p read me"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1007.0, 1676.666748, 115.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1039.55957, 356.537659, 115.0, 20.0 ],
					"style" : "",
					"text" : "vocoder preset json"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 908.547852, 1951.0, 45.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1237.833374, 780.704346, 45.0, 20.0 ],
					"style" : "",
					"text" : "record"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 696.559631, 1951.0, 45.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1137.05957, 780.704346, 45.0, 20.0 ],
					"style" : "",
					"text" : "record"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 528.571472, 1951.0, 45.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1036.0, 780.704346, 45.0, 20.0 ],
					"style" : "",
					"text" : "record"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 969.0, 2017.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 753.488159, 2018.226318, 36.0, 22.0 ],
					"style" : "",
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 727.559631, 1976.559814, 40.0, 22.0 ],
					"style" : "",
					"text" : "r play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 696.559631, 2018.226318, 50.0, 22.0 ],
					"style" : "",
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 696.559631, 1976.559814, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1137.05957, 806.26416, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 696.559631, 2062.280029, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 753.488159, 2062.280029, 37.0, 22.0 ],
					"style" : "",
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 696.559631, 2129.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "sfrecord~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 585.5, 2018.226318, 36.0, 22.0 ],
					"style" : "",
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 559.571472, 1976.559814, 40.0, 22.0 ],
					"style" : "",
					"text" : "r play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 528.571472, 2018.226318, 50.0, 22.0 ],
					"style" : "",
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 528.571472, 1976.559814, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1036.0, 806.26416, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 528.571472, 2062.280029, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 585.5, 2062.280029, 37.0, 22.0 ],
					"style" : "",
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.8,
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1175.333374, 668.0, 144.0, 25.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 1062.892944, 567.704346, 83.666626, 44.0 ],
					"style" : "",
					"text" : "start/stop playing"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 26.4,
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 883.0, 630.333252, 69.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1062.892944, 502.102081, 69.607056, 36.0 ],
					"style" : "",
					"text" : "reset"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.330747,
					"format" : 6,
					"id" : "obj-58",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 69.0, 1630.071289, 58.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1129.413574, 678.132568, 66.086426, 21.0 ],
					"style" : "",
					"triangle" : 0,
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 69.0, 1571.0, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1111.5, 678.132568, 21.0, 21.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 69.0, 1602.059692, 84.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1111.5, 655.620972, 84.0, 23.0 ],
					"style" : "",
					"text" : "adstatus cpu"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 592.5, 1793.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1036.0, 857.704346, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 630.0, 1834.75, 41.0, 22.0 ],
					"style" : "",
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 516.0, 1834.75, 41.0, 22.0 ],
					"style" : "",
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"channels" : 1,
					"id" : "obj-39",
					"maxclass" : "live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 466.571442, 1975.333496, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 986.0, 780.704346, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[5]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[4]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.564186,
									"id" : "obj-17",
									"linecount" : 31,
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "int", "" ],
									"patching_rect" : [ 50.0, 100.0, 158.0, 444.0 ],
									"style" : "",
									"text" : "mubu.process Source audio slice:fft:sum:scale:onseg @name markers @process 1 @prepad 2000 @slice.size 1024 @slice.hop 256 @slice.norm power @fft.mode power @fft.weighting itur468 @sum.colname Loudness @scale.inmin 1 @scale.inmax 10 @scale.outmin 0 @scale.outmax 10 @scale.func log @scale.base 10 @onseg.filtersize 5 @onseg.duration 1 @onseg.max 1 @onseg.min 1 @onseg.mean 1 @onseg.stddev 1 @onseg.offthresh -120 @onseg.mininter 150 @onseg.threshold 12 @info gui \"interface markers, autobounds 1, paramcols Cue Label Duration\" @progressoutput input"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-35",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-35", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 689.333313, 491.0, 111.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p mubu all process"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 689.333313, 456.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.564186,
									"id" : "obj-34",
									"linecount" : 29,
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 4,
									"outlettype" : [ "", "", "int", "" ],
									"patching_rect" : [ 50.0, 100.0, 161.0, 416.0 ],
									"style" : "",
									"text" : "mubu.process Target audio slice:fft:sum:scale:onseg @name markers @process 1 @prepad 2000 @slice.size 1024 @slice.hop 256 @slice.norm power @fft.mode power @fft.weighting itur468 @sum.colname Loudness @scale.inmin 1 @scale.inmax 10 @scale.outmin 0 @scale.outmax 10 @scale.func log @scale.base 10 @onseg.filtersize 5 @onseg.duration 1 @onseg.max 1 @onseg.min 1 @onseg.mean 1 @onseg.stddev 1 @onseg.offthresh -120 @onseg.mininter 150 @onseg.threshold 4 @info gui \"interface markers, autobounds 1, paramcols Cue Label Duration\" @progressoutput input"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-18",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1921.0, 491.0, 111.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p Mubu all process"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 939.547852, 1975.333496, 40.0, 22.0 ],
					"style" : "",
					"text" : "r play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1204.833374, 694.0, 42.0, 22.0 ],
					"style" : "",
					"text" : "s play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1490.0, 505.833405, 39.0, 22.0 ],
					"style" : "",
					"text" : "* 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1490.0, 536.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "cents = $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 255.0, 505.833405, 39.0, 22.0 ],
					"style" : "",
					"text" : "* 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 255.0, 536.0, 66.0, 22.0 ],
					"style" : "",
					"text" : "cents = $1"
				}

			}
, 			{
				"box" : 				{
					"format" : 5,
					"id" : "obj-106",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1490.0, 468.333405, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 656.55957, 316.268829, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1490.0, 437.000031, 34.0, 22.0 ],
					"style" : "",
					"text" : "+ 60"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1490.0, 407.268829, 100.0, 22.0 ],
					"style" : "",
					"text" : "route bufferindex"
				}

			}
, 			{
				"box" : 				{
					"format" : 5,
					"id" : "obj-84",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 255.0, 468.333405, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.0, 316.268829, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 255.0, 437.000031, 34.0, 22.0 ],
					"style" : "",
					"text" : "+ 60"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 255.0, 407.268829, 100.0, 22.0 ],
					"style" : "",
					"text" : "route bufferindex"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1116.0, 222.499985, 39.0, 22.0 ],
					"style" : "",
					"text" : "types"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-89",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 215.0, 175.666748, 47.0, 22.0 ],
									"style" : "",
									"text" : "curve~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-90",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 215.0, 207.66687, 48.0, 22.0 ],
									"style" : "",
									"text" : "dbtoa~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 50.0, 175.666748, 47.0, 22.0 ],
									"style" : "",
									"text" : "curve~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 215.0, 142.333374, 133.0, 22.0 ],
									"style" : "",
									"text" : "bach.slot2curve @out t"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 142.333374, 133.0, 22.0 ],
									"style" : "",
									"text" : "bach.slot2curve @out t"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 50.0, 207.66687, 48.0, 22.0 ],
									"style" : "",
									"text" : "dbtoa~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 50.0, 100.0, 222.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.playkeys duration (slot 6 7) @out t"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-57",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-58",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 289.66687, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-59",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 215.0, 289.66687, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 1 ],
									"source" : [ "obj-80", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"order" : 1,
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 1 ],
									"source" : [ "obj-80", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-86", 0 ],
									"order" : 0,
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-81", 0 ],
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-90", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 538.0, 1757.666626, 133.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p source-target dry/wet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 528.571472, 2129.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "sfrecord~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 908.547852, 2017.0, 50.0, 22.0 ],
					"style" : "",
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 908.547852, 1975.333496, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1237.833374, 805.037842, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 696.559631, 1676.666748, 106.0, 22.0 ],
					"style" : "",
					"text" : "delay~ 4800 1264"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 466.571442, 1682.0, 106.0, 22.0 ],
					"style" : "",
					"text" : "delay~ 4800 1264"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 32.20706,
					"id" : "obj-97",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1885.333374, 407.268829, 146.666672, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1133.345093, 224.268829, 146.666672, 42.0 ],
					"style" : "",
					"text" : "TARGET"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 466.571442, 1890.833496, 68.428558, 22.0 ],
					"style" : "",
					"text" : "*~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 630.0, 1890.833496, 85.559631, 22.0 ],
					"style" : "",
					"text" : "*~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "preset", "int", "preset", "int" ],
					"patching_rect" : [ 1348.75, 1122.666626, 100.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1004.05957, 409.935455, 164.0, 29.0 ],
					"preset_data" : [ 						{
							"number" : 1,
							"data" : [ 5, "obj-69", "umenu", "int", 17, 5, "<invalid>", "live.gain~", "float", 0.0, 5, "obj-144", "toggle", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, "obj-170", "bach.slot", "begin_preset", 1633, 256, "obj-170", "bach.slot", "restore_preset", 0, 250, "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 200.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, 256, "obj-170", "bach.slot", "restore_preset", 500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", 256, "obj-170", "bach.slot", "restore_preset", 750, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", 256, "obj-170", "bach.slot", "restore_preset", 1000, 250, "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 1250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 139, "obj-170", "bach.slot", "restore_preset", 1500, 133, 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 4, "obj-170", "bach.slot", "end_preset", 5, "obj-167", "bach.roll", "begin_preset", 4080, 256, "obj-167", "bach.roll", "restore_preset", 0, 250, "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 5.0, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 50.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 250, 250, "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "slot text", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "slot filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 150.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", 256, "obj-167", "bach.roll", "restore_preset", 500, 250, "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", 256, "obj-167", "bach.roll", "restore_preset", 750, 250, "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", 256, "obj-167", "bach.roll", "restore_preset", 1000, 250, "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 70.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", 256, "obj-167", "bach.roll", "restore_preset", 1250, 250, "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", 256, "obj-167", "bach.roll", "restore_preset", 1500, 250, ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "clefs", "FG", "FG", "FG", ")", "(", "keys", "CM", "CM", "CM", ")", "(", "voicenames", "(", ")", "(", ")", "(", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "stafflines", 5, 5, 5, ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "numparts", 1, 1, 1, ")", "(", "loop", 0.0, 211963.248542, ")", "(", "(", 0.0, "(", 6600.0, 120000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.451178, 0.0, 0.0, 24, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.034848, -20.848565, 0.222222, ")", "(", 0.453029, -5.333413, 0.0, ")", "(", 0.642609, -19.111185, 0.0, ")", "(", 0.929663, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 256, "obj-167", "bach.roll", "restore_preset", 1750, 250, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.055453, 945.553134, 0.0, ")", "(", 0.146788, 1292.219811, 0.0, ")", "(", 0.233863, 1105.182158, 0.0, ")", "(", 0.290315, 743.330905, 0.0, ")", "(", 0.386244, 1018.515489, 0.0, ")", "(", 0.453414, 1292.219811, 0.0, ")", "(", 0.512303, 1008.242255, 0.0, ")", "(", 0.624247, 1123.797814, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.792162, 1112.240426, 0.0, ")", "(", 0.864423, 1061.108693, 0.0, ")", "(", 1.0, 988.886472, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6600.0, 120000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.47075, 500.0, 0.0, 100, ")", "(", 1.0, 600.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.020202, -3.111185, -0.268889, ")", "(", 0.962963, -4.888963, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.348333, 0.74074, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.611666, 0.651851, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 2000, 250, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.445, 270.999614, 0.0, ")", "(", 0.591666, 18.999606, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.071429, 1124.444472, 0.0, ")", "(", 0.155, 1609.997606, 0.0, ")", "(", 0.233863, 1105.182158, 0.0, ")", "(", 0.291666, 627.775354, 0.0, ")", "(", 0.386244, 1018.515489, 0.0, ")", "(", 0.441666, 1494.442047, 0.0, ")", "(", 0.512303, 1008.242255, 0.0, ")", "(", 0.624247, 1123.797814, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.792162, 1112.240426, 0.0, ")", "(", 0.861666, 1609.997606, 0.0, ")", "(", 1.0, 988.886472, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 200000.0, "(", 6600.0, 160000.0, 100, "(", "breakpoints", 256, "obj-167", "bach.roll", "restore_preset", 2250, 250, "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.431606, 900.0, 0.0, 100, ")", "(", 1.0, -1300.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.098012, -12.444518, -0.268889, ")", "(", 0.675382, -20.444518, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.102017, 0.222221, 0.0, ")", "(", 0.297736, 0.251851, 0.0, ")", "(", 0.485626, 0.088888, 0.0, ")", "(", 0.642201, 0.074073, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.445, 270.999614, 0.0, ")", "(", 0.591666, 18.999606, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.071429, 1124.444472, 0.0, ")", "(", 0.155, 1609.997606, 0.0, ")", "(", 0.233863, 1105.182158, 0.0, ")", "(", 0.291666, 627.775354, 0.0, ")", "(", 0.386244, 1018.515489, 0.0, ")", "(", 0.441666, 1494.442047, 0.0, ")", "(", 0.512303, 1008.242255, 0.0, ")", "(", 0.624247, 1123.797814, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.792162, 1112.240426, 0.0, ")", "(", 0.861666, 1609.997606, 0.0, ")", "(", 1.0, 988.886472, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, 256, "obj-167", "bach.roll", "restore_preset", 2500, 250, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 300000.0, "(", 6600.0, 120000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.451178, 0.0, 0.0, 24, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.283791, -19.111185, 0.222222, ")", "(", 0.459938, -3.111185, 0.0, ")", "(", 0.642609, -19.111185, 0.0, ")", "(", 0.929663, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.055453, 945.553134, 0.0, ")", "(", 0.146788, 1292.219811, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 2750, 250, "(", 0.233863, 1105.182158, 0.0, ")", "(", 0.290315, 743.330905, 0.0, ")", "(", 0.386244, 1018.515489, 0.0, ")", "(", 0.453414, 1292.219811, 0.0, ")", "(", 0.512303, 1008.242255, 0.0, ")", "(", 0.624247, 1123.797814, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.792162, 1112.240426, 0.0, ")", "(", 0.864423, 1061.108693, 0.0, ")", "(", 1.0, 988.886472, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 7000.0, 180000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 0.451178, 0.0, 0.0, 100, ")", "(", 1.0, 0.0, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.027272, -15.333415, -0.268889, ")", "(", 0.144444, -20.1819, 0.0, ")", "(", 0.375126, -6.000075, 0.035556, ")", "(", 0.758333, -9.55563, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 3000, 250, 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.022018, "(", 0.019183, 0.0, -0.019183, -1.960132, 0.961634, "(", "bandpass", 274.62016, 0.0, 1.0, ")", ")", ")", "(", 0.511314, "(", 0.077571, 0.0, -0.077571, -1.818577, 0.844857, "(", "bandpass", 1186.114547, 0.0, 1.0, ")", ")", ")", "(", 0.760856, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", "(", 0.956574, "(", 0.060908, 0.0, -0.060908, -1.862316, 0.878184, "(", "bandpass", 913.019138, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6800.0, 220000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.457702, -100.0, 0.0, 41, ")", "(", 1.0, 0.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.572578, -18.44452, -0.231111, ")", "(", 0.794393, -38.000076, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 3250, 250, 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 220000.0, "(", 6900.0, 200000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 0.451178, 500.0, 0.0, 57, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -31.333409, -0.268889, ")", "(", 0.219061, -24.222298, 0.035556, ")", "(", 0.408256, -6.444519, -0.231111, ")", "(", 0.758333, -9.55563, -0.213333, ")", "(", 0.975, -11.777852, -0.222222, ")", 256, "obj-167", "bach.roll", "restore_preset", 3500, 250, "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 5900.0, 420000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 3750, 250, 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 9.0, 0.0, ")", "(", 0.035, 9.0, 0.0, ")", "(", 0.094131, 10.062954, 0.0, ")", "(", 0.121666, 9.0, 0.0, ")", "(", 0.158333, 9.025916, 0.0, ")", "(", 0.189194, 15.066667, 86, "obj-167", "bach.roll", "restore_preset", 4000, 80, 0.0, ")", "(", 0.314082, 16.0, 0.0, ")", "(", 0.332721, 10.788879, 0.0, ")", "(", 0.397961, 14.107398, 0.252121, ")", "(", 0.459473, 16.0, 0.0, ")", "(", 0.49116, 15.248139, 0.0, ")", "(", 0.517256, 14.962963, 0.0, ")", "(", 0.563856, 14.962963, 0.0, ")", "(", 0.623503, 13.174065, 0.0, ")", "(", 0.665, 9.0, 0.0, ")", "(", 0.709247, 10.374065, 0.0, ")", "(", 0.726023, 13.174065, 0.0, ")", "(", 0.791262, 13.070361, 0.0, ")", "(", 0.868333, 9.0, 0.0, ")", "(", 0.996301, 9.0, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", 4, "obj-167", "bach.roll", "end_preset", 5, "obj-136", "toggle", "int", 0, 5, "obj-76", "number", "int", 0, 5, "obj-29", "umenu", "int", 17, 5, "obj-7", "live.gain~", "float", -68.682343, 5, "obj-52", "toggle", "int", 1, 5, "obj-55", "live.gain~", "float", 0.0, 5, "obj-102", "toggle", "int", 1, 5, "<invalid>", "flonum", "float", 2.0 ]
						}
, 						{
							"number" : 2,
							"data" : [ 5, "obj-69", "umenu", "int", 17, 5, "<invalid>", "live.gain~", "float", 0.0, 5, "obj-144", "toggle", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, "obj-170", "bach.slot", "begin_preset", 1633, 256, "obj-170", "bach.slot", "restore_preset", 0, 250, "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 200.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, 256, "obj-170", "bach.slot", "restore_preset", 500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", 256, "obj-170", "bach.slot", "restore_preset", 750, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", 256, "obj-170", "bach.slot", "restore_preset", 1000, 250, "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 1250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 139, "obj-170", "bach.slot", "restore_preset", 1500, 133, 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 4, "obj-170", "bach.slot", "end_preset", 5, "obj-167", "bach.roll", "begin_preset", 3773, 256, "obj-167", "bach.roll", "restore_preset", 0, 250, "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 5.0, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 50.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 250, 250, "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "Source level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "Target Level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "morph", ")", "(", "type", "function", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 500, 250, ")", "(", "range", 10.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", 256, "obj-167", "bach.roll", "restore_preset", 750, 250, ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", 256, "obj-167", "bach.roll", "restore_preset", 1000, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 70.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, 256, "obj-167", "bach.roll", "restore_preset", 1250, 250, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 1500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "clefs", "FG", "FG", "FG", ")", "(", "keys", "CM", "CM", "CM", ")", "(", "voicenames", "(", ")", "(", ")", "(", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "stafflines", 5, 5, 5, ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "numparts", 1, 1, 1, ")", "(", "loop", 0.0, 211963.248542, ")", "(", "(", 0.0, "(", 6300.0, 360000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, -200.0, 0.0, 24, ")", "(", 0.348379, -400.0, 0.0, 37, ")", "(", 0.413618, -100.0, 0.0, 43, ")", "(", 0.549317, -300.0, 0.0, 56, ")", "(", 0.718939, -300.0, 0.0, 72, ")", "(", 0.922487, -100.0, 0.0, 92, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 256, "obj-167", "bach.roll", "restore_preset", 1750, 250, 0.5, ")", "(", 0.025552, -7.111185, 0.222222, ")", "(", 0.205504, -19.55563, 0.0, ")", "(", 0.459938, -3.111185, 0.0, ")", "(", 0.642609, -19.111185, 0.0, ")", "(", 0.929663, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.055453, 945.553134, 0.0, ")", "(", 0.146788, 1292.219811, 0.0, ")", "(", 0.233863, 1105.182158, 0.0, ")", "(", 0.290315, 743.330905, 0.0, ")", "(", 0.386244, 1018.515489, 0.0, ")", "(", 0.453414, 1292.219811, 0.0, ")", "(", 0.512303, 1008.242255, 0.0, ")", "(", 0.624247, 1123.797814, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.792162, 1112.240426, 0.0, ")", "(", 0.864423, 1061.108693, 0.0, ")", "(", 1.0, 988.886472, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6300.0, 320000.0, 100, 256, "obj-167", "bach.roll", "restore_preset", 2000, 250, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.295451, 213.808531, 0.0, 100, ")", "(", 0.47075, 400.0, 0.0, 100, ")", "(", 0.750497, 247.142822, 0.0, 100, ")", "(", 0.92175, 414.784992, 0.0, 100, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.020202, -3.111185, -0.268889, ")", "(", 0.962963, -4.888963, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.348333, 0.74074, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.611666, 0.651851, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.445, 270.999614, 0.0, ")", "(", 0.591666, 18.999606, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.071429, 1124.444472, 0.0, ")", "(", 0.155, 1609.997606, 0.0, ")", "(", 0.233863, 1105.182158, 0.0, ")", "(", 0.291666, 627.775354, 0.0, ")", "(", 0.386244, 1018.515489, 0.0, ")", "(", 0.441666, 1494.442047, 0.0, ")", "(", 0.512303, 1008.242255, 0.0, ")", "(", 0.624247, 1123.797814, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.792162, 1112.240426, 0.0, ")", "(", 0.861666, 1609.997606, 0.0, ")", "(", 1.0, 988.886472, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", 256, "obj-167", "bach.roll", "restore_preset", 2250, 250, ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 6300.0, 420000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 0.049395, -400.0, 0.0, 56, ")", "(", 0.451178, 0.0, 0.0, 100, ")", "(", 1.0, -300.0, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.032154, -16.222298, -0.268889, ")", "(", 0.24325, -34.000076, -0.142222, ")", "(", 0.375126, -6.000075, 0.115556, ")", "(", 0.601136, -31.333409, -0.133333, ")", "(", 0.758333, -9.55563, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 2500, 250, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.071429, 1933.333385, 0.0, ")", "(", 0.233863, 1018.515489, 0.0, ")", "(", 0.386244, 1451.848835, 0.0, ")", "(", 0.512303, 1441.575601, 0.0, ")", "(", 0.624247, 1239.353373, 0.0, ")", "(", 0.792162, 1227.795985, 0.0, ")", "(", 0.996301, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.022018, "(", 0.019183, 0.0, -0.019183, -1.960132, 0.961634, "(", "bandpass", 274.62016, 0.0, 1.0, ")", ")", ")", "(", 0.511314, "(", 0.077571, 0.0, -0.077571, -1.818577, 0.844857, "(", "bandpass", 1186.114547, 0.0, 1.0, ")", ")", ")", "(", 0.760856, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", "(", 0.956574, "(", 0.060908, 0.0, -0.060908, -1.862316, 0.878184, "(", "bandpass", 913.019138, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6100.0, 340000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.195209, 500.0, 0.0, 41, ")", "(", 0.502896, -200.0, 0.0, 52, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.270336, -31.33341, 0.0, ")", "(", 0.353229, -34.000076, 0.0, ")", "(", 0.572578, -18.44452, -0.231111, ")", "(", 0.794393, -38.000076, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 2750, 250, "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 220000.0, "(", 6400.0, 200000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 0.451178, 300.0, 0.0, 57, ")", "(", 1.0, 256, "obj-167", "bach.roll", "restore_preset", 3000, 250, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -31.333409, -0.268889, ")", "(", 0.219061, -24.222298, 0.035556, ")", "(", 0.408256, -6.444519, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 5900.0, 420000.0, 100, "(", "slots", "(", 1, 256, "obj-167", "bach.roll", "restore_preset", 3250, 250, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, 256, "obj-167", "bach.roll", "restore_preset", 3500, 250, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 6, "(", 0.0, -70.0, 0.0, ")", "(", 0.046133, -55.011207, 0.0, ")", "(", 0.073627, -27.663058, 0.195556, ")", "(", 0.092733, -62.37417, 0.0, ")", "(", 0.143061, -65.529726, 0.0, ")", "(", 0.197116, -63.426022, 0.0, ")", "(", 0.226474, -25.559354, 0.115556, ")", "(", 0.247444, -56.063059, 0.204444, ")", "(", 0.312684, -62.37417, 0.0, ")", "(", 0.338313, -27.663058, 0.177778, ")", "(", 0.372331, -52.907503, -0.106667, ")", "(", 0.430115, -62.37417, 0.0, ")", "(", 0.495354, -51.855651, 0.0, ")", "(", 0.53077, -24.507502, 0.0, ")", "(", 0.681287, -21.351947, 0.0, ")", "(", 0.733945, -65.529726, 0.0, ")", "(", 0.823416, -69.737133, 0.0, ")", "(", 0.873278, -47.648244, 0.0, ")", "(", 0.955759, -68.685281, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 7, "(", 0.0, -70.0, 0.0, ")", "(", 0.038677, -33.974169, 0.0, ")", "(", 0.202708, -50.803799, 0.0, ")", "(", 0.318276, -22.403799, 0.0, ")", "(", 0.534498, -62.637037, 0.0, ")", "(", 0.709713, -58.166763, 0.0, ")", "(", 0.79732, -17.144539, 0.0, ")", "(", 0.961351, -55.011207, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 8, "(", 0.0, 17.367672, 0.0, ")", "(", 0.491626, 10.0, 0.0, ")", "(", 1.0, 17.367672, 0.0, ")", ")", "(", 9, "(", 0.0, 9.0, 0.0, ")", "(", 0.035, 9.0, 0.0, ")", "(", 0.064307, 9.0, 0.0, ")", "(", 0.121666, 16.0, 0.0, ")", "(", 0.314082, 16.0, 0.0, ")", "(", 0.370001, 13.692583, 0.0, ")", "(", 0.412873, 14.625917, 0.0, ")", "(", 0.474384, 11.722213, 0.0, ")", "(", 0.511664, 16.0, 0.0, ")", "(", 0.563856, 16.0, 0.0, ")", "(", 0.634687, 15.351843, 0.0, ")", "(", 0.694335, 12.862954, 0.0, ")", "(", 0.726023, 13.174065, 0.0, ")", "(", 0.755847, 11.618509, 0.0, ")", 29, "obj-167", "bach.roll", "restore_preset", 3750, 23, "(", 0.798718, 11.92962, 0.0, ")", "(", 0.868333, 9.0, 0.0, ")", "(", 0.996301, 9.0, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", 4, "obj-167", "bach.roll", "end_preset", 5, "obj-136", "toggle", "int", 0, 5, "obj-76", "number", "int", 0, 5, "obj-29", "umenu", "int", 17, 5, "obj-7", "live.gain~", "float", 0.0, 5, "obj-52", "toggle", "int", 0, 5, "obj-55", "live.gain~", "float", 0.0, 5, "obj-102", "toggle", "int", 0, 5, "<invalid>", "flonum", "float", 0.5 ]
						}
, 						{
							"number" : 8,
							"data" : [ 5, "obj-69", "umenu", "int", 17, 5, "<invalid>", "live.gain~", "float", 0.0, 5, "obj-144", "toggle", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, "obj-170", "bach.slot", "begin_preset", 1633, 256, "obj-170", "bach.slot", "restore_preset", 0, 250, "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 200.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, 256, "obj-170", "bach.slot", "restore_preset", 500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", 256, "obj-170", "bach.slot", "restore_preset", 750, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", 256, "obj-170", "bach.slot", "restore_preset", 1000, 250, "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 1250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 139, "obj-170", "bach.slot", "restore_preset", 1500, 133, 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 4, "obj-170", "bach.slot", "end_preset", 5, "obj-167", "bach.roll", "begin_preset", 5580, 256, "obj-167", "bach.roll", "restore_preset", 0, 250, "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 5.0, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 50.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 250, 250, "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "Source level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "Target Level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "morph", ")", "(", "type", "function", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 500, 250, ")", "(", "range", 10.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", 256, "obj-167", "bach.roll", "restore_preset", 750, 250, ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", 256, "obj-167", "bach.roll", "restore_preset", 1000, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 70.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, 256, "obj-167", "bach.roll", "restore_preset", 1250, 250, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 1500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "clefs", "FG", "FG", "FG", ")", "(", "keys", "CM", "CM", "CM", ")", "(", "voicenames", "(", ")", "(", ")", "(", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "stafflines", 5, 5, 5, ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "numparts", 1, 1, 1, ")", "(", "loop", 0.0, 211963.248542, ")", "(", "(", 0.0, "(", 8400.0, 84363.640048, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.196721, -11.030397, 0.222222, ")", "(", 0.901639, -23.636458, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 256, "obj-167", "bach.roll", "restore_preset", 1750, 250, 0.0, ")", "(", 0.200364, 275.944967, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 20000.0, "(", 8400.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 100.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 256, "obj-167", "bach.roll", "restore_preset", 2000, 250, 535.856152, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 8400.0, 240000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -4.727368, 0.222222, ")", "(", 0.126407, -3.757671, 0.0, ")", "(", 0.256277, -14.424338, 0.0, ")", "(", 0.41039, -11.515247, 0.0, ")", "(", 0.45368, -16.363732, 0.0, ")", "(", 0.576623, -14.424338, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 256, "obj-167", "bach.roll", "restore_preset", 2250, 250, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 140000.0, "(", 8400.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 2500, 250, 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 224363.640048, "(", 8400.0, 195636.359952, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 2750, 250, "(", 0.704206, 1.0, 0.0, ")", "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 207.575762, 0.0, ")", "(", 0.046021, 207.575762, 0.0, ")", "(", 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 7400.0, 206727.275588, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.042216, -15.575854, -0.268889, ")", "(", 0.369128, -21.394036, -0.523636, ")", "(", 0.651007, -19.454642, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, 256, "obj-167", "bach.roll", "restore_preset", 3000, 250, "(", 0.0, "(", 0.0, 0.0, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 16727.275588, "(", 6800.0, 146545.460701, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -154.675329, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.254408, -18.606157, -0.142222, ")", "(", 0.650329, -19.575833, 0.115556, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.142365, 1.0, 0.0, ")", "(", 0.595523, 1.0, 0.0, ")", "(", 0.732233, 1.0, 0.0, ")", "(", 1.0, 1.0, 0.0, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.114265, 320.0, 0.0, ")", "(", 0.253196, 283.009419, 0.0, ")", "(", 0.481016, 264.514129, 0.0, ")", "(", 0.517959, 172.037677, 0.0, ")", "(", 1.0, 186.663499, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.061294, 1933.333385, 0.0, ")", "(", 0.376342, 1018.515489, 0.0, ")", "(", 0.671892, 1451.848835, 0.0, ")", "(", 0.916389, 1441.575601, 0.0, ")", "(", 1.0, 1363.702273, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 18.0, 0.0, 18.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.493329, 14.051387, 0.0, ")", "(", 0.797089, 9.73447, 0.0, ")", "(", 1.0, 10.499689, 0.0, ")", ")", ")", 256, "obj-167", "bach.roll", "restore_preset", 3250, 250, 0, ")", 0, ")", "(", 20000.0, "(", 6400.0, 370000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.042216, -15.575854, -0.268889, ")", "(", 0.369128, -21.394036, -0.523636, ")", "(", 0.651007, -19.454642, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6300.0, 150000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -154.675329, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.254408, -18.606157, -0.142222, ")", "(", 0.650329, -19.575833, 0.115556, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.142365, 1.0, 0.0, ")", "(", 0.595523, 1.0, 0.0, ")", "(", 0.732233, 1.0, 0.0, ")", "(", 1.0, 1.0, 0.0, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.114265, 320.0, 0.0, ")", "(", 0.253196, 283.009419, 0.0, ")", "(", 0.481016, 264.514129, 0.0, ")", "(", 0.517959, 256, "obj-167", "bach.roll", "restore_preset", 3500, 250, 172.037677, 0.0, ")", "(", 1.0, 186.663499, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.061294, 1933.333385, 0.0, ")", "(", 0.376342, 1018.515489, 0.0, ")", "(", 0.671892, 1451.848835, 0.0, ")", "(", 0.916389, 1441.575601, 0.0, ")", "(", 1.0, 1363.702273, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 18.0, 0.0, 18.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.493329, 14.051387, 0.0, ")", "(", 0.797089, 9.73447, 0.0, ")", "(", 1.0, 10.499689, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6100.0, 310000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 3750, 250, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 90000.0, "(", 6600.0, 310000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 4000, 250, 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6100.0, 220000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, 256, "obj-167", "bach.roll", "restore_preset", 4250, 250, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 170000.0, "(", 7100.0, 240000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -133.376617, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 4500, 250, "(", 0.110029, -13.757673, -0.133333, ")", "(", 0.468354, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 1.0, 0.0, ")", "(", 0.404956, 1.0, 0.0, ")", "(", 0.797685, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 186.663499, 0.0, ")", "(", 0.383534, 196.698065, 0.0, ")", "(", 0.71914, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1363.702273, 0.0, ")", "(", 0.15483, 1239.353373, 0.0, ")", "(", 0.532516, 1227.795985, 0.0, ")", "(", 0.99168, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.956574, "(", 0.172982, 0.0, -0.172982, -1.502355, 0.654036, "(", "bandpass", 3029.30318, 0.0, 1.0, ")", ")", ")", "(", 0.288097, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.499689, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 190000.0, "(", 5600.0, 180000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -133.376617, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.110029, -13.757673, -0.133333, ")", "(", 0.468354, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 1.0, 0.0, ")", "(", 0.404956, 1.0, 0.0, ")", "(", 0.797685, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 186.663499, 0.0, ")", "(", 0.383534, 196.698065, 0.0, ")", "(", 0.71914, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1363.702273, 0.0, ")", "(", 0.15483, 1239.353373, 0.0, ")", "(", 0.532516, 1227.795985, 0.0, ")", "(", 0.99168, 1111.664216, 256, "obj-167", "bach.roll", "restore_preset", 4750, 250, 0.0, ")", ")", "(", 5, "(", 0.956574, "(", 0.172982, 0.0, -0.172982, -1.502355, 0.654036, "(", "bandpass", 3029.30318, 0.0, 1.0, ")", ")", ")", "(", 0.288097, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.499689, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 200000.0, "(", 7700.0, 220000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, 256, "obj-167", "bach.roll", "restore_preset", 5000, 250, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 5900.0, 420000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.41515, 0.5, ")", "(", 0.113228, 0.614141, 0.0, ")", "(", 0.346869, 0.614141, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", 256, "obj-167", "bach.roll", "restore_preset", 5250, 250, "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 6, "(", 0.0, -70.0, 0.0, ")", "(", 0.046133, -55.011207, 0.0, ")", "(", 0.073627, -27.663058, 0.195556, ")", "(", 0.092733, -62.37417, 0.0, ")", "(", 0.143061, -65.529726, 0.0, ")", "(", 0.197116, -63.426022, 0.0, ")", "(", 0.226474, -25.559354, 0.115556, ")", "(", 0.247444, -56.063059, 0.204444, ")", "(", 0.312684, -62.37417, 0.0, ")", "(", 0.338313, -27.663058, 0.177778, ")", "(", 0.372331, -52.907503, -0.106667, ")", "(", 0.430115, -62.37417, 0.0, ")", "(", 0.495354, -51.855651, 0.0, ")", "(", 0.53077, -24.507502, 0.0, ")", "(", 0.681287, -21.351947, 0.0, ")", "(", 0.733945, -65.529726, 0.0, ")", "(", 0.823416, -69.737133, 0.0, ")", "(", 0.873278, -47.648244, 0.0, ")", "(", 0.955759, -68.685281, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 7, "(", 0.0, -70.0, 0.0, ")", "(", 0.038677, -33.974169, 0.0, ")", "(", 0.202708, -50.803799, 0.0, ")", "(", 0.318276, -22.403799, 0.0, ")", "(", 0.534498, -62.637037, 0.0, ")", "(", 0.709713, -58.166763, 0.0, ")", "(", 0.79732, -17.144539, 0.0, ")", "(", 0.961351, -55.011207, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 8, "(", 0.0, 17.367672, 0.0, ")", "(", 0.491626, 10.0, 0.0, ")", "(", 1.0, 17.367672, 0.0, ")", ")", 86, "obj-167", "bach.roll", "restore_preset", 5500, 80, "(", 9, "(", 0.0, 16.0, 0.0, ")", "(", 0.121666, 16.0, 0.0, ")", "(", 0.185281, 11.135353, 0.0, ")", "(", 0.34632, 11.135353, 0.0, ")", "(", 0.377489, 9.0, 0.0, ")", "(", 0.47619, 9.0, 0.0, ")", "(", 0.511664, 14.868687, 0.0, ")", "(", 0.557576, 14.734332, 0.0, ")", "(", 0.593939, 9.0, 0.0, ")", "(", 0.694372, 9.0, 0.0, ")", "(", 0.755847, 11.73164, 0.0, ")", "(", 0.8, 11.792917, 0.0, ")", "(", 0.868333, 9.0, 0.0, ")", "(", 0.996301, 9.0, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", 4, "obj-167", "bach.roll", "end_preset", 5, "obj-136", "toggle", "int", 0, 5, "obj-76", "number", "int", 0, 5, "obj-29", "umenu", "int", 17, 5, "obj-7", "live.gain~", "float", -70.0, 5, "obj-52", "toggle", "int", 1, 5, "obj-55", "live.gain~", "float", 0.0, 5, "obj-102", "toggle", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 5, "obj-84", "number", "int", 84, 5, "obj-106", "number", "int", 68 ]
						}
, 						{
							"number" : 14,
							"data" : [ 5, "obj-69", "umenu", "int", 17, 5, "<invalid>", "live.gain~", "float", 0.0, 5, "obj-144", "toggle", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, "obj-170", "bach.slot", "begin_preset", 1633, 256, "obj-170", "bach.slot", "restore_preset", 0, 250, "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 200.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, 256, "obj-170", "bach.slot", "restore_preset", 500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", 256, "obj-170", "bach.slot", "restore_preset", 750, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", 256, "obj-170", "bach.slot", "restore_preset", 1000, 250, "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 1250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 139, "obj-170", "bach.slot", "restore_preset", 1500, 133, 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 4, "obj-170", "bach.slot", "end_preset", 5, "obj-167", "bach.roll", "begin_preset", 8083, 256, "obj-167", "bach.roll", "restore_preset", 0, 250, "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 5.0, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 50.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 250, 250, "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "Source level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "Target Level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "morph", ")", "(", "type", "function", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 500, 250, ")", "(", "range", 10.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", 256, "obj-167", "bach.roll", "restore_preset", 750, 250, ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", 256, "obj-167", "bach.roll", "restore_preset", 1000, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 70.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, 256, "obj-167", "bach.roll", "restore_preset", 1250, 250, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 1500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "clefs", "FG", "FG", "FG", ")", "(", "keys", "CM", "CM", "CM", ")", "(", "voicenames", "(", ")", "(", ")", "(", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "stafflines", 5, 5, 5, ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "numparts", 1, 1, 1, ")", "(", "loop", 0.0, 211963.248542, ")", "(", "(", 0.0, "(", 7800.0, 84363.640048, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.196721, -11.030397, 0.222222, ")", "(", 0.901639, -23.636458, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 256, "obj-167", "bach.roll", "restore_preset", 1750, 250, 0.0, ")", "(", 0.200364, 275.944967, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 0.0, "(", 6800.0, 80000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.027273, -12.121307, 0.222222, ")", "(", 0.901639, -23.636458, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.200364, 275.944967, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 20000.0, "(", 6700.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 100.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, 256, "obj-167", "bach.roll", "restore_preset", 2000, 250, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 30000.0, "(", 7800.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 2250, 250, 100, ")", "(", 0.209792, 100.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 2500, 250, "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 7300.0, 80000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.196721, -11.030397, 0.222222, ")", "(", 0.901639, -23.636458, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.200364, 275.944967, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 80000.0, "(", 6800.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 100.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 256, "obj-167", "bach.roll", "restore_preset", 2750, 250, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 7800.0, 180000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -4.727368, 0.222222, ")", "(", 0.126407, -3.757671, 0.0, ")", "(", 0.256277, -14.424338, 0.0, ")", "(", 0.41039, -11.515247, 0.0, ")", "(", 0.45368, -16.363732, 0.0, ")", "(", 0.576623, -14.424338, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 3000, 250, 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6800.0, 240000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -4.727368, 0.222222, ")", "(", 0.126407, -3.757671, 0.0, ")", "(", 0.256277, -14.424338, 0.0, ")", "(", 0.41039, -11.515247, 0.0, ")", "(", 0.45368, -16.363732, 0.0, ")", "(", 0.576623, 256, "obj-167", "bach.roll", "restore_preset", 3250, 250, -14.424338, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 140000.0, "(", 7800.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", 256, "obj-167", "bach.roll", "restore_preset", 3500, 250, "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 3750, 250, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 150000.0, "(", 7300.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, 256, "obj-167", "bach.roll", "restore_preset", 4000, 250, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 160000.0, "(", 6800.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 256, "obj-167", "bach.roll", "restore_preset", 4250, 250, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 170000.0, "(", 6700.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 4500, 250, "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 220000.0, "(", 6800.0, 200000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", "(", 0.704206, 1.0, 0.0, ")", "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 256, "obj-167", "bach.roll", "restore_preset", 4750, 250, 0.0, 207.575762, 0.0, ")", "(", 0.046021, 207.575762, 0.0, ")", "(", 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 224363.640048, "(", 7800.0, 195636.359952, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", "(", 0.704206, 1.0, 0.0, ")", "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 207.575762, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 5000, 250, 0.046021, 207.575762, 0.0, ")", "(", 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 260000.0, "(", 7300.0, 160000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", "(", 0.704206, 1.0, 0.0, ")", "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 207.575762, 0.0, ")", "(", 0.046021, 207.575762, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 5250, 250, 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 7300.0, 206727.275588, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.014072, -13.63646, -0.268889, ")", "(", 0.369128, -21.394036, -0.523636, ")", "(", 0.651007, -19.454642, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 16727.275588, "(", 6800.0, 146545.460701, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -154.675329, 0.0, 49, ")", ")", 256, "obj-167", "bach.roll", "restore_preset", 5500, 250, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.254408, -18.606157, -0.142222, ")", "(", 0.650329, -19.575833, 0.115556, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.142365, 1.0, 0.0, ")", "(", 0.595523, 1.0, 0.0, ")", "(", 0.732233, 1.0, 0.0, ")", "(", 1.0, 1.0, 0.0, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.114265, 320.0, 0.0, ")", "(", 0.253196, 283.009419, 0.0, ")", "(", 0.481016, 264.514129, 0.0, ")", "(", 0.517959, 172.037677, 0.0, ")", "(", 1.0, 186.663499, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.061294, 1933.333385, 0.0, ")", "(", 0.376342, 1018.515489, 0.0, ")", "(", 0.671892, 1451.848835, 0.0, ")", "(", 0.916389, 1441.575601, 0.0, ")", "(", 1.0, 1363.702273, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 18.0, 0.0, 18.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.493329, 14.051387, 0.0, ")", "(", 0.797089, 9.73447, 0.0, ")", "(", 1.0, 10.499689, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 20000.0, "(", 6400.0, 370000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.042216, -15.575854, -0.268889, ")", "(", 0.369128, -21.394036, -0.523636, ")", "(", 0.651007, -19.454642, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 256, "obj-167", "bach.roll", "restore_preset", 5750, 250, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6300.0, 150000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -154.675329, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.254408, -18.606157, -0.142222, ")", "(", 0.650329, -19.575833, 0.115556, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.142365, 1.0, 0.0, ")", "(", 0.595523, 1.0, 0.0, ")", "(", 0.732233, 1.0, 0.0, ")", "(", 1.0, 1.0, 0.0, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.114265, 320.0, 0.0, ")", "(", 0.253196, 283.009419, 0.0, ")", "(", 0.481016, 264.514129, 0.0, ")", "(", 0.517959, 172.037677, 0.0, ")", "(", 1.0, 186.663499, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.061294, 1933.333385, 0.0, ")", "(", 0.376342, 1018.515489, 0.0, ")", "(", 0.671892, 1451.848835, 0.0, ")", "(", 0.916389, 1441.575601, 0.0, ")", "(", 1.0, 1363.702273, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 18.0, 0.0, 18.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 6000, 250, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.493329, 14.051387, 0.0, ")", "(", 0.797089, 9.73447, 0.0, ")", "(", 1.0, 10.499689, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6100.0, 310000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 256, "obj-167", "bach.roll", "restore_preset", 6250, 250, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 90000.0, "(", 6600.0, 310000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 256, "obj-167", "bach.roll", "restore_preset", 6500, 250, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6100.0, 220000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 6750, 250, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 170000.0, "(", 7100.0, 240000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -133.376617, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.110029, -13.757673, -0.133333, ")", "(", 0.468354, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 1.0, 0.0, ")", "(", 0.404956, 1.0, 0.0, ")", "(", 0.797685, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 186.663499, 0.0, ")", "(", 0.383534, 196.698065, 0.0, ")", "(", 0.71914, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1363.702273, 0.0, ")", "(", 0.15483, 1239.353373, 256, "obj-167", "bach.roll", "restore_preset", 7000, 250, 0.0, ")", "(", 0.532516, 1227.795985, 0.0, ")", "(", 0.99168, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.956574, "(", 0.172982, 0.0, -0.172982, -1.502355, 0.654036, "(", "bandpass", 3029.30318, 0.0, 1.0, ")", ")", ")", "(", 0.288097, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.499689, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 190000.0, "(", 5600.0, 180000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -133.376617, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.110029, -13.757673, -0.133333, ")", "(", 0.468354, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.757576, 0.0, ")", "(", 0.404956, 0.757576, 0.0, ")", "(", 0.79495, 0.792928, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 186.663499, 0.0, ")", "(", 0.383534, 196.698065, 0.0, ")", "(", 0.71914, 220.61171, 0.0, ")", "(", 1.0, 154.273446, 0.0, ")", ")", "(", 4, "(", 0.0, 1363.702273, 0.0, ")", "(", 0.15483, 1779.393933, 0.0, ")", "(", 0.532516, 1779.393933, 0.0, ")", "(", 0.99168, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.124242, "(", 0.015788, 0.0, -0.015788, -1.967412, 0.968425, "(", "bandpass", 225.210743, 0.0, 1.0, ")", ")", ")", "(", 0.960606, "(", 0.006166, 0.0, -0.006166, -1.98722, 0.987667, "(", "bandpass", 148.977372, 0.0, 1.71036, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.499689, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 200000.0, "(", 7700.0, 220000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 256, "obj-167", "bach.roll", "restore_preset", 7250, 250, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 8200.0, 420000.0, 100, "(", "slots", "(", 1, 256, "obj-167", "bach.roll", "restore_preset", 7500, 250, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.41515, 0.5, ")", "(", 0.113228, 0.614141, 0.0, ")", "(", 0.346869, 0.614141, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, 256, "obj-167", "bach.roll", "restore_preset", 7750, 250, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 6, "(", 0.0, -70.0, 0.0, ")", "(", 0.046133, -55.011207, 0.0, ")", "(", 0.073627, -27.663058, 0.195556, ")", "(", 0.092733, -62.37417, 0.0, ")", "(", 0.143061, -65.529726, 0.0, ")", "(", 0.197116, -63.426022, 0.0, ")", "(", 0.226474, -25.559354, 0.115556, ")", "(", 0.247444, -56.063059, 0.204444, ")", "(", 0.312684, -62.37417, 0.0, ")", "(", 0.338313, -27.663058, 0.177778, ")", "(", 0.372331, -52.907503, -0.106667, ")", "(", 0.430115, -62.37417, 0.0, ")", "(", 0.495354, -51.855651, 0.0, ")", "(", 0.53077, -24.507502, 0.0, ")", "(", 0.681287, -21.351947, 0.0, ")", "(", 0.733945, -65.529726, 0.0, ")", "(", 0.823416, -69.737133, 0.0, ")", "(", 0.873278, -47.648244, 0.0, ")", "(", 0.955759, -68.685281, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 7, "(", 0.0, -70.0, 0.0, ")", "(", 0.038677, -33.974169, 0.0, ")", "(", 0.202708, -50.803799, 0.0, ")", "(", 0.318276, -22.403799, 0.0, ")", "(", 0.534498, -62.637037, 0.0, ")", "(", 0.709713, -58.166763, 0.0, ")", "(", 0.79732, -17.144539, 0.0, ")", "(", 0.961351, -55.011207, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 8, "(", 0.0, 2000.0, 0.0, ")", "(", 0.046753, 675.340056, 0.0, ")", "(", 0.112554, 10.0, 0.0, ")", "(", 0.212987, 10.0, 0.0, ")", "(", 0.277056, 803.986525, 0.0, ")", "(", 0.370563, 900.471376, 0.0, ")", "(", 0.491626, 10.0, 0.0, ")", "(", 0.541991, 10.0, 0.0, ")", "(", 0.581818, 289.400651, 0.0, ")", "(", 0.671861, 385.885502, 0.0, ")", "(", 0.729004, 64.269331, 0.0, ")", "(", 0.801732, 64.269331, 0.0, ")", "(", 0.903896, 2000.0, 0.0, ")", "(", 0.980087, 1833.158272, 0.0, ")", "(", 1.0, 17.367672, 0.0, ")", ")", "(", 9, "(", 0.0, 9.0, 0.0, ")", "(", 0.062338, 16.0, 0.0, ")", 89, "obj-167", "bach.roll", "restore_preset", 8000, 83, "(", 0.121666, 16.0, 0.0, ")", "(", 0.14026, 13.037362, 0.0, ")", "(", 0.187013, 12.697968, 0.0, ")", "(", 0.232035, 14.734332, 0.0, ")", "(", 0.348052, 14.734332, 0.0, ")", "(", 0.377489, 9.0, 0.0, ")", "(", 0.47619, 9.0, 0.0, ")", "(", 0.505628, 14.734332, 0.0, ")", "(", 0.557576, 14.734332, 0.0, ")", "(", 0.593939, 9.0, 0.0, ")", "(", 0.694372, 9.0, 0.0, ")", "(", 0.71342, 16.0, 0.0, ")", "(", 0.820779, 16.0, 0.0, ")", "(", 0.868333, 9.0, 0.0, ")", "(", 0.996301, 9.0, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", 4, "obj-167", "bach.roll", "end_preset", 5, "obj-136", "toggle", "int", 0, 5, "obj-76", "number", "int", 0, 5, "obj-29", "umenu", "int", 17, 5, "obj-7", "live.gain~", "float", -70.0, 5, "obj-52", "toggle", "int", 1, 5, "obj-55", "live.gain~", "float", 0.0, 5, "obj-102", "toggle", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 5, "obj-84", "number", "int", 67, 5, "obj-106", "number", "int", 82 ]
						}
, 						{
							"number" : 17,
							"data" : [ 5, "obj-69", "umenu", "int", 17, 5, "<invalid>", "live.gain~", "float", 0.0, 5, "obj-144", "toggle", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, "obj-170", "bach.slot", "begin_preset", 1633, 256, "obj-170", "bach.slot", "restore_preset", 0, 250, "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 200.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, 256, "obj-170", "bach.slot", "restore_preset", 500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", 256, "obj-170", "bach.slot", "restore_preset", 750, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", 256, "obj-170", "bach.slot", "restore_preset", 1000, 250, "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 1250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 139, "obj-170", "bach.slot", "restore_preset", 1500, 133, 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 4, "obj-170", "bach.slot", "end_preset", 5, "obj-167", "bach.roll", "begin_preset", 4089, 256, "obj-167", "bach.roll", "restore_preset", 0, 250, "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 5.0, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 50.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 250, 250, "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "Source level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "Target Level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "morph", ")", "(", "type", "function", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 500, 250, ")", "(", "range", 10.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", 256, "obj-167", "bach.roll", "restore_preset", 750, 250, ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", 256, "obj-167", "bach.roll", "restore_preset", 1000, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 70.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, 256, "obj-167", "bach.roll", "restore_preset", 1250, 250, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 1500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "clefs", "FG", "FG", "FG", ")", "(", "keys", "CM", "CM", "CM", ")", "(", "voicenames", "(", ")", "(", ")", "(", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "stafflines", 5, 5, 5, ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "numparts", 1, 1, 1, ")", "(", "loop", 0.0, 211963.248542, ")", "(", "(", 0.0, "(", 6100.0, 420000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.549317, 0.0, 0.0, 56, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 256, "obj-167", "bach.roll", "restore_preset", 1750, 250, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 0.0, "(", 6700.0, 420000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 256, "obj-167", "bach.roll", "restore_preset", 2000, 250, -200.0, 0.0, 24, ")", "(", 0.348379, -400.0, 0.0, 37, ")", "(", 0.413618, -500.0, 0.0, 43, ")", "(", 0.549317, -500.0, 0.0, 56, ")", "(", 0.718939, 100.0, 0.0, 72, ")", "(", 0.922487, -100.0, 0.0, 92, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.095238, -9.091004, 0.222222, ")", "(", 0.332468, -1.333428, 0.0, ")", "(", 0.470996, -21.697065, 0.0, ")", "(", 0.75671, -32.363732, 0.0, ")", "(", 0.929663, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.055453, 207.575762, 0.0, ")", "(", 0.146788, 207.575762, 0.0, ")", "(", 0.233863, 1105.182158, 0.0, ")", "(", 0.290315, 743.330905, 0.0, ")", "(", 0.386244, 235.151463, 0.0, ")", "(", 0.453414, 235.151463, 0.0, ")", "(", 0.512303, 283.393748, 0.0, ")", "(", 0.624247, 398.949307, 0.0, ")", "(", 0.695, 433.431914, 0.0, ")", "(", 0.792162, 860.119206, 0.0, ")", "(", 0.864423, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 256, "obj-167", "bach.roll", "restore_preset", 2250, 250, 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 0.0, "(", 6800.0, 420000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 0.0, 0.0, 24, ")", "(", 0.348379, -400.0, 0.0, 37, ")", "(", 0.407558, 0.0, 0.0, 43, ")", "(", 0.549317, -300.0, 0.0, 56, ")", "(", 0.718939, -300.0, 0.0, 72, ")", "(", 0.922487, -100.0, 0.0, 92, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -18.909186, 0.222222, ")", "(", 0.205504, -21.495024, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 256, "obj-167", "bach.roll", "restore_preset", 2500, 250, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 6200.0, 36727.275588, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.807336, -0.707146, -0.268889, ")", "(", 1.0, -41.878886, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 6727.275588, "(", 6300.0, 403272.724412, 51, "(", "breakpoints", "(", 256, "obj-167", "bach.roll", "restore_preset", 2750, 250, 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -288.051946, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.209197, 0.0, -0.142222, ")", "(", 0.349207, -6.000075, 0.115556, ")", "(", 0.587917, -13.757673, -0.133333, ")", "(", 0.753832, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.076446, 1.0, 0.0, ")", "(", 0.319778, 1.0, 0.0, ")", "(", 0.393188, 1.0, 0.0, ")", "(", 0.724477, 1.0, 0.0, ")", "(", 0.906322, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.061357, 320.0, 0.0, ")", "(", 0.135959, 283.009419, 0.0, ")", "(", 0.258291, 264.514129, 0.0, ")", "(", 0.278129, 172.037677, 0.0, ")", "(", 0.714558, 196.698065, 0.0, ")", "(", 0.869954, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.032913, 1933.333385, 0.0, ")", "(", 0.202085, 1018.515489, 0.0, ")", "(", 0.360786, 1451.848835, 0.0, ")", "(", 0.492074, 1441.575601, 0.0, ")", "(", 0.608661, 1239.353373, 0.0, ")", "(", 0.783541, 1227.795985, 0.0, ")", "(", 0.996147, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.288097, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", "(", 0.956574, "(", 0.172982, 0.0, -0.172982, -1.502355, 0.654036, "(", "bandpass", 3029.30318, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.264903, 14.051387, 0.0, ")", "(", 0.428013, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6500.0, 340000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 256, "obj-167", "bach.roll", "restore_preset", 3000, 250, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 256, "obj-167", "bach.roll", "restore_preset", 3250, 250, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 210000.0, "(", 6500.0, 210000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, 256, "obj-167", "bach.roll", "restore_preset", 3500, 250, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 5900.0, 420000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", 256, "obj-167", "bach.roll", "restore_preset", 3750, 250, "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 6, "(", 0.0, -70.0, 0.0, ")", "(", 0.046133, -55.011207, 0.0, ")", "(", 0.073627, -27.663058, 0.195556, ")", "(", 0.092733, -62.37417, 0.0, ")", "(", 0.143061, -65.529726, 0.0, ")", "(", 0.197116, -63.426022, 0.0, ")", "(", 0.226474, -25.559354, 0.115556, ")", "(", 0.247444, -56.063059, 0.204444, ")", "(", 0.312684, -62.37417, 0.0, ")", "(", 0.338313, -27.663058, 0.177778, ")", "(", 0.372331, -52.907503, -0.106667, ")", "(", 0.430115, -62.37417, 0.0, ")", "(", 0.495354, -51.855651, 0.0, ")", "(", 0.53077, -24.507502, 0.0, ")", "(", 0.681287, -21.351947, 0.0, ")", "(", 0.733945, -65.529726, 0.0, ")", "(", 0.823416, -69.737133, 0.0, ")", "(", 0.873278, -47.648244, 0.0, ")", "(", 0.955759, -68.685281, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 7, "(", 0.0, -70.0, 0.0, ")", "(", 0.038677, -33.974169, 0.0, ")", "(", 0.202708, -50.803799, 0.0, ")", "(", 0.318276, -22.403799, 0.0, ")", "(", 0.534498, -62.637037, 0.0, ")", "(", 0.709713, -58.166763, 0.0, ")", "(", 0.79732, -17.144539, 0.0, ")", "(", 0.961351, -55.011207, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 8, "(", 0.0, 17.367672, 0.0, ")", "(", 0.491626, 10.0, 0.0, ")", "(", 1.0, 17.367672, 0.0, ")", ")", "(", 9, "(", 0.0, 9.0, 0.0, ")", "(", 0.035, 9.0, 0.0, ")", "(", 0.064307, 9.0, 0.0, 95, "obj-167", "bach.roll", "restore_preset", 4000, 89, ")", "(", 0.121666, 10.569697, 0.0, ")", "(", 0.314082, 10.569697, 0.0, ")", "(", 0.358442, 13.71615, 0.0, ")", "(", 0.382684, 15.41312, 0.0, ")", "(", 0.405195, 15.299988, 0.0, ")", "(", 0.413853, 13.829281, 0.0, ")", "(", 0.474384, 11.722213, 0.0, ")", "(", 0.511664, 11.70101, 0.0, ")", "(", 0.563856, 11.70101, 0.0, ")", "(", 0.632035, 13.037362, 0.0, ")", "(", 0.697836, 12.811099, 0.0, ")", "(", 0.730736, 14.734332, 0.0, ")", "(", 0.755847, 14.786186, 0.0, ")", "(", 0.8, 14.847463, 0.0, ")", "(", 0.868333, 16.0, 0.0, ")", "(", 0.996301, 16.0, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", 4, "obj-167", "bach.roll", "end_preset", 5, "obj-136", "toggle", "int", 0, 5, "obj-76", "number", "int", 0, 5, "obj-29", "umenu", "int", 17, 5, "obj-7", "live.gain~", "float", -61.340134, 5, "obj-52", "toggle", "int", 1, 5, "obj-55", "live.gain~", "float", -1.658696, 5, "obj-102", "toggle", "int", 0, 5, "<invalid>", "flonum", "float", 0.5, 5, "obj-84", "number", "int", 68, 5, "obj-106", "number", "int", 62 ]
						}
, 						{
							"number" : 18,
							"data" : [ 5, "obj-69", "umenu", "int", 17, 5, "<invalid>", "live.gain~", "float", 0.0, 5, "obj-144", "toggle", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, "obj-170", "bach.slot", "begin_preset", 1633, 256, "obj-170", "bach.slot", "restore_preset", 0, 250, "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 200.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, 256, "obj-170", "bach.slot", "restore_preset", 500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", 256, "obj-170", "bach.slot", "restore_preset", 750, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", 256, "obj-170", "bach.slot", "restore_preset", 1000, 250, "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 1250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 139, "obj-170", "bach.slot", "restore_preset", 1500, 133, 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 4, "obj-170", "bach.slot", "end_preset", 5, "obj-167", "bach.roll", "begin_preset", 4537, 256, "obj-167", "bach.roll", "restore_preset", 0, 250, "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 5.0, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 50.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 250, 250, "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "Source level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "Target Level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "morph", ")", "(", "type", "function", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 500, 250, ")", "(", "range", 10.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", 256, "obj-167", "bach.roll", "restore_preset", 750, 250, ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", 256, "obj-167", "bach.roll", "restore_preset", 1000, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 70.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, 256, "obj-167", "bach.roll", "restore_preset", 1250, 250, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 1500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "clefs", "FG", "FG", "FG", ")", "(", "keys", "CM", "CM", "CM", ")", "(", "voicenames", "(", ")", "(", ")", "(", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "stafflines", 5, 5, 5, ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "numparts", 1, 1, 1, ")", "(", "loop", 0.0, 211963.248542, ")", "(", "(", 0.0, "(", 8000.0, 44363.640048, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.901639, -9.091004, 0.222222, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.200364, 275.944967, 256, "obj-167", "bach.roll", "restore_preset", 1750, 250, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 20000.0, "(", 7700.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 0.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 0.0, ")", "(", 0.233863, 256, "obj-167", "bach.roll", "restore_preset", 2000, 250, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6400.0, 240000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -4.727368, 0.222222, ")", "(", 0.126407, -3.757671, 0.0, ")", "(", 0.256277, -14.424338, 0.0, ")", "(", 0.395455, -11.757671, 0.0, ")", "(", 0.45368, -16.363732, 0.0, ")", "(", 0.576623, -14.424338, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, 256, "obj-167", "bach.roll", "restore_preset", 2250, 250, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 140000.0, "(", 7600.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 2500, 250, 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 224363.640048, "(", 7899.302319, 195636.359952, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", "(", 0.704206, 1.0, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 2750, 250, "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 207.575762, 0.0, ")", "(", 0.046021, 207.575762, 0.0, ")", "(", 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 7400.0, 216727.275588, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.181208, -30.121309, -0.268889, ")", "(", 0.369128, -45.636461, -0.523636, ")", "(", 0.651007, -43.697067, 0.0, ")", "(", 1.0, -41.878886, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 3000, 250, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 6727.275588, "(", 7500.0, 216545.460701, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -154.675329, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.254408, -5.030399, -0.142222, ")", "(", 0.650329, -6.000075, 0.115556, ")", "(", 1.0, -12.728262, 0.0, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.142365, 1.0, 0.0, ")", "(", 0.595523, 1.0, 0.0, ")", "(", 0.732233, 1.0, 0.0, ")", "(", 1.0, 1.0, 0.0, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.114265, 320.0, 0.0, ")", "(", 0.253196, 283.009419, 0.0, ")", "(", 0.481016, 264.514129, 0.0, ")", "(", 0.517959, 172.037677, 0.0, ")", "(", 1.0, 186.663499, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.061294, 1933.333385, 0.0, ")", "(", 0.376342, 1018.515489, 0.0, ")", "(", 0.671892, 1451.848835, 0.0, ")", "(", 0.916389, 1441.575601, 0.0, ")", "(", 1.0, 1363.702273, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 18.0, 0.0, 18.0, "(", "none", 0.0, 1483.5, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.493329, 14.051387, 0.0, ")", "(", 0.797089, 9.73447, 0.0, ")", "(", 1.0, 10.499689, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 3250, 250, 40000.0, "(", 7700.0, 340000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, 256, "obj-167", "bach.roll", "restore_preset", 3500, 250, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 210000.0, "(", 7700.0, 210000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 256, "obj-167", "bach.roll", "restore_preset", 3750, 250, 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 223272.736289, "(", 7345.324671, 186727.263711, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -133.376617, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -12.728262, 0.0, ")", "(", 0.110029, -13.757673, -0.133333, ")", "(", 0.468354, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 1.0, 0.0, ")", "(", 0.404956, 1.0, 0.0, ")", "(", 0.797685, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 186.663499, 0.0, ")", "(", 0.383534, 196.698065, 0.0, ")", "(", 0.71914, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1363.702273, 0.0, ")", "(", 0.15483, 1239.353373, 0.0, ")", "(", 0.532516, 1227.795985, 0.0, ")", "(", 0.99168, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.956574, "(", 0.172982, 0.0, -0.172982, -1.502355, 0.654036, "(", "bandpass", 3029.30318, 0.0, 1.0, ")", ")", ")", "(", 0.288097, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.499689, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 5900.0, 420000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 4000, 250, 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.41515, 0.5, ")", "(", 0.113228, 0.614141, 0.0, ")", "(", 0.346869, 0.614141, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 6, 256, "obj-167", "bach.roll", "restore_preset", 4250, 250, "(", 0.0, -70.0, 0.0, ")", "(", 0.046133, -55.011207, 0.0, ")", "(", 0.073627, -27.663058, 0.195556, ")", "(", 0.092733, -62.37417, 0.0, ")", "(", 0.143061, -65.529726, 0.0, ")", "(", 0.197116, -63.426022, 0.0, ")", "(", 0.226474, -25.559354, 0.115556, ")", "(", 0.247444, -56.063059, 0.204444, ")", "(", 0.312684, -62.37417, 0.0, ")", "(", 0.338313, -27.663058, 0.177778, ")", "(", 0.372331, -52.907503, -0.106667, ")", "(", 0.430115, -62.37417, 0.0, ")", "(", 0.495354, -51.855651, 0.0, ")", "(", 0.53077, -24.507502, 0.0, ")", "(", 0.681287, -21.351947, 0.0, ")", "(", 0.733945, -65.529726, 0.0, ")", "(", 0.823416, -69.737133, 0.0, ")", "(", 0.873278, -47.648244, 0.0, ")", "(", 0.955759, -68.685281, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 7, "(", 0.0, -70.0, 0.0, ")", "(", 0.038677, -33.974169, 0.0, ")", "(", 0.202708, -50.803799, 0.0, ")", "(", 0.318276, -22.403799, 0.0, ")", "(", 0.534498, -62.637037, 0.0, ")", "(", 0.709713, -58.166763, 0.0, ")", "(", 0.79732, -17.144539, 0.0, ")", "(", 0.961351, -55.011207, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 8, "(", 0.0, 17.367672, 0.0, ")", "(", 0.491626, 10.0, 0.0, ")", "(", 1.0, 17.367672, 0.0, ")", ")", "(", 9, "(", 0.0, 9.0, 0.0, ")", "(", 0.035, 9.0, 0.0, ")", "(", 0.064307, 9.0, 0.0, ")", "(", 0.121666, 14.189899, 0.0, ")", "(", 0.185281, 14.281806, 0.0, ")", "(", 0.204329, 9.0, 0.0, ")", "(", 0.322078, 9.0, 0.0, ")", "(", 0.34632, 16.0, 0.0, ")", "(", 0.361905, 15.978776, 0.0, ")", "(", 0.382684, 11.000998, 0.0, ")", "(", 0.405195, 10.887867, 0.0, ")", "(", 0.413853, 13.829281, 0.0, ")", "(", 0.470996, 13.71615, 0.0, ")", "(", 0.511664, 14.868687, 0.0, ")", "(", 0.557576, 14.734332, 0.0, ")", "(", 0.571429, 11.340392, 0.0, ")", "(", 43, "obj-167", "bach.roll", "restore_preset", 4500, 37, 0.632035, 10.774736, 0.0, ")", "(", 0.694372, 9.0, 0.0, ")", "(", 0.755847, 11.73164, 0.0, ")", "(", 0.8, 11.792917, 0.0, ")", "(", 0.868333, 9.0, 0.0, ")", "(", 0.996301, 9.0, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", 4, "obj-167", "bach.roll", "end_preset", 5, "obj-136", "toggle", "int", 0, 5, "obj-76", "number", "int", 0, 5, "obj-29", "umenu", "int", 17, 5, "obj-7", "live.gain~", "float", -70.0, 5, "obj-52", "toggle", "int", 1, 5, "obj-55", "live.gain~", "float", -1.658696, 5, "obj-102", "toggle", "int", 0, 5, "<invalid>", "flonum", "float", 0.5, 5, "obj-84", "number", "int", 64, 5, "obj-106", "number", "int", 62 ]
						}
, 						{
							"number" : 24,
							"data" : [ 5, "obj-69", "umenu", "int", 17, 5, "<invalid>", "live.gain~", "float", 0.0, 5, "obj-144", "toggle", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "number", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 10, "<invalid>", "multislider", "list", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5, "obj-170", "bach.slot", "begin_preset", 1633, 256, "obj-170", "bach.slot", "restore_preset", 0, 250, "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.1, 200.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "default", 0.0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, 256, "obj-170", "bach.slot", "restore_preset", 500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", 256, "obj-170", "bach.slot", "restore_preset", 750, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", 256, "obj-170", "bach.slot", "restore_preset", 1000, 250, "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", 256, "obj-170", "bach.slot", "restore_preset", 1250, 250, "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 0.0, 0.0, ")", "(", 0.346869, 0.0, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 139, "obj-170", "bach.slot", "restore_preset", 1500, 133, 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 4, "obj-170", "bach.slot", "end_preset", 5, "obj-167", "bach.roll", "begin_preset", 8008, 256, "obj-167", "bach.roll", "restore_preset", 0, 250, "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -60.0, 0.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 0.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 5.0, 320.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 50.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 250, 250, "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", 0.0, 22050.0, ")", "(", "slope", 0.6, ")", "(", "representation", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "Source level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "Target Level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", -70.0, 1.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "morph", ")", "(", "type", "function", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 500, 250, ")", "(", "range", 10.0, 2000.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", 9.0, 16.0, ")", "(", "slope", 0.0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", 0.0, 1.0, ")", "(", "domainslope", 0.0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", 256, "obj-167", "bach.roll", "restore_preset", 750, 250, ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", 256, "obj-167", "bach.roll", "restore_preset", 1000, 250, ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 70.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 110.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, 256, "obj-167", "bach.roll", "restore_preset", 1250, 250, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, 256, "obj-167", "bach.roll", "restore_preset", 1500, 250, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", 100.0, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "clefs", "FG", "FG", "FG", ")", "(", "keys", "CM", "CM", "CM", ")", "(", "voicenames", "(", ")", "(", ")", "(", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "stafflines", 5, 5, 5, ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "numparts", 1, 1, 1, ")", "(", "loop", 0.0, 211963.248542, ")", "(", "(", 0.0, "(", 7800.0, 84363.640048, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.196721, -11.030397, 0.222222, ")", "(", 0.901639, -23.636458, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 256, "obj-167", "bach.roll", "restore_preset", 1750, 250, 0.0, ")", "(", 0.200364, 275.944967, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 0.0, "(", 6800.0, 80000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.196721, -11.030397, 0.222222, ")", "(", 0.901639, -23.636458, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.200364, 275.944967, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 20000.0, "(", 6700.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 100.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, 256, "obj-167", "bach.roll", "restore_preset", 2000, 250, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 30000.0, "(", 7800.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 2250, 250, 100, ")", "(", 0.209792, 100.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 2500, 250, "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 7300.0, 80000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, -100.697681, 0.0, 61, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.196721, -11.030397, 0.222222, ")", "(", 0.901639, -23.636458, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.932878, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.200364, 275.944967, 0.0, ")", "(", 0.994054, 140.332943, 0.0, ")", "(", 1.0, 140.986367, 0.0, ")", ")", "(", 4, "(", 0.0, 757.775354, 0.0, ")", "(", 0.524982, 207.575762, 0.0, ")", "(", 1.0, 207.575762, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 12.091505, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 80000.0, "(", 6800.0, 300000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.209792, 100.0, 0.0, 24, ")", "(", 1.0, -100.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.086061, -8.848579, 0.222222, ")", "(", 0.205504, -12.767751, 0.0, ")", "(", 0.412121, -3.394034, 0.0, ")", "(", 0.642424, -3.394034, 0.0, ")", "(", 0.928139, -28.606156, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.335983, 0.696295, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 1.0, 256, "obj-167", "bach.roll", "restore_preset", 2750, 250, 0.0, ")", "(", 0.825, 0.888888, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 275.944967, 0.0, ")", "(", 0.105, 140.332943, 0.0, ")", "(", 0.170371, 208.34275, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.459938, 182.332943, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.731666, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 379.593525, 0.0, ")", "(", 0.055453, 567.371304, 0.0, ")", "(", 0.146788, 535.856152, 0.0, ")", "(", 0.233863, 348.818499, 0.0, ")", "(", 0.290315, 239.088466, 0.0, ")", "(", 0.386244, 514.273049, 0.0, ")", "(", 0.453414, 346.765237, 0.0, ")", "(", 0.512303, 62.787681, 0.0, ")", "(", 0.623377, 408.481731, 0.0, ")", "(", 0.695, 86.765237, 0.0, ")", "(", 0.792162, 513.452529, 0.0, ")", "(", 0.864423, 525.351101, 0.0, ")", "(", 1.0, 453.12888, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 7800.0, 180000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -4.727368, 0.222222, ")", "(", 0.126407, -3.757671, 0.0, ")", "(", 0.256277, -14.424338, 0.0, ")", "(", 0.41039, -11.515247, 0.0, ")", "(", 0.45368, -16.363732, 0.0, ")", "(", 0.576623, -14.424338, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 3000, 250, 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6800.0, 240000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -4.727368, 0.222222, ")", "(", 0.126407, -3.757671, 0.0, ")", "(", 0.256277, -14.424338, 0.0, ")", "(", 0.41039, -11.515247, 0.0, ")", "(", 0.45368, -16.363732, 0.0, ")", "(", 0.576623, 256, "obj-167", "bach.roll", "restore_preset", 3250, 250, -14.424338, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 140000.0, "(", 7800.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", 256, "obj-167", "bach.roll", "restore_preset", 3500, 250, "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 3750, 250, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 150000.0, "(", 7300.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, 256, "obj-167", "bach.roll", "restore_preset", 4000, 250, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 160000.0, "(", 6800.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 256, "obj-167", "bach.roll", "restore_preset", 4250, 250, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 170000.0, "(", 6700.0, 120000.0, 100, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.012121, -8.606156, 0.222222, ")", "(", 0.126407, -7.636459, 0.0, ")", "(", 0.256277, -27.030399, 0.0, ")", "(", 0.41039, -24.121308, 0.0, ")", "(", 0.45368, -5.697065, 0.0, ")", "(", 0.576623, -3.757671, 0.0, ")", "(", 0.642609, -28.808155, 0.0, ")", "(", 0.851948, -29.93949, 0.0, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.041558, 0.258584, 0.0, ")", "(", 0.335983, 0.373063, 0.0, ")", "(", 0.439827, 0.4202, 0.0, ")", "(", 0.616513, 0.562962, 0.0, ")", "(", 0.73545, 0.515152, 0.0, ")", "(", 0.825, 0.404039, 0.0, ")", "(", 0.910091, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.105, 89.423851, 0.0, ")", "(", 0.170371, 157.433658, 0.0, ")", "(", 0.306879, 110.946766, 0.0, ")", "(", 0.459938, 121.242032, 0.0, ")", "(", 0.603465, 107.666274, 0.0, ")", "(", 0.732468, 111.908586, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 2000.0, 0.0, ")", "(", 0.055453, 392.727225, 0.0, ")", "(", 0.146788, 392.727225, 0.0, ")", "(", 0.233863, 2000.0, 0.0, ")", "(", 0.290315, 2000.0, 0.0, ")", "(", 0.386244, 266.666615, 0.0, ")", 256, "obj-167", "bach.roll", "restore_preset", 4500, 250, "(", 0.457143, 266.666615, 0.0, ")", "(", 0.512303, 2000.0, 0.0, ")", "(", 0.624247, 2000.0, 0.0, ")", "(", 0.695, 685.553134, 0.0, ")", "(", 0.761905, 585.754444, 0.0, ")", "(", 0.792162, 2000.0, 0.0, ")", "(", 0.864423, 2000.0, 0.0, ")", "(", 0.94026, 239.087767, 0.0, ")", "(", 1.0, 295.553118, 0.0, ")", ")", "(", 5, "(", 0.009785, "(", 0.07857, 0.157141, 0.07857, -1.220449, 0.53473, "(", "lowpass", 4572.183497, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 220000.0, "(", 6800.0, 200000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", "(", 0.704206, 1.0, 0.0, ")", "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 256, "obj-167", "bach.roll", "restore_preset", 4750, 250, 0.0, 207.575762, 0.0, ")", "(", 0.046021, 207.575762, 0.0, ")", "(", 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 224363.640048, "(", 7800.0, 195636.359952, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", "(", 0.704206, 1.0, 0.0, ")", "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 207.575762, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 5000, 250, 0.046021, 207.575762, 0.0, ")", "(", 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 260000.0, "(", 7300.0, 160000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 0.116466, -99.302319, 0.0, 24, ")", "(", 0.913332, 0.697681, 0.0, 92, ")", "(", 1.0, 0.697681, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.263011, -17.697064, 0.0, ")", "(", 0.408519, -21.697065, 0.0, ")", "(", 0.727977, -32.363732, 0.0, ")", "(", 0.921356, -11.111185, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.932878, 0.0, ")", "(", 0.008498, 1.0, 0.0, ")", "(", 0.257561, 0.696295, 0.0, ")", "(", 0.348543, 1.0, 0.0, ")", "(", 0.571222, 0.562962, 0.0, ")", "(", 0.704206, 1.0, 0.0, ")", "(", 0.804332, 0.888888, 0.0, ")", "(", 0.899472, 0.68148, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 140.986367, 0.0, ")", "(", 0.07239, 208.34275, 0.0, ")", "(", 0.225019, 172.037677, 0.0, ")", "(", 0.396155, 182.332943, 0.0, ")", "(", 0.556633, 107.666274, 0.0, ")", "(", 0.699975, 200.999612, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 207.575762, 0.0, ")", "(", 0.046021, 207.575762, 0.0, ")", "(", 256, "obj-167", "bach.roll", "restore_preset", 5250, 250, 0.14338, 1105.182158, 0.0, ")", "(", 0.206499, 743.330905, 0.0, ")", "(", 0.313758, 235.151463, 0.0, ")", "(", 0.388861, 235.151463, 0.0, ")", "(", 0.454705, 283.393748, 0.0, ")", "(", 0.57987, 398.949307, 0.0, ")", "(", 0.658978, 433.431914, 0.0, ")", "(", 0.767616, 860.119206, 0.0, ")", "(", 0.848411, 361.212072, 0.0, ")", "(", 1.0, 361.212072, 0.0, ")", ")", "(", 9, "(", 0.0, 12.091505, 0.0, ")", "(", 0.210821, 14.051387, 0.0, ")", "(", 0.385931, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 7400.0, 206727.275588, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.042216, -15.575854, -0.268889, ")", "(", 0.369128, -21.394036, -0.523636, ")", "(", 0.651007, -19.454642, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 16727.275588, "(", 6800.0, 146545.460701, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -154.675329, 0.0, 49, ")", ")", 256, "obj-167", "bach.roll", "restore_preset", 5500, 250, "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.254408, -18.606157, -0.142222, ")", "(", 0.650329, -19.575833, 0.115556, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.142365, 1.0, 0.0, ")", "(", 0.595523, 1.0, 0.0, ")", "(", 0.732233, 1.0, 0.0, ")", "(", 1.0, 1.0, 0.0, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.114265, 320.0, 0.0, ")", "(", 0.253196, 283.009419, 0.0, ")", "(", 0.481016, 264.514129, 0.0, ")", "(", 0.517959, 172.037677, 0.0, ")", "(", 1.0, 186.663499, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.061294, 1933.333385, 0.0, ")", "(", 0.376342, 1018.515489, 0.0, ")", "(", 0.671892, 1451.848835, 0.0, ")", "(", 0.916389, 1441.575601, 0.0, ")", "(", 1.0, 1363.702273, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 18.0, 0.0, 18.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.493329, 14.051387, 0.0, ")", "(", 0.797089, 9.73447, 0.0, ")", "(", 1.0, 10.499689, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 20000.0, "(", 6400.0, 370000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -11.948054, 0.0, 50, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.042216, -15.575854, -0.268889, ")", "(", 0.369128, -21.394036, -0.523636, ")", "(", 0.651007, -19.454642, 0.0, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 1.0, 0.351741, 0.0, ")", ")", "(", 3, "(", 0.0, 256, "obj-167", "bach.roll", "restore_preset", 5750, 250, 102.163457, 0.0, ")", "(", 0.5314, 317.944968, 0.0, ")", "(", 1.0, 318.439357, 0.0, ")", ")", "(", 4, "(", 0.0, 1009.517247, 0.0, ")", "(", 0.990099, 859.542286, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 65.982508, 0.0, 65.982508, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 26.0, 0.0, 26.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 1.0, 11.407546, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6300.0, 150000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -154.675329, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.254408, -18.606157, -0.142222, ")", "(", 0.650329, -19.575833, 0.115556, ")", "(", 1.0, -60.0, 0.0, ")", ")", "(", 2, "(", 0.0, 0.351741, 0.0, ")", "(", 0.142365, 1.0, 0.0, ")", "(", 0.595523, 1.0, 0.0, ")", "(", 0.732233, 1.0, 0.0, ")", "(", 1.0, 1.0, 0.0, ")", ")", "(", 3, "(", 0.0, 318.439357, 0.0, ")", "(", 0.114265, 320.0, 0.0, ")", "(", 0.253196, 283.009419, 0.0, ")", "(", 0.481016, 264.514129, 0.0, ")", "(", 0.517959, 172.037677, 0.0, ")", "(", 1.0, 186.663499, 0.0, ")", ")", "(", 4, "(", 0.0, 1524.611728, 0.0, ")", "(", 0.061294, 1933.333385, 0.0, ")", "(", 0.376342, 1018.515489, 0.0, ")", "(", 0.671892, 1451.848835, 0.0, ")", "(", 0.916389, 1441.575601, 0.0, ")", "(", 1.0, 1363.702273, 0.0, ")", ")", "(", 5, "(", 0.0, "(", 0.0, 0.0, 18.0, 0.0, 18.0, "(", "display", 0.0, 0.0, 0.0, ")", ")", ")", "(", 0.0, "(", 0.0, 0.0, 0.0, 0.0, 0.0, "(", "display", 0.0, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 6000, 250, 0.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 11.407546, 0.0, ")", "(", 0.493329, 14.051387, 0.0, ")", "(", 0.797089, 9.73447, 0.0, ")", "(", 1.0, 10.499689, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 40000.0, "(", 6100.0, 310000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 256, "obj-167", "bach.roll", "restore_preset", 6250, 250, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 90000.0, "(", 6600.0, 310000.0, 100, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 100, ")", "(", 1.0, 400.0, 0.0, 72, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.214143, -13.111187, -0.268889, ")", "(", 0.278075, -11.091006, 0.0, ")", "(", 0.353229, -9.757651, 0.0, ")", "(", 0.572578, -13.575758, -0.231111, ")", "(", 0.794393, -15.697045, -0.213333, ")", "(", 0.975, -35.777853, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.269174, 0.677777, 0.0, ")", "(", 0.417355, 0.718519, 0.0, ")", "(", 0.73545, 0.718519, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 256, "obj-167", "bach.roll", "restore_preset", 6500, 250, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.026483, "(", 0.057401, 0.114801, 0.057401, -1.359057, 0.58866, "(", "lowpass", 3820.513054, 0.0, 1.0, ")", ")", ")", "(", 0.081284, "(", 0.073769, 0.147538, 0.073769, -1.323349, 0.618424, "(", "lowpass", 4305.515347, 0.0, 1.220846, ")", ")", ")", "(", 0.190886, "(", 0.031903, 0.063805, 0.031903, -1.521205, 0.648815, "(", "lowpass", 2779.535701, 0.0, 0.905541, ")", ")", ")", "(", 0.50795, "(", 0.028469, 0.056937, 0.028469, -1.580763, 0.694638, "(", "lowpass", 2587.681116, 0.0, 1.0, ")", ")", ")", "(", 0.926788, "(", 0.066273, 0.132547, 0.066273, -1.299267, 0.56436, "(", "lowpass", 4146.078813, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 100000.0, "(", 6100.0, 220000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, 256, "obj-167", "bach.roll", "restore_preset", 6750, 250, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 170000.0, "(", 7100.0, 240000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -133.376617, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.110029, -13.757673, -0.133333, ")", "(", 0.468354, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 1.0, 0.0, ")", "(", 0.404956, 1.0, 0.0, ")", "(", 0.797685, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 186.663499, 0.0, ")", "(", 0.383534, 196.698065, 0.0, ")", "(", 0.71914, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1363.702273, 0.0, ")", "(", 0.15483, 1239.353373, 256, "obj-167", "bach.roll", "restore_preset", 7000, 250, 0.0, ")", "(", 0.532516, 1227.795985, 0.0, ")", "(", 0.99168, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.956574, "(", 0.172982, 0.0, -0.172982, -1.502355, 0.654036, "(", "bandpass", 3029.30318, 0.0, 1.0, ")", ")", ")", "(", 0.288097, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.499689, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 190000.0, "(", 5600.0, 180000.0, 51, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 51, ")", "(", 1.0, -133.376617, 0.0, 49, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.0, ")", "(", 0.110029, -13.757673, -0.133333, ")", "(", 0.468354, -11.636364, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 1.0, 0.0, ")", "(", 0.404956, 1.0, 0.0, ")", "(", 0.797685, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 186.663499, 0.0, ")", "(", 0.383534, 196.698065, 0.0, ")", "(", 0.71914, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 1363.702273, 0.0, ")", "(", 0.15483, 1239.353373, 0.0, ")", "(", 0.532516, 1227.795985, 0.0, ")", "(", 0.99168, 1111.664216, 0.0, ")", ")", "(", 5, "(", 0.956574, "(", 0.172982, 0.0, -0.172982, -1.502355, 0.654036, "(", "bandpass", 3029.30318, 0.0, 1.0, ")", ")", ")", "(", 0.288097, "(", 0.074207, 0.0, -0.074207, -1.780626, 0.851586, "(", "bandpass", 1949.422856, 0.0, 1.71036, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.499689, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", "(", 200000.0, "(", 7700.0, 220000.0, 19, "(", "breakpoints", "(", 0.0, 0.0, 0.0, 19, ")", "(", 1.0, 256, "obj-167", "bach.roll", "restore_preset", 7250, 250, 0.0, 0.0, 100, ")", ")", "(", "slots", "(", 1, "(", 0.0, -60.0, 0.5, ")", "(", 0.080183, -36.181894, -0.268889, ")", "(", 0.219061, -29.070783, 0.035556, ")", "(", 0.395671, -15.939491, -0.231111, ")", "(", 0.557736, -24.222298, 0.0, ")", "(", 0.761284, -24.222298, -0.213333, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.0, 0.5, ")", "(", 0.113228, 1.0, 0.0, ")", "(", 0.346869, 1.0, 0.0, ")", "(", 0.417355, 1.0, 0.0, ")", "(", 0.73545, 1.0, 0.0, ")", "(", 0.910053, 1.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.013639, "(", 0.023766, 0.047531, 0.023766, -1.623142, 0.718204, "(", "lowpass", 2345.651034, 0.0, 1.0, ")", ")", ")", "(", 0.929101, "(", 0.001322, 0.002643, 0.001322, -1.923364, 0.92865, "(", "lowpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 9, "(", 0.0, 10.993571, 0.0, ")", "(", 0.29418, 14.051387, 0.0, ")", "(", 0.450794, 9.73447, 0.0, ")", "(", 1.0, 13.751601, 0.0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", 0.0, "(", 5900.0, 420000.0, 100, "(", "slots", "(", 1, 256, "obj-167", "bach.roll", "restore_preset", 7500, 250, "(", 0.0, -60.0, 0.5, ")", "(", 0.414815, -19.809501, -0.268889, ")", "(", 0.83714, -32.285691, -0.222222, ")", "(", 1.0, -60.0, 0.18, ")", ")", "(", 2, "(", 0.0, 0.41515, 0.5, ")", "(", 0.113228, 0.614141, 0.0, ")", "(", 0.346869, 0.614141, 0.0, ")", "(", 0.417355, 0.0, 0.0, ")", "(", 0.560847, 0.0, 0.0, ")", "(", 0.592593, 0.0, 0.0, ")", "(", 0.73545, 0.0, 0.0, ")", "(", 0.910053, 0.0, 0.0, ")", "(", 1.0, 0.0, 0.5, ")", ")", "(", 3, "(", 0.0, 102.163457, 0.0, ")", "(", 0.021164, 317.944968, 0.0, ")", "(", 0.09874, 320.0, 0.0, ")", "(", 0.170371, 283.009419, 0.0, ")", "(", 0.287831, 264.514129, 0.0, ")", "(", 0.306879, 172.037677, 0.0, ")", "(", 0.725926, 196.698065, 0.0, ")", "(", 0.875133, 139.157162, 0.0, ")", "(", 1.0, 72.818898, 0.0, ")", ")", "(", 4, "(", 0.0, 113.961665, 0.0, ")", "(", 0.071429, 200.0, 0.0, ")", "(", 0.233863, 180.737686, 0.0, ")", "(", 0.386244, 180.737686, 0.0, ")", "(", 0.512303, 170.464452, 0.0, ")", "(", 0.624247, 170.464452, 0.0, ")", "(", 0.792162, 158.907064, 0.0, ")", "(", 1.0, 108.825048, 0.0, ")", ")", "(", 5, "(", 0.02035, "(", 0.036199, 0.0, -0.036199, -1.922155, 0.927601, "(", "bandpass", 527.72961, 0.0, 1.0, ")", ")", ")", "(", 0.084656, "(", 0.031267, 0.0, -0.031267, -1.931441, 0.937467, "(", "bandpass", 553.702425, 0.0, 1.220846, ")", ")", ")", "(", 0.186244, "(", 0.057309, 0.0, -0.057309, -1.87392, 0.885383, "(", "bandpass", 774.334781, 0.0, 0.905541, ")", ")", ")", "(", 0.513228, "(", 0.041473, 0.0, -0.041473, -1.909862, 0.917053, "(", "bandpass", 608.130261, 0.0, 1.0, ")", ")", ")", "(", 0.795767, "(", 0.022313, 0.0, -0.022313, -1.949407, 0.955374, "(", "bandpass", 548.496423, 0.0, 1.71036, ")", ")", ")", "(", 0.929101, "(", 0.035675, 0.0, -0.035675, -1.923364, 0.92865, 256, "obj-167", "bach.roll", "restore_preset", 7750, 250, "(", "bandpass", 519.785179, 0.0, 1.0, ")", ")", ")", ")", "(", 6, "(", 0.0, -70.0, 0.0, ")", "(", 0.046133, -55.011207, 0.0, ")", "(", 0.073627, -27.663058, 0.195556, ")", "(", 0.092733, -62.37417, 0.0, ")", "(", 0.143061, -65.529726, 0.0, ")", "(", 0.197116, -63.426022, 0.0, ")", "(", 0.226474, -25.559354, 0.115556, ")", "(", 0.247444, -56.063059, 0.204444, ")", "(", 0.312684, -62.37417, 0.0, ")", "(", 0.338313, -27.663058, 0.177778, ")", "(", 0.372331, -52.907503, -0.106667, ")", "(", 0.430115, -62.37417, 0.0, ")", "(", 0.495354, -51.855651, 0.0, ")", "(", 0.53077, -24.507502, 0.0, ")", "(", 0.681287, -21.351947, 0.0, ")", "(", 0.733945, -65.529726, 0.0, ")", "(", 0.823416, -69.737133, 0.0, ")", "(", 0.873278, -47.648244, 0.0, ")", "(", 0.955759, -68.685281, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 7, "(", 0.0, -70.0, 0.0, ")", "(", 0.038677, -33.974169, 0.0, ")", "(", 0.202708, -50.803799, 0.0, ")", "(", 0.318276, -22.403799, 0.0, ")", "(", 0.534498, -62.637037, 0.0, ")", "(", 0.709713, -58.166763, 0.0, ")", "(", 0.79732, -17.144539, 0.0, ")", "(", 0.961351, -55.011207, 0.0, ")", "(", 1.0, -70.0, 0.0, ")", ")", "(", 8, "(", 0.0, 17.367672, 0.0, ")", "(", 0.491626, 10.0, 0.0, ")", "(", 1.0, 17.367672, 0.0, ")", ")", "(", 9, "(", 0.0, 16.0, 0.0, ")", "(", 0.121666, 16.0, 0.0, ")", "(", 0.185281, 11.135353, 0.0, ")", "(", 0.34632, 11.135353, 0.0, ")", "(", 0.377489, 9.0, 0.0, ")", "(", 0.47619, 9.0, 0.0, ")", "(", 0.511664, 14.868687, 0.0, ")", "(", 0.557576, 14.734332, 0.0, ")", "(", 0.593939, 9.0, 0.0, ")", "(", 0.694372, 9.0, 0.0, ")", "(", 0.755847, 11.73164, 0.0, ")", "(", 0.8, 11.792917, 0.0, ")", "(", 0.868333, 9.0, 0.0, ")", "(", 0.996301, 9.0, 0.0, ")", 14, "obj-167", "bach.roll", "restore_preset", 8000, 8, ")", ")", 0, ")", 0, ")", 0, ")", 4, "obj-167", "bach.roll", "end_preset", 5, "obj-136", "toggle", "int", 0, 5, "obj-76", "number", "int", 0, 5, "obj-29", "umenu", "int", 17, 5, "obj-7", "live.gain~", "float", -70.0, 5, "obj-52", "toggle", "int", 1, 5, "obj-55", "live.gain~", "float", 0.0, 5, "obj-102", "toggle", "int", 0, 5, "<invalid>", "flonum", "float", 0.0, 5, "obj-84", "number", "int", 67, 5, "obj-106", "number", "int", 68 ]
						}
 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"channels" : 1,
					"id" : "obj-55",
					"maxclass" : "live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 846.547852, 1975.333496, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1187.833374, 780.704346, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[4]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[3]"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"bgcolor2" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.870588, 0.415686, 0.062745, 1.0 ],
					"bgfillcolor_color2" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-54",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 966.547852, 1676.666748, 35.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1004.05957, 355.537659, 35.0, 22.0 ],
					"style" : "",
					"text" : "read"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.92549, 0.364706, 0.341176, 1.0 ],
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 34.0, 79.0, 1612.0, 937.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-98",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 945.0, 1113.0, 51.0, 35.0 ],
									"style" : "",
									"text" : "recall 9 9 1."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-75",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1186.374878, 540.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1186.374878, 513.10614, 76.0, 22.0 ],
									"style" : "",
									"text" : "snapshot~ 5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 1186.374878, 485.0, 47.0, 22.0 ],
									"style" : "",
									"text" : "curve~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1186.374878, 457.0, 133.0, 22.0 ],
									"style" : "",
									"text" : "bach.slot2curve @out t"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"hint" : "bach_control",
									"id" : "obj-34",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1359.374878, 435.10611, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 194.0, 181.401581, 80.0, 13.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 502.75, 68.084244, 80.0, 13.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"maxclass" : "meter~",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 22.510864, 181.401581, 80.0, 13.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 502.75, 17.89389, 80.0, 13.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-111",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1073.208252, 916.0, 46.166626, 22.0 ],
									"style" : "",
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-109",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 996.374878, 666.0, 92.625122, 22.0 ],
									"style" : "",
									"text" : "t b l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 996.374878, 579.553101, 64.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.rot 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "bang", "bang", "" ],
									"patching_rect" : [ 996.374878, 865.0, 162.166748, 22.0 ],
									"style" : "",
									"text" : "t l b b l"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1415.374878, 1027.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "pow $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 996.374878, 829.0, 438.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.pick 1 2 3 @out t"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-50",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 996.374878, 797.0, 66.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.flat 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-47",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 996.374878, 764.0, 81.0, 22.0 ],
									"style" : "",
									"text" : "bach.filternull"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 4,
									"outlettype" : [ "int", "", "", "int" ],
									"patching_rect" : [ 996.374878, 701.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "counter"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 996.374878, 733.0, 92.625122, 22.0 ],
									"style" : "",
									"text" : "bach.lookup"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1070.0, 701.0, 81.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.group 3"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 996.374878, 619.0, 123.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.slice -1 @out nt"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 996.374878, 532.10614, 113.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.collect"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "", "int" ],
									"patching_rect" : [ 996.374878, 457.0, 113.0, 22.0 ],
									"style" : "",
									"text" : "t b l 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1043.374878, 493.0, 95.0, 22.0 ],
									"style" : "",
									"text" : "bach.slot2curve"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-110",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 996.374878, 426.0, 183.0, 22.0 ],
									"saved_object_attributes" : 									{
										"versionnumber" : 80001
									}
,
									"style" : "",
									"text" : "bach.playkeys duration (slot 9 8)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-102",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 901.5, 1259.0, 87.0, 22.0 ],
									"style" : "",
									"text" : "prepend recall"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"hint" : "bach_control",
									"id" : "obj-85",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 996.374878, 386.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-79",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 178.135986, 430.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1006.874878, 976.0, 24.0, 24.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 468.75, 27.89389, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 13.0,
									"id" : "obj-42",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1415.374878, 1058.0, 167.0, 23.0 ],
									"style" : "",
									"text" : "prepend interp dis_function"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-127",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1233.0, 1349.0, 50.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 415.75, 166.0, 50.0, 20.0 ],
									"style" : "",
									"text" : "presets"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1205.874878, 1065.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"linecount" : 9,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 428.0, 228.0, 19.0, 127.0 ],
									"presentation" : 1,
									"presentation_linecount" : 9,
									"presentation_rect" : [ 77.374893, 22.215576, 21.0, 127.0 ],
									"style" : "",
									"text" : "s\ny\nn\nt\nh\ne\ns\ni\ns"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 549.0, 395.0, 54.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 155.635986, 164.60611, 56.0, 20.0 ],
									"style" : "",
									"text" : "analysis"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 738.0, 344.0, 24.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 252.0, 165.10611, 24.0, 20.0 ],
									"style" : "",
									"text" : "48"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 697.0, 344.0, 24.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 73.374893, 5.0, 24.0, 20.0 ],
									"style" : "",
									"text" : "48"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 740.5, 370.0, 19.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 79.374893, 150.0, 19.0, 20.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 697.0, 370.0, 19.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 87.0, 165.10611, 19.0, 20.0 ],
									"style" : "",
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-108",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1295.0, 1325.0, 65.0, 22.0 ],
									"style" : "",
									"text" : "writeagain"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-107",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1240.0, 1325.0, 36.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 403.5, 147.0, 36.0, 22.0 ],
									"style" : "",
									"text" : "write"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-103",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1037.5, 1206.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 303.25, 27.89389, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1027.0, 1184.106201, 105.5, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 275.5, 8.0, 105.5, 20.0 ],
									"style" : "",
									"text" : "RECALL PRESET"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1027.0, 1234.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-101",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1214.374878, 954.0, 33.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 420.75, 93.10611, 33.0, 20.0 ],
									"style" : "",
									"text" : "time"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-100",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1149.041626, 954.0, 31.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 421.75, 51.084244, 31.0, 20.0 ],
									"style" : "",
									"text" : "end"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-99",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1079.708252, 954.0, 37.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 418.75, 10.89389, 37.0, 20.0 ],
									"style" : "",
									"text" : "start "
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"maxclass" : "number",
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1205.874878, 976.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 412.25, 110.10611, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"maxclass" : "number",
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1139.541626, 976.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 412.25, 68.084244, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"maxclass" : "number",
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1073.208252, 976.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 412.25, 27.89389, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"rect" : [ 84.0, 129.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-7",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 258.0, 138.633301, 33.0, 22.0 ],
													"style" : "",
													"text" : "stop"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-12",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 258.0, 108.633301, 41.0, 22.0 ],
													"style" : "",
													"text" : "r stop"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "float", "float" ],
													"patching_rect" : [ 167.5, 203.049957, 29.5, 22.0 ],
													"style" : "",
													"text" : "t f f"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "bang", "" ],
													"patching_rect" : [ 167.5, 240.466614, 36.0, 22.0 ],
													"style" : "",
													"text" : "sel 1"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-2",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 167.5, 300.633301, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-3",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 16.5, 40.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-1",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 120.5, 40.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-40",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 40.5, 369.633301, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-35",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 40.5, 340.466614, 87.0, 22.0 ],
													"style" : "",
													"text" : "prepend recall"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 40.5, 312.633301, 51.0, 22.0 ],
													"style" : "",
													"text" : "zl.rot -1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-27",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 167.5, 108.633301, 29.5, 22.0 ],
													"style" : "",
													"text" : "i"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-25",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 167.5, 138.633301, 47.0, 22.0 ],
													"style" : "",
													"text" : "0, 1 $1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-23",
													"maxclass" : "newobj",
													"numinlets" : 3,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 167.5, 169.633301, 43.0, 22.0 ],
													"style" : "",
													"text" : "line 0."
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-18",
													"maxclass" : "newobj",
													"numinlets" : 3,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 40.5, 283.466614, 59.0, 22.0 ],
													"style" : "",
													"text" : "pack f i 1"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-41",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 60.5, 40.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-42",
													"index" : 4,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 177.5, 40.0, 30.0, 30.0 ],
													"style" : ""
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-18", 2 ],
													"midpoints" : [ 130.0, 187.0, 90.0, 187.0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-7", 0 ],
													"source" : [ "obj-12", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-34", 0 ],
													"source" : [ "obj-18", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-5", 0 ],
													"source" : [ "obj-23", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-23", 0 ],
													"source" : [ "obj-25", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-25", 0 ],
													"source" : [ "obj-27", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-35", 0 ],
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-40", 0 ],
													"source" : [ "obj-35", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-2", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-18", 1 ],
													"source" : [ "obj-41", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-27", 1 ],
													"source" : [ "obj-42", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-18", 0 ],
													"source" : [ "obj-5", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-4", 0 ],
													"source" : [ "obj-5", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-23", 0 ],
													"source" : [ "obj-7", 0 ]
												}

											}
 ],
										"styles" : [ 											{
												"name" : "AudioStatus_Menu",
												"default" : 												{
													"bgfillcolor" : 													{
														"type" : "color",
														"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
														"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
														"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
														"angle" : 270.0,
														"proportion" : 0.39,
														"autogradient" : 0
													}

												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "Max_class_18",
												"default" : 												{
													"fontsize" : [ 18.0 ]
												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "newobjBlue-1",
												"default" : 												{
													"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "newobjBrown-1",
												"default" : 												{
													"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "newobjCyan-1",
												"default" : 												{
													"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "newobjGreen-1",
												"default" : 												{
													"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "newobjYellow-1",
												"default" : 												{
													"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
													"fontsize" : [ 12.059008 ]
												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "numberGold-1",
												"default" : 												{
													"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
												}
,
												"parentstyle" : "",
												"multi" : 0
											}
, 											{
												"name" : "panelViolet",
												"default" : 												{
													"bgfillcolor" : 													{
														"type" : "color",
														"color" : [ 0.372549, 0.196078, 0.486275, 0.2 ],
														"color1" : [ 0.454902, 0.462745, 0.482353, 1.0 ],
														"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
														"angle" : 270.0,
														"proportion" : 0.39,
														"autogradient" : 0
													}

												}
,
												"parentstyle" : "",
												"multi" : 0
											}
 ]
									}
,
									"patching_rect" : [ 1006.874878, 1027.0, 218.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "p preset-interp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 796.0, 203.5, 69.0, 22.0 ],
									"restore" : [ 20 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr follow",
									"varname" : "follow"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-87",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 1510.5, 1234.0, 61.0, 22.0 ],
									"style" : "",
									"text" : "route text"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"keymode" : 1,
									"maxclass" : "textedit",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "", "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1510.5, 1176.106201, 100.0, 50.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 278.25, 151.10611, 100.0, 20.0 ],
									"style" : "",
									"text" : "zigzag"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1419.0, 1228.0, 51.0, 22.0 ],
									"style" : "",
									"text" : "pack i s"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-81",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1454.5, 1176.106201, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 303.25, 130.10611, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-82",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1419.0, 1144.106201, 100.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 278.25, 111.10611, 100.0, 20.0 ],
									"style" : "",
									"text" : "PRESET NAME"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1419.0, 1194.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1419.0, 1263.0, 93.0, 22.0 ],
									"style" : "",
									"text" : "slotname $1 $2"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.309495, 0.299387, 0.299789, 1.0 ],
									"id" : "obj-69",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1336.5, 1234.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 303.25, 201.10611, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-70",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1281.0, 1263.0, 60.0, 22.0 ],
									"style" : "",
									"text" : "delete $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1281.0, 1184.106201, 105.5, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 275.5, 182.10611, 105.5, 20.0 ],
									"style" : "",
									"text" : "DELETE PRESET",
									"textoncolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1281.0, 1234.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1167.5, 1206.0, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 300.5, 78.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1157.0, 1263.0, 54.0, 22.0 ],
									"style" : "",
									"text" : "store $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1157.0, 1184.106201, 100.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 275.5, 58.10611, 100.0, 20.0 ],
									"style" : "",
									"text" : "STORE PRESET"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-57",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 1157.0, 1234.0, 29.5, 22.0 ],
									"style" : "",
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-51",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1118.0, 1291.0, 79.0, 22.0 ],
									"style" : "",
									"text" : "clientwindow"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "textbutton",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1011.0, 1259.0, 108.5, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 401.25, 182.10611, 103.0, 35.5 ],
									"style" : "",
									"text" : "SHOW PRESETS"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1011.0, 1291.0, 91.0, 22.0 ],
									"style" : "",
									"text" : "storagewindow"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "", "" ],
									"patching_rect" : [ 419.5, 160.7789, 89.0, 22.0 ],
									"restore" : [ 2, 2, 4, 5, 7, 10, 12, 14, 16, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 24, 24, 24, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 28, 28, 29, 30, 31, 32, 33, 34, 43 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"style" : "",
									"text" : "pattr voicemap",
									"varname" : "voicemap"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1011.0, 1325.0, 207.0, 22.0 ],
									"saved_object_attributes" : 									{
										"client_rect" : [ 4, 44, 358, 172 ],
										"parameter_enable" : 0,
										"storage_rect" : [ 583, 69, 1051, 286 ]
									}
,
									"style" : "",
									"text" : "pattrstorage chan-voc @savemode 0",
									"varname" : "chan-voc"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 759.0, 261.853455, 69.0, 20.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 91.0, 189.60611, 69.0, 20.0 ],
									"style" : "",
									"text" : "follow (ms)"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-39",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 178.135986, 243.735046, 100.0, 22.0 ],
									"style" : "",
									"text" : "delay~ 4800 640"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-35",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"rect" : [ 575.0, 265.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-6",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 161.0, 75.0, 22.0 ],
													"style" : "",
													"text" : "speedlim 10"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-3",
													"linecount" : 2,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 338.0, 255.144287, 96.0, 35.0 ],
													"style" : "",
													"text" : ";\r$1-voicemap $2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "list" ],
													"patching_rect" : [ 338.0, 194.0, 68.0, 22.0 ],
													"style" : "",
													"text" : "listfunnel 1"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-70",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 176.0, 228.144287, 47.0, 22.0 ],
													"style" : "",
													"text" : "s reset"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-69",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 176.0, 197.420105, 56.0, 22.0 ],
													"style" : "",
													"text" : "deferlow"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-42",
													"maxclass" : "newobj",
													"numinlets" : 5,
													"numoutlets" : 4,
													"outlettype" : [ "int", "", "", "int" ],
													"patching_rect" : [ 50.0, 168.0, 87.0, 22.0 ],
													"style" : "",
													"text" : "counter 0 1 48"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-41",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "bang", "int" ],
													"patching_rect" : [ 50.0, 131.420105, 32.5, 22.0 ],
													"style" : "",
													"text" : "t b i"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-40",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 2,
													"outlettype" : [ "", "" ],
													"patching_rect" : [ 50.0, 100.0, 49.0, 22.0 ],
													"style" : "",
													"text" : "zl iter 1"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-32",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 197.420105, 107.0, 22.0 ],
													"style" : "",
													"text" : "pack i i"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-10",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 4,
													"outlettype" : [ "", "", "", "" ],
													"patching_rect" : [ 50.0, 228.144287, 83.0, 22.0 ],
													"saved_object_attributes" : 													{
														"embed" : 0
													}
,
													"style" : "",
													"text" : "coll voicemap"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-34",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 40.0, 25.0, 25.0 ],
													"style" : ""
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-32", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-34", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-41", 0 ],
													"source" : [ "obj-40", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-32", 1 ],
													"midpoints" : [ 73.0, 159.0, 147.5, 159.0 ],
													"source" : [ "obj-41", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-42", 0 ],
													"source" : [ "obj-41", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-32", 0 ],
													"source" : [ "obj-42", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-69", 0 ],
													"source" : [ "obj-42", 3 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-1", 0 ],
													"source" : [ "obj-6", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-70", 0 ],
													"source" : [ "obj-69", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 454.5, 403.450958, 72.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "p voicemap"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 702.5, 203.5, 79.0, 22.0 ],
									"style" : "",
									"text" : "loadmess 20"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-33",
									"maxclass" : "number",
									"minimum" : 10,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 702.5, 233.901581, 50.0, 22.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 172.646851, 188.60611, 78.0, 22.0 ],
									"style" : "",
									"varname" : "number"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 702.5, 261.853455, 51.0, 22.0 ],
									"style" : "",
									"text" : "s follow"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-71",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 95.135986, 44.215569, 45.0, 22.0 ],
									"style" : "",
									"text" : "r reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "multislider",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 454.5, 203.5, 208.0, 185.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 91.0, 8.0, 179.0, 160.0 ],
									"setminmax" : [ 1.0, 48.0 ],
									"settype" : 0,
									"size" : 48,
									"style" : "",
									"varname" : "multislider"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 178.135986, 321.735046, 139.0, 22.0 ],
									"style" : "",
									"text" : "poly~ vocvoice_syn~ 48"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "spectroscope~",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 311.5, 435.10611, 300.0, 100.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 181.5, 229.696899, 366.0, 100.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-16",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 318.135986, 176.901581, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-14",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 318.135986, 211.513947, 54.0, 22.0 ],
									"style" : "",
									"text" : "open $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"maxclass" : "live.gain~",
									"numinlets" : 2,
									"numoutlets" : 5,
									"orientation" : 1,
									"outlettype" : [ "signal", "signal", "", "float", "list" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 178.135986, 355.196899, 136.0, 48.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 8.374893, 281.696899, 136.0, 48.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_longname" : "live.gain~[3]",
											"parameter_shortname" : "live.gain~",
											"parameter_type" : 0,
											"parameter_mmin" : -70.0,
											"parameter_mmax" : 6.0,
											"parameter_initial" : [ 0.0 ],
											"parameter_unitstyle" : 4
										}

									}
,
									"varname" : "live.gain~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 52.510864, 28.974777, 36.0, 36.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 8.374893, 176.60611, 25.5, 25.5 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 178.135986, 285.735046, 133.0, 22.0 ],
									"style" : "",
									"text" : "poly~ vocvoice_an~ 48"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 7,
											"minor" : 3,
											"revision" : 4,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"rect" : [ 789.0, 211.0, 581.0, 286.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"boxes" : [ 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-9",
													"linecount" : 4,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 12.807392, 93.611557, 98.0, 62.0 ],
													"style" : "",
													"text" : ";\rfreq_base 65.4;\ranQ_base 30;\rsynQ_base 60;\r"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-8",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 3,
													"outlettype" : [ "bang", "bang", "bang" ],
													"patching_rect" : [ 13.118731, 55.318439, 46.0, 22.0 ],
													"style" : "",
													"text" : "t b b b"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-6",
													"linecount" : 4,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 126.625427, 93.611557, 97.0, 62.0 ],
													"style" : "",
													"text" : ";\rfreq_ratio 1.11;\ranQ_ratio 0.98;\rsynQ_ratio 0.97"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-4",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 38.118732, 9.211809, 60.0, 22.0 ],
													"style" : "",
													"text" : "loadbang"
												}

											}
, 											{
												"box" : 												{
													"fontname" : "Arial",
													"fontsize" : 12.0,
													"id" : "obj-3",
													"linecount" : 2,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 238.753311, 93.611557, 98.0, 35.0 ],
													"style" : "",
													"text" : ";\rto_thispoly bang"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-1",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 13.118731, 9.211809, 25.0, 25.0 ],
													"style" : ""
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"source" : [ "obj-1", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-8", 0 ],
													"source" : [ "obj-4", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-8", 2 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-8", 1 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-9", 0 ],
													"source" : [ "obj-8", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 52.510864, 70.926636, 36.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"style" : "",
										"tags" : ""
									}
,
									"style" : "",
									"text" : "p init"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"hint" : "Target",
									"id" : "obj-2",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 113.374878, 140.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"hint" : "Source",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 170.874893, 140.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"grad1" : [ 0.52549, 0.062745, 0.003922, 1.0 ],
									"grad2" : [ 0.341176, 0.027451, 0.023529, 1.0 ],
									"id" : "obj-7",
									"maxclass" : "panel",
									"mode" : 1,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 678.0, 404.0, 128.0, 128.0 ],
									"presentation" : 1,
									"presentation_rect" : [ 1.0, -4.0, 625.0, 391.0 ],
									"proportion" : 0.39,
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"order" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"midpoints" : [ 180.374893, 313.0, 187.635986, 313.0 ],
									"order" : 1,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 1 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-105", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-106", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-107", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-108", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-109", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"source" : [ "obj-109", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 1 ],
									"midpoints" : [ 1087.874878, 486.0, 1078.208211, 486.0 ],
									"source" : [ "obj-110", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"order" : 1,
									"source" : [ "obj-110", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 1 ],
									"source" : [ "obj-110", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"order" : 0,
									"source" : [ "obj-110", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"source" : [ "obj-111", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-35", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"midpoints" : [ 327.635986, 276.624496, 187.635986, 276.624496 ],
									"order" : 1,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"midpoints" : [ 327.635986, 314.624512, 187.635986, 314.624512 ],
									"order" : 0,
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"order" : 1,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-39", 0 ],
									"midpoints" : [ 122.874878, 232.0, 187.635986, 232.0 ],
									"order" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-21", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 2 ],
									"source" : [ "obj-21", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 1 ],
									"source" : [ "obj-28", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 1 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-47", 0 ],
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-39", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-42", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-45", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-32", 0 ],
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-51", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-46", 0 ],
									"midpoints" : [ 1215.374878, 1099.0, 981.687439, 1099.0, 981.687439, 690.0, 1005.874878, 690.0 ],
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"source" : [ "obj-53", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"source" : [ "obj-53", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-57", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 0 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-42", 0 ],
									"source" : [ "obj-62", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 0 ],
									"source" : [ "obj-63", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 1 ],
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-63", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"source" : [ "obj-63", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-57", 1 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 1 ],
									"source" : [ "obj-69", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-70", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-70", 0 ],
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 1,
									"midpoints" : [ 1195.874878, 572.0, 1463.937439, 572.0, 1463.937439, 222.901581, 712.0, 222.901581 ],
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 1 ],
									"source" : [ "obj-81", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-83", 0 ],
									"source" : [ "obj-82", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-110", 0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 1 ],
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-88", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"midpoints" : [ 187.635986, 350.686005, 321.0, 350.686005 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"order" : 0,
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-93", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-98", 1 ],
									"order" : 1,
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 1 ],
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 2 ],
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 3 ],
									"source" : [ "obj-97", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "Max_class_18",
								"default" : 								{
									"fontsize" : [ 18.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBlue-1",
								"default" : 								{
									"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjBrown-1",
								"default" : 								{
									"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjCyan-1",
								"default" : 								{
									"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjGreen-1",
								"default" : 								{
									"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "newobjYellow-1",
								"default" : 								{
									"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
									"fontsize" : [ 12.059008 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "numberGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "panelViolet",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.372549, 0.196078, 0.486275, 0.2 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 1.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 846.547852, 1757.666626, 139.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1181.833374, 355.537659, 63.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p vocoder",
					"varname" : "vocoder"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 329.52356, 1549.333374, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 329.52356, 1580.0, 65.0, 22.0 ],
					"style" : "",
					"text" : "metro 200"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 873.52356, 1676.666748, 50.0, 35.0 ],
					"style" : "",
					"text" : "1 1 1 1 1 1 1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 696.559631, 1610.0, 81.0, 22.0 ],
					"style" : "",
					"text" : "prepend note"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 588.571411, 1682.0, 50.0, 35.0 ],
					"style" : "",
					"text" : "1 1 1 1 1 1 1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 329.52356, 1610.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "mutemap 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 466.571442, 1610.0, 81.0, 22.0 ],
					"style" : "",
					"text" : "prepend note"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1188.571411, 1594.666626, 43.0, 22.0 ],
					"style" : "",
					"text" : "s stop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1300.071411, 1556.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1188.571411, 1558.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"channels" : 1,
					"id" : "obj-7",
					"maxclass" : "live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 630.0, 1976.559814, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1087.05957, 780.704346, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[2]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1921.0, 458.666656, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1490.0, 26.666668, 49.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 900.55957, 12.268829, 49.0, 22.0 ],
					"style" : "",
					"text" : "clearall"
				}

			}
, 			{
				"box" : 				{
					"alignviewbounds" : 0,
					"autobounds" : 0,
					"autoupdate" : 120.0,
					"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"bufferchooser_bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"bufferchooser_fgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bufferchooser_position" : 1,
					"bufferchooser_shape" : "buttons",
					"bufferchooser_size" : 15,
					"bufferchooser_visible" : 1,
					"cursor_color" : [ 1.0, 0.0, 0.0, 1.0 ],
					"cursor_followmouse" : 0,
					"cursor_position" : -1.0,
					"cursor_shape" : "bar",
					"cursor_size" : 3,
					"cursor_visible" : 1,
					"domain_bounds" : [ -16.326531, 97863.650794 ],
					"domainruler_bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"domainruler_fgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"domainruler_grid" : 0,
					"domainruler_position" : 0,
					"domainruler_size" : 15,
					"domainruler_unit" : 0,
					"domainruler_visible" : 1,
					"domainscrollbar_color" : [ 1.0, 1.0, 1.0, 1.0 ],
					"domainscrollbar_size" : 10,
					"domainscrollbar_visible" : 1,
					"embed" : 0,
					"externalfiles" : 1,
					"id" : "obj-43",
					"layout" : 0,
					"maxclass" : "imubu",
					"name" : "Target",
					"numinlets" : 1,
					"numoutlets" : 1,
					"opacity" : 0.0,
					"opacityprogressive" : 0,
					"orientation" : 0,
					"outlettype" : [ "" ],
					"outputkeys" : 0,
					"outputmouse" : 0,
					"outputselection" : 0,
					"outputtimeselection" : 0,
					"outputvalues" : 0,
					"patching_rect" : [ 1490.0, 69.0, 542.0, 325.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 656.55957, 40.268829, 466.0, 274.0 ],
					"rangeruler_grid" : 0,
					"rangeruler_size" : 30,
					"rangeruler_visible" : 1,
					"region_bounds" : [ 0.0, 0.0 ],
					"region_color" : [ 0.8, 0.7, 0.7, 1.0 ],
					"region_visible" : 1,
					"split_color" : [ 1.0, 0.0, 0.0, 1.0 ],
					"split_size" : 2,
					"split_visible" : 1,
					"tabs_position" : 0,
					"tabs_size" : 20,
					"tabs_visible" : 1,
					"toolbar_bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"toolbar_position" : 1,
					"toolbar_size" : 30,
					"toolbar_visible" : 1,
					"useplaceholders" : 1,
					"windresize" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1049.571411, 1484.75, 46.5, 22.0 ],
					"style" : "",
					"text" : "t l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 466.571442, 1558.0, 478.97641, 22.0 ],
					"style" : "",
					"text" : "gate 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1314.0, 283.25, 81.0, 22.0 ],
					"style" : "",
					"text" : "route append"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1314.0, 149.208328, 29.5, 22.0 ],
					"style" : "",
					"text" : "t l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1192.0, 313.533295, 141.0, 22.0 ],
					"style" : "",
					"text" : "combine a b @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1192.0, 348.899963, 169.0, 22.0 ],
					"style" : "",
					"text" : "readappend $1 @name audio"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1374.0, 222.499985, 95.0, 23.0 ],
					"style" : "",
					"text" : "prepend types"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"items" : [ "AIFF", ",", "APPL", "(application)", ",", "BMP", ",", "DATA", ",", "FLAC", ",", "fold", ",", "GIFf", ",", "JPEG", ",", "JSON", ",", "MPEG", ",", "Midi", ",", "MooV", ",", "PICT", ",", "PNG", ",", "TEXT", ",", "TIFF", ",", "VfW", "(AVI", "file)", ",", "WAVE", ",", "aPcs", "(audio", "plugin)", ",", "iLaF", "(Max", "external", "-", "Mac)", ",", "iLaX", "(Max", "external", "-", "Win)" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1336.0, 191.833328, 95.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1153.220093, 201.541656, 95.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-30",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1321.375, 105.833328, 120.0, 50.0 ],
					"presentation" : 1,
					"presentation_linecount" : 3,
					"presentation_rect" : [ 1149.720093, 131.583328, 102.0, 50.0 ],
					"style" : "",
					"text" : "drop a folder here for source concatenation"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "dropfile",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1314.0, 94.083328, 134.75, 44.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 1133.345093, 113.625, 134.75, 85.916672 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 1314.0, 255.499985, 130.0, 23.0 ],
					"style" : "",
					"text" : "folder C74:/help/max"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.741176, 0.196078, 1.0 ],
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"patching_rect" : [ 696.559631, 1641.333496, 148.0, 22.0 ],
					"style" : "",
					"text" : "poly~ PolyConcatTarget 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 1077.071411, 1517.0, 242.0, 22.0 ],
					"saved_object_attributes" : 					{
						"versionnumber" : 80001
					}
,
					"style" : "",
					"text" : "bach.playkeys midichannel stop end @out t"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.741176, 0.196078, 1.0 ],
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"patching_rect" : [ 466.571442, 1641.333496, 152.0, 22.0 ],
					"style" : "",
					"text" : "poly~ PolyConcatSource 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1595.333374, 694.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1252.000122, 715.333252, 29.5, 22.0 ],
					"style" : "",
					"text" : "!- 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1595.333374, 721.333252, 57.0, 22.0 ],
					"style" : "",
					"text" : "dump $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1529.000122, 721.333252, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1184.000122, 721.333252, 33.0, 22.0 ],
					"style" : "",
					"text" : "stop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1144.833374, 721.333252, 33.0, 22.0 ],
					"style" : "",
					"text" : "play"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1149.333374, 694.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1149.333374, 668.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1010.833374, 565.704346, 53.0, 53.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 853.666626, 636.333252, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1010.833374, 496.602081, 55.0, 55.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 852.666626, 697.333252, 47.0, 22.0 ],
					"style" : "",
					"text" : "s reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 282.000153, 743.0, 88.0, 22.0 ],
					"style" : "",
					"text" : "dumpselection"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-162",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1478.333374, 718.333374, 33.0, 22.0 ],
					"style" : "",
					"text" : "next"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1435.333374, 718.333374, 34.0, 22.0 ],
					"style" : "",
					"text" : "prev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1392.333374, 718.333374, 41.0, 22.0 ],
					"style" : "",
					"text" : "dump"
				}

			}
, 			{
				"box" : 				{
					"bgslots" : [ 1, 2, 3, 4 ],
					"breakpointshavevelocity" : 1,
					"bwcompatibility" : 70911,
					"clefs" : [ "FG", "FG", "FG" ],
					"defaultnoteslots" : [ "null" ],
					"enharmonictable" : [ "default", "default", "default" ],
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"grid" : 1,
					"hidevoices" : [ 0, 0, 0 ],
					"id" : "obj-167",
					"keys" : [ "CM", "CM", "CM" ],
					"loop" : [ 0.0, 211963.248542 ],
					"maxclass" : "bach.roll",
					"midichannels" : [ 1, 2, 3 ],
					"numinlets" : 6,
					"numoutlets" : 8,
					"numparts" : [ 1, 1, 1 ],
					"numvoices" : 3,
					"out" : "nnnnnnn",
					"outlettype" : [ "", "", "", "", "", "", "", "bang" ],
					"patching_rect" : [ 255.0, 808.666565, 946.0, 575.0 ],
					"pitcheditrange" : [ "null" ],
					"presentation" : 1,
					"presentation_rect" : [ 18.0, 347.537659, 927.0, 575.0 ],
					"ruler" : 1,
					"rulerlabels" : 1,
					"showplayhead" : 1,
					"showstems" : 0,
					"showvelocity" : 1,
					"snaponset" : 1,
					"snaptail" : 1,
					"stafflines" : [ 5, 5, 5 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"versionnumber" : 80001,
					"voicenames" : [ "(", ")", "(", ")", "(", ")" ],
					"voicespacing" : [ 0.0, 17.0, 17.0, 17.0 ],
					"whole_roll_data_0000000000" : [ "roll", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 2, "(", "name", "relative dim", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 3, "(", "name", "Attack-Release", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075052544, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1078525952, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1087735936, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 858993459, 1071854387, ")", "(", "representation", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 6, "(", "name", "Source level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226566656, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 7, "(", "name", "Target Level", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226566656, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 8, "(", "name", "morph", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1076101120, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1076887552, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "relative", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 10, "(", "name", "slot llll", ")", "(", "type", "llll", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 20, "(", "name", "dynamics", ")", "(", "type", "dynamics", ")", "(", "key", "d", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079083008, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 21, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", "l", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 22, "(", "name", "articulations", ")", "(", "type", "articulations", ")", "(", "key", "a", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079738368, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 23, "(", "name", "notehead", ")", "(", "type", "notehead", ")", "(", "key", "h", ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079738368, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", "(", "follownotehead", 0, ")", ")", ")", "(", "commands", "(", 1, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 2, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 3, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 4, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", "(", 5, "(", "note", "note", ")", "(", "chord", "chord", ")", "(", "rest", "rest", ")", "(", "key", 0, ")", ")", ")", "(", "groups", ")", "(", "markers", ")", "(", "midichannels", 1, 2, 3, ")", "(", "articulationinfo", ")", "(", "noteheadinfo", ")", "(", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086351360, "_x_x_x_x_bach_float64_x_x_x_x_", 1034090686, 1089771706, 100, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3459613337, 3227069606, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 61, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1521998971, 1070149159, "_x_x_x_x_bach_float64_x_x_x_x_", 840026884, 3223719824, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 1070363077, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 137988709, 1072486970, "_x_x_x_x_bach_float64_x_x_x_x_", 3914811151, 3224871662, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 4138012011, 1072552482, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 228973296, 1070179719, "_x_x_x_x_bach_float64_x_x_x_x_", 2511834314, 1081163550, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1435412430, 1072680778, "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1080134311, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1367792465, 1080139664, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3972810389, 1082633779, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 220177203, 1071697063, "_x_x_x_x_bach_float64_x_x_x_x_", 2758674674, 1080685164, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2758674674, 1080685164, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 2712220160, 1082667693, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3192706889, 1076375257, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1087604736, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086351360, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091719040, 100, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3648729337, 1070258806, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1079574528, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 24, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3227058176, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4235593668, 1068894231, "_x_x_x_x_bach_float64_x_x_x_x_", 4065994000, 3223433848, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 1070363077, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2140749139, 1070222836, "_x_x_x_x_bach_float64_x_x_x_x_", 2830692686, 3223947542, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3258952465, 1071276080, "_x_x_x_x_bach_float64_x_x_x_x_", 1279006901, 3221956347, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3334818767, 1071943356, "_x_x_x_x_bach_float64_x_x_x_x_", 1279006901, 3221956347, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2405731442, 1072542544, "_x_x_x_x_bach_float64_x_x_x_x_", 170149424, 3225197357, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 3217846725, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2633880104, 1069350018, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3611345941, 1070956734, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1072056332, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3203702005, 1071298033, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2022826517, 1071889017, "_x_x_x_x_bach_float64_x_x_x_x_", 3797713162, 1071776712, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1717986918, 1072326246, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 1072460229, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 690768180, 1072504695, "_x_x_x_x_bach_float64_x_x_x_x_", 622598459, 1072025263, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1066773507, "_x_x_x_x_bach_float64_x_x_x_x_", 2511834314, 1081163550, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2920577761, 1069211975, "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1080134311, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 3470333575, 1080691447, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3406561901, 1071476639, "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1080478375, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3572038401, 1071861653, "_x_x_x_x_bach_float64_x_x_x_x_", 1001655093, 1079700132, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3501394779, 1072130510, "_x_x_x_x_bach_float64_x_x_x_x_", 3528332814, 1080631292, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 336725436, 1081588095, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3941474308, 1068262485, "_x_x_x_x_bach_float64_x_x_x_x_", 1849378558, 1082243832, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4256759267, 1069730290, "_x_x_x_x_bach_float64_x_x_x_x_", 1714963261, 1082179289, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 2456308976, 1081462040, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1570927238, 1070765189, "_x_x_x_x_bach_float64_x_x_x_x_", 3064338907, 1080943316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 877685157, 1082135087, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 660806488, 1071449276, "_x_x_x_x_bach_float64_x_x_x_x_", 1764166407, 1081453630, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 3139655453, 1078944978, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1384285139, 1071903412, "_x_x_x_x_bach_float64_x_x_x_x_", 730900355, 1081706421, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2748779069, 1072053616, "_x_x_x_x_bach_float64_x_x_x_x_", 2761698331, 1079357689, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 3347463151, 1082133406, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1818042477, 1072408922, "_x_x_x_x_bach_float64_x_x_x_x_", 235570366, 1082157775, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3833172412, 1081889295, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2012106279, 1065617960, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1068768561, "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1069817137, "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1068768561, "_x_x_x_x_bach_float64_x_x_x_x_", 400248959, 3220408053, "_x_x_x_x_bach_float64_x_x_x_x_", 413295899, 1071717507, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 4188589546, 1085398062, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090021888, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086351360, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091390464, 100, "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 348270308, 1065931493, "_x_x_x_x_bach_float64_x_x_x_x_", 674275506, 3222464723, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 1070363077, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3313378290, 1069559322, "_x_x_x_x_bach_float64_x_x_x_x_", 3492873564, 3222146997, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2775442226, 1070622423, "_x_x_x_x_bach_float64_x_x_x_x_", 3566265965, 3224164674, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1797701511, 1071268820, "_x_x_x_x_bach_float64_x_x_x_x_", 1953282407, 3223783374, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3602274971, 1071450391, "_x_x_x_x_bach_float64_x_x_x_x_", 2320794168, 3224395037, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 333701779, 1071805362, "_x_x_x_x_bach_float64_x_x_x_x_", 3566265965, 3224164674, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3219370046, 1071943744, "_x_x_x_x_bach_float64_x_x_x_x_", 1056905552, 3225210595, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1941737535, 1072382760, "_x_x_x_x_bach_float64_x_x_x_x_", 1789455174, 3225284738, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 365312738, 1067796247, "_x_x_x_x_bach_float64_x_x_x_x_", 3889247505, 1070632099, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3611345941, 1070956734, "_x_x_x_x_bach_float64_x_x_x_x_", 2719367133, 1071112259, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 624522605, 1071392288, "_x_x_x_x_bach_float64_x_x_x_x_", 2322718314, 1071309966, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2022826517, 1071889017, "_x_x_x_x_bach_float64_x_x_x_x_", 3797713162, 1071776712, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 202310140, 1071676448, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1717986918, 1072326246, "_x_x_x_x_bach_float64_x_x_x_x_", 1691598639, 1071242182, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 690768180, 1072504695, "_x_x_x_x_bach_float64_x_x_x_x_", 622598459, 1072025263, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2920577761, 1069211975, "_x_x_x_x_bach_float64_x_x_x_x_", 1609685023, 1079401248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2260595907, 1080274400, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 3496721854, 1079753879, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3406561901, 1071476639, "_x_x_x_x_bach_float64_x_x_x_x_", 1942562168, 1079922557, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3572038401, 1071861653, "_x_x_x_x_bach_float64_x_x_x_x_", 1001655093, 1079700132, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3140205209, 1072132192, "_x_x_x_x_bach_float64_x_x_x_x_", 1172629151, 1079769638, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3941474308, 1068262485, "_x_x_x_x_bach_float64_x_x_x_x_", 3064888662, 1081641890, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4256759267, 1069730290, "_x_x_x_x_bach_float64_x_x_x_x_", 3064888662, 1081641890, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1570927238, 1070765189, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 1954381918, 1081125546, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3064338907, 1071464916, "_x_x_x_x_bach_float64_x_x_x_x_", 1954381918, 1081125546, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2748779069, 1072053616, "_x_x_x_x_bach_float64_x_x_x_x_", 3515138674, 1082485868, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2553615756, 1072193926, "_x_x_x_x_bach_float64_x_x_x_x_", 435131727, 1082281481, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1818042477, 1072408922, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 599233837, 1072567964, "_x_x_x_x_bach_float64_x_x_x_x_", 4240266593, 1080943310, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2453835075, 1081243865, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2012106279, 1065617960, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1068768561, "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1069817137, "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1068768561, "_x_x_x_x_bach_float64_x_x_x_x_", 400248959, 3220408053, "_x_x_x_x_bach_float64_x_x_x_x_", 413295899, 1071717507, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 4188589546, 1085398062, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090590464, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086351360, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090341888, 100, "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 348270308, 1065931493, "_x_x_x_x_bach_float64_x_x_x_x_", 340298849, 3223402074, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 1070363077, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3313378290, 1069559322, "_x_x_x_x_bach_float64_x_x_x_x_", 3900242622, 3223227323, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2775442226, 1070622423, "_x_x_x_x_bach_float64_x_x_x_x_", 982963395, 3225094088, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1797701511, 1071268820, "_x_x_x_x_bach_float64_x_x_x_x_", 176471616, 3224903438, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3602274971, 1071450391, "_x_x_x_x_bach_float64_x_x_x_x_", 1749597878, 3222718923, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 333701779, 1071805362, "_x_x_x_x_bach_float64_x_x_x_x_", 3492873564, 3222146997, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3219370046, 1071943744, "_x_x_x_x_bach_float64_x_x_x_x_", 1056905552, 3225210595, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1941737535, 1072382760, "_x_x_x_x_bach_float64_x_x_x_x_", 1789455174, 3225284738, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 365312738, 1067796247, "_x_x_x_x_bach_float64_x_x_x_x_", 3889247505, 1070632099, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3611345941, 1070956734, "_x_x_x_x_bach_float64_x_x_x_x_", 2719367133, 1071112259, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 624522605, 1071392288, "_x_x_x_x_bach_float64_x_x_x_x_", 2322718314, 1071309966, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2022826517, 1071889017, "_x_x_x_x_bach_float64_x_x_x_x_", 3797713162, 1071776712, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 202310140, 1071676448, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1717986918, 1072326246, "_x_x_x_x_bach_float64_x_x_x_x_", 1691598639, 1071242182, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 690768180, 1072504695, "_x_x_x_x_bach_float64_x_x_x_x_", 622598459, 1072025263, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2920577761, 1069211975, "_x_x_x_x_bach_float64_x_x_x_x_", 1609685023, 1079401248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2260595907, 1080274400, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 3496721854, 1079753879, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3406561901, 1071476639, "_x_x_x_x_bach_float64_x_x_x_x_", 1942562168, 1079922557, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3572038401, 1071861653, "_x_x_x_x_bach_float64_x_x_x_x_", 1001655093, 1079700132, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3140205209, 1072132192, "_x_x_x_x_bach_float64_x_x_x_x_", 1172629151, 1079769638, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3941474308, 1068262485, "_x_x_x_x_bach_float64_x_x_x_x_", 3064888662, 1081641890, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4256759267, 1069730290, "_x_x_x_x_bach_float64_x_x_x_x_", 3064888662, 1081641890, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1570927238, 1070765189, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 1954381918, 1081125546, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3064338907, 1071464916, "_x_x_x_x_bach_float64_x_x_x_x_", 1954381918, 1081125546, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2748779069, 1072053616, "_x_x_x_x_bach_float64_x_x_x_x_", 3515138674, 1082485868, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2553615756, 1072193926, "_x_x_x_x_bach_float64_x_x_x_x_", 435131727, 1082281481, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1818042477, 1072408922, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1084178432, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 599233837, 1072567964, "_x_x_x_x_bach_float64_x_x_x_x_", 4240266593, 1080943310, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2453835075, 1081243865, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2012106279, 1065617960, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1068768561, "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1069817137, "_x_x_x_x_bach_float64_x_x_x_x_", 3520563259, 1068768561, "_x_x_x_x_bach_float64_x_x_x_x_", 400248959, 3220408053, "_x_x_x_x_bach_float64_x_x_x_x_", 413295899, 1071717507, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 4188589546, 1085398062, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 517045343, 1091265373, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086351360, "_x_x_x_x_bach_float64_x_x_x_x_", 3777921953, 1091035554, 100, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1025019715, 1069404343, "_x_x_x_x_bach_float64_x_x_x_x_", 835353959, 3227046745, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 24, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 130841884, 1072511492, "_x_x_x_x_bach_float64_x_x_x_x_", 448875622, 1072059239, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 92, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 448875622, 1072059239, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 383729558, 1070650668, "_x_x_x_x_bach_float64_x_x_x_x_", 3377149965, 3224482418, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3761429279, 1071260972, "_x_x_x_x_bach_float64_x_x_x_x_", 3658624941, 3224744562, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1810345895, 1072122774, "_x_x_x_x_bach_float64_x_x_x_x_", 3307880732, 3225431694, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2482972133, 1072528319, "_x_x_x_x_bach_float64_x_x_x_x_", 1032166541, 3223730413, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 3217846725, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 4138012011, 1072552482, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1084668221, 1065445222, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 569272145, 1070627809, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1072056332, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1284504459, 1071009415, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1545088715, 1071794035, "_x_x_x_x_bach_float64_x_x_x_x_", 3797713162, 1071776712, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 91534343, 1072072923, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1986267756, 1072282902, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 1072460229, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2163564006, 1072482425, "_x_x_x_x_bach_float64_x_x_x_x_", 622598459, 1072025263, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1367792465, 1080139664, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2861479011, 1068664870, "_x_x_x_x_bach_float64_x_x_x_x_", 3470333575, 1080691447, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 788349837, 1070386540, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2152294011, 1071209114, "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1080478375, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 39582419, 1071763440, "_x_x_x_x_bach_float64_x_x_x_x_", 1001655093, 1079700132, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4171272238, 1072064049, "_x_x_x_x_bach_float64_x_x_x_x_", 3528332814, 1080631292, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_" ],
					"whole_roll_data_0000000001" : [ 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 2758674674, 1080685164, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2212217395, 1067946000, "_x_x_x_x_bach_float64_x_x_x_x_", 2758674674, 1080685164, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2641576686, 1069701702, "_x_x_x_x_bach_float64_x_x_x_x_", 2275439314, 1083262138, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 701763296, 1070231183, "_x_x_x_x_bach_float64_x_x_x_x_", 2978302122, 1082604197, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1865871232, 1070863516, "_x_x_x_x_bach_float64_x_x_x_x_", 3371102651, 1080911064, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1064052378, 1071178521, "_x_x_x_x_bach_float64_x_x_x_x_", 3371102651, 1080911064, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1374390, 1071454691, "_x_x_x_x_bach_float64_x_x_x_x_", 3400789465, 1081194060, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2277363459, 1071812171, "_x_x_x_x_bach_float64_x_x_x_x_", 1552510418, 1081667376, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 131666517, 1071978073, "_x_x_x_x_bach_float64_x_x_x_x_", 514296564, 1081808617, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1845255389, 1072205903, "_x_x_x_x_bach_float64_x_x_x_x_", 575044581, 1082843380, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3545375244, 1072375342, "_x_x_x_x_bach_float64_x_x_x_x_", 2778465883, 1081512804, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2778465883, 1081512804, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3192706889, 1076375257, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3123162779, 1070267438, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4024487436, 1071166231, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086121984, "_x_x_x_x_bach_float64_x_x_x_x_", 879196985, 1091124282, 51, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 51, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1434038041, 3223840103, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 50, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3871105563, 1067818325, "_x_x_x_x_bach_float64_x_x_x_x_", 1440909988, 3224315606, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 201485506, 1071095755, "_x_x_x_x_bach_float64_x_x_x_x_", 2333438552, 3224724703, "_x_x_x_x_bach_float64_x_x_x_x_", 1222656930, 3219177888, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2714694209, 1071961356, "_x_x_x_x_bach_float64_x_x_x_x_", 1795777366, 3224597603, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2934596535, 1071022828, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2460157267, 1071710522, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2603918412, 1081337607, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1382360994, 1083149347, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 421112953, 1072672484, "_x_x_x_x_bach_float64_x_x_x_x_", 2584402081, 1082842198, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1765540796, 1079017185, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1765540796, 1079017185, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 2432669476, 1082215838, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3501669657, 1072924821, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077542912, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077542912, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 1036289709, 1082162646, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3733666610, 1076285609, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2738608587, 1087395281, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085968384, "_x_x_x_x_bach_float64_x_x_x_x_", 2944663938, 1090642827, 51, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 51, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1267736907, 3227735452, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 49, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2113261349, 1070614584, "_x_x_x_x_bach_float64_x_x_x_x_", 451624401, 3224541997, "_x_x_x_x_bach_float64_x_x_x_x_", 2606942069, 3217175636, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3277094407, 1071959934, "_x_x_x_x_bach_float64_x_x_x_x_", 3399415075, 3224605545, "_x_x_x_x_bach_float64_x_x_x_x_", 4175120529, 1069389075, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 2934596535, 1071022828, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 764160581, 1069693188, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1075872128, 1071844998, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3867257273, 1072131699, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 2603918412, 1081337607, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2517881628, 1069367416, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4276000720, 1070609500, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1429639994, 1071565047, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3233113941, 1071682334, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1648442808, 1080513851, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1758668849, 1083691634, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1524747750, 1068458477, "_x_x_x_x_bach_float64_x_x_x_x_", 1658888168, 1084110165, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3246857837, 1071126012, "_x_x_x_x_bach_float64_x_x_x_x_", 3098698645, 1083167775, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2798531971, 1072005155, "_x_x_x_x_bach_float64_x_x_x_x_", 889230029, 1083617125, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 103628971, 1072517903, "_x_x_x_x_bach_float64_x_x_x_x_", 1784232494, 1083606605, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 547831669, 1083526863, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077018624, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077018624, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 1414796587, 1084379981, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 756464000, 1085288980, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3733666610, 1076285609, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3427452622, 1071616691, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3395291907, 1072267712, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1016223622, 1076166615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1087604736, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085865984, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091999040, 51, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 51, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1434038041, 3223840103, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 50, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3871105563, 1067818325, "_x_x_x_x_bach_float64_x_x_x_x_", 1440909988, 3224315606, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 201485506, 1071095755, "_x_x_x_x_bach_float64_x_x_x_x_", 2333438552, 3224724703, "_x_x_x_x_bach_float64_x_x_x_x_", 1222656930, 3219177888, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2714694209, 1071961356, "_x_x_x_x_bach_float64_x_x_x_x_", 1795777366, 3224597603, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2934596535, 1071022828, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2460157267, 1071710522, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2603918412, 1081337607, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1382360994, 1083149347, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 421112953, 1072672484, "_x_x_x_x_bach_float64_x_x_x_x_", 2584402081, 1082842198, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1765540796, 1079017185, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1765540796, 1079017185, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 1197917918, 1084602130, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 512097541, 1072495153, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077542912, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077542912, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 3989303063, 1085329795, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3501669657, 1072924821, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3733666610, 1076285609, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1088653312, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085840384, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090670464, 51, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 51, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1267736907, 3227735452, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 49, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2113261349, 1070614584, "_x_x_x_x_bach_float64_x_x_x_x_", 451624401, 3224541997, "_x_x_x_x_bach_float64_x_x_x_x_", 2606942069, 3217175636, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3277094407, 1071959934, "_x_x_x_x_bach_float64_x_x_x_x_", 3399415075, 3224605545, "_x_x_x_x_bach_float64_x_x_x_x_", 4175120529, 1069389075, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 2934596535, 1071022828, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 764160581, 1069693188, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1075872128, 1071844998, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3867257273, 1072131699, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 2603918412, 1081337607, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2517881628, 1069367416, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4276000720, 1070609500, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1429639994, 1071565047, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3233113941, 1071682334, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1648442808, 1080513851, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1758668849, 1083691634, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1524747750, 1068458477, "_x_x_x_x_bach_float64_x_x_x_x_", 1658888168, 1084110165, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3246857837, 1071126012, "_x_x_x_x_bach_float64_x_x_x_x_", 3098698645, 1083167775, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2798531971, 1072005155, "_x_x_x_x_bach_float64_x_x_x_x_", 889230029, 1083617125, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 103628971, 1072517903, "_x_x_x_x_bach_float64_x_x_x_x_", 1784232494, 1083606605, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 547831669, 1083526863, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077018624, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1077018624, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 835903715, 1082951719, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "display", "_x_x_x_x_bach_float64_x_x_x_x_", 19516331, 1084126641, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1921396570, 1073438114, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3733666610, 1076285609, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3427452622, 1071616691, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3395291907, 1072267712, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1016223622, 1076166615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1088653312, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085789184, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091759040, 100, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081671680, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 72, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2933222145, 1070295305, "_x_x_x_x_bach_float64_x_x_x_x_", 2158066447, 3223992557, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 364213227, 1070713851, "_x_x_x_x_bach_float64_x_x_x_x_", 1453554372, 3223727768, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3468684308, 1071029069, "_x_x_x_x_bach_float64_x_x_x_x_", 3572863034, 3223553002, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 420288320, 1071796879, "_x_x_x_x_bach_float64_x_x_x_x_", 3232289308, 3224053449, "_x_x_x_x_bach_float64_x_x_x_x_", 2506061878, 3217921291, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3731192709, 1072262058, "_x_x_x_x_bach_float64_x_x_x_x_", 353218110, 3224331491, "_x_x_x_x_bach_float64_x_x_x_x_", 3910413104, 3217772158, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 858993459, 1072640819, "_x_x_x_x_bach_float64_x_x_x_x_", 2951089209, 3225543568, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 3217846725, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2633880104, 1069350018, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2512109192, 1070676517, "_x_x_x_x_bach_float64_x_x_x_x_", 1679778889, 1072017497, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3203702005, 1071298033, "_x_x_x_x_bach_float64_x_x_x_x_", 2396110715, 1072102939, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 2396110715, 1072102939, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1072504615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1066773507, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1322162732, 1069106950, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3069286709, 1070754770, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 699014517, 1072118473, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3956592593, 1072431382, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3948621133, 1079803275, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3271321971, 1068648747, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1080623104, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 2870275104, 1080286470, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2518706261, 1079719117, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 194613558, 1067130460, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2509844463, 1068327838, "_x_x_x_x_bach_float64_x_x_x_x_", 2509844463, 1069376414, "_x_x_x_x_bach_float64_x_x_x_x_", 2509844463, 1068327838, "_x_x_x_x_bach_float64_x_x_x_x_", 2389328953, 3220553394, "_x_x_x_x_bach_float64_x_x_x_x_", 3258635374, 1071830604, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2936245802, 1085135110, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 967845110, 1068814087, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2834004230, 1068688005, "_x_x_x_x_bach_float64_x_x_x_x_", 2834004230, 1069736581, "_x_x_x_x_bach_float64_x_x_x_x_", 2834004230, 1068688005, "_x_x_x_x_bach_float64_x_x_x_x_", 2766443011, 3220515951, "_x_x_x_x_bach_float64_x_x_x_x_", 507437192, 1071893026, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 3989303063, 1085329795, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3501669657, 1072924821, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3550597924, 1070100211, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3778191141, 1067472264, "_x_x_x_x_bach_float64_x_x_x_x_", 3778191141, 1068520840, "_x_x_x_x_bach_float64_x_x_x_x_", 3778191141, 1067472264, "_x_x_x_x_bach_float64_x_x_x_x_", 3806144971, 3220723418, "_x_x_x_x_bach_float64_x_x_x_x_", 4261870429, 1071956759, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 1197917918, 1084602130, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 512097541, 1072495153, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1539316279, 1071661344, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 517590741, 1067263711, "_x_x_x_x_bach_float64_x_x_x_x_", 517590741, 1068312287, "_x_x_x_x_bach_float64_x_x_x_x_", 517590741, 1067263711, "_x_x_x_x_bach_float64_x_x_x_x_", 2494401295, 3220785870, "_x_x_x_x_bach_float64_x_x_x_x_", 221663224, 1072052857, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 3141304721, 1084503900, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1321887854, 1072539711, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3542813351, 1068562249, "_x_x_x_x_bach_float64_x_x_x_x_", 3542813351, 1069610825, "_x_x_x_x_bach_float64_x_x_x_x_", 3542813351, 1068562249, "_x_x_x_x_bach_float64_x_x_x_x_", 251745367, 3220490700, "_x_x_x_x_bach_float64_x_x_x_x_", 127413761, 1071779645, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 756464000, 1085288980, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1089861888, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085917184, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091759040, 100, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081671680, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 72, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2933222145, 1070295305, "_x_x_x_x_bach_float64_x_x_x_x_", 2158066447, 3223992557, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 364213227, 1070713851, "_x_x_x_x_bach_float64_x_x_x_x_", 1453554372, 3223727768, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3468684308, 1071029069, "_x_x_x_x_bach_float64_x_x_x_x_", 3572863034, 3223553002, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 420288320, 1071796879, "_x_x_x_x_bach_float64_x_x_x_x_", 3232289308, 3224053449, "_x_x_x_x_bach_float64_x_x_x_x_", 2506061878, 3217921291, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3731192709, 1072262058, "_x_x_x_x_bach_float64_x_x_x_x_", 353218110, 3224331491, "_x_x_x_x_bach_float64_x_x_x_x_", 3910413104, 3217772158, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 858993459, 1072640819, "_x_x_x_x_bach_float64_x_x_x_x_", 2951089209, 3225543568, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 3217846725, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2633880104, 1069350018, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2512109192, 1070676517, "_x_x_x_x_bach_float64_x_x_x_x_", 1679778889, 1072017497, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3203702005, 1071298033, "_x_x_x_x_bach_float64_x_x_x_x_", 2396110715, 1072102939, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 2396110715, 1072102939, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1072504615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1066773507, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1322162732, 1069106950, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3069286709, 1070754770, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 699014517, 1072118473, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3956592593, 1072431382, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3948621133, 1079803275, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3271321971, 1068648747, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1080623104, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 2870275104, 1080286470, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2518706261, 1079719117, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 194613558, 1067130460, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2509844463, 1068327838, "_x_x_x_x_bach_float64_x_x_x_x_", 2509844463, 1069376414, "_x_x_x_x_bach_float64_x_x_x_x_", 2509844463, 1068327838, "_x_x_x_x_bach_float64_x_x_x_x_", 2389328953, 3220553394, "_x_x_x_x_bach_float64_x_x_x_x_", 3258635374, 1071830604, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2936245802, 1085135110, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 967845110, 1068814087, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2834004230, 1068688005, "_x_x_x_x_bach_float64_x_x_x_x_", 2834004230, 1069736581, "_x_x_x_x_bach_float64_x_x_x_x_", 2834004230, 1068688005, "_x_x_x_x_bach_float64_x_x_x_x_", 2766443011, 3220515951, "_x_x_x_x_bach_float64_x_x_x_x_", 507437192, 1071893026, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 3989303063, 1085329795, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3501669657, 1072924821, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3550597924, 1070100211, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3778191141, 1067472264, "_x_x_x_x_bach_float64_x_x_x_x_", 3778191141, 1068520840, "_x_x_x_x_bach_float64_x_x_x_x_", 3778191141, 1067472264, "_x_x_x_x_bach_float64_x_x_x_x_", 3806144971, 3220723418, "_x_x_x_x_bach_float64_x_x_x_x_", 4261870429, 1071956759, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 1197917918, 1084602130, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 512097541, 1072495153, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1539316279, 1071661344, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 517590741, 1067263711, "_x_x_x_x_bach_float64_x_x_x_x_", 517590741, 1068312287, "_x_x_x_x_bach_float64_x_x_x_x_", 517590741, 1067263711, "_x_x_x_x_bach_float64_x_x_x_x_", 2494401295, 3220785870, "_x_x_x_x_bach_float64_x_x_x_x_", 221663224, 1072052857, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 3141304721, 1084503900, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1321887854, 1072539711, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3542813351, 1068562249, "_x_x_x_x_bach_float64_x_x_x_x_", 3542813351, 1069610825, "_x_x_x_x_bach_float64_x_x_x_x_", 3542813351, 1068562249, "_x_x_x_x_bach_float64_x_x_x_x_", 251745367, 3220490700, "_x_x_x_x_bach_float64_x_x_x_x_", 127413761, 1071779645, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 756464000, 1085288980, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090021888, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085789184, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091230464, 19, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 19, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2192701064, 1068795615, "_x_x_x_x_bach_float64_x_x_x_x_", 1299622744, 3225556808, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3681164930, 1070336560, "_x_x_x_x_bach_float64_x_x_x_x_", 3584957662, 3225227806, "_x_x_x_x_bach_float64_x_x_x_x_", 1650092075, 1067594853, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1967026302, 1071207084, "_x_x_x_x_bach_float64_x_x_x_x_", 4141860302, 3224363268, "_x_x_x_x_bach_float64_x_x_x_x_", 2506061878, 3217921291, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 721004750, 1071765753, "_x_x_x_x_bach_float64_x_x_x_x_", 2240804697, 3224910056, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1130297953, 1072192624, "_x_x_x_x_bach_float64_x_x_x_x_", 2240804697, 3224910056, "_x_x_x_x_bach_float64_x_x_x_x_", 3910413104, 3217772158, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2633880104, 1069350018, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 146784802, 1071002394, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3203702005, 1071298033, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1072504615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1066773507, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1322162732, 1069106950, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3069286709, 1070754770, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 699014517, 1072118473, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3956592593, 1072431382, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3948621133, 1079803275, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3271321971, 1068648747, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1080623104, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 2870275104, 1080286470, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2518706261, 1079719117, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2543445273, 1066135235, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 954245957, 1066948096, "_x_x_x_x_bach_float64_x_x_x_x_", 954245957, 1067996672, "_x_x_x_x_bach_float64_x_x_x_x_", 954245957, 1066948096, "_x_x_x_x_bach_float64_x_x_x_x_", 2056497830, 3220830307, "_x_x_x_x_bach_float64_x_x_x_x_", 4232276405, 1072102278, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 1414796587, 1084379981, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 87411174, 1072544562, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4106969941, 1062577953, "_x_x_x_x_bach_float64_x_x_x_x_", 4106969941, 1063626529, "_x_x_x_x_bach_float64_x_x_x_x_", 4106969941, 1062577953, "_x_x_x_x_bach_float64_x_x_x_x_", 1602803369, 3221145113, "_x_x_x_x_bach_float64_x_x_x_x_", 50021401, 1072543617, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 200111116, 1082146376, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090830464, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086045184, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091390464, 51, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 51, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1058554820, 3227560973, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 49, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1285329093, 1069296348, "_x_x_x_x_bach_float64_x_x_x_x_", 3072860122, 3224077293, "_x_x_x_x_bach_float64_x_x_x_x_", 1161634035, 3217101070, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 238868901, 1071511939, "_x_x_x_x_bach_float64_x_x_x_x_", 2156966936, 3223799249, "_x_x_x_x_bach_float64_x_x_x_x_", 3910413104, 3217772158, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2450811418, 1071246028, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2976927732, 1072268962, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1648442808, 1080513851, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 817486895, 1071156178, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3819428517, 1072104241, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 547831669, 1083526863, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 758663023, 1069797752, "_x_x_x_x_bach_float64_x_x_x_x_", 3667695912, 1083399529, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4271052918, 1071712862, "_x_x_x_x_bach_float64_x_x_x_x_", 380705901, 1083387695, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2986548459, 1072675799, "_x_x_x_x_bach_float64_x_x_x_x_", 675100139, 1083268776, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 331777634, 1072602177, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3823738540, 1069950021, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3823738540, 3217433669, "_x_x_x_x_bach_float64_x_x_x_x_", 2630478976, 3220703653, "_x_x_x_x_bach_float64_x_x_x_x_", 235614378, 1071967709, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 979939738, 1084730011, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1715787895, 1070755886, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2229934005, 1068695356, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 2229934005, 3216179004, "_x_x_x_x_bach_float64_x_x_x_x_", 1171944421, 3220995441, "_x_x_x_x_bach_float64_x_x_x_x_", 3737483794, 1072382000, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 19516331, 1084126641, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1921396570, 1073438114, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1016223622, 1076166615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090990464, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085661184, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1090910464, 51, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 51, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 1058554820, 3227560973, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 49, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1285329093, 1069296348, "_x_x_x_x_bach_float64_x_x_x_x_", 3072860122, 3224077293, "_x_x_x_x_bach_float64_x_x_x_x_", 1161634035, 3217101070, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 238868901, 1071511939, "_x_x_x_x_bach_float64_x_x_x_x_", 2156966936, 3223799249, "_x_x_x_x_bach_float64_x_x_x_x_", 3910413104, 3217772158, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2450811418, 1071246028, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2976927732, 1072268962, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1648442808, 1080513851, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 817486895, 1071156178, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3819428517, 1072104241, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 547831669, 1083526863, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 758663023, 1069797752, "_x_x_x_x_bach_float64_x_x_x_x_", 3667695912, 1083399529, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4271052918, 1071712862, "_x_x_x_x_bach_float64_x_x_x_x_", 380705901, 1083387695, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2986548459, 1072675799, "_x_x_x_x_bach_float64_x_x_x_x_", 675100139, 1083268776, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 331777634, 1072602177, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3823738540, 1069950021, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3823738540, 3217433669, "_x_x_x_x_bach_float64_x_x_x_x_", 2630478976, 3220703653, "_x_x_x_x_bach_float64_x_x_x_x_", 235614378, 1071967709, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 979939738, 1084730011, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1715787895, 1070755886, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2229934005, 1068695356, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 2229934005, 3216179004, "_x_x_x_x_bach_float64_x_x_x_x_", 1171944421, 3220995441, "_x_x_x_x_bach_float64_x_x_x_x_", 3737483794, 1072382000, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 19516331, 1084126641, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1921396570, 1073438114, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1016223622, 1076166615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091070464, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1086198784, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1091230464, 19, "(", "breakpoints", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 19, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, 100, ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2192701064, 1068795615, "_x_x_x_x_bach_float64_x_x_x_x_", 1299622744, 3225556808, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3681164930, 1070336560, "_x_x_x_x_bach_float64_x_x_x_x_", 3584957662, 3225227806, "_x_x_x_x_bach_float64_x_x_x_x_", 1650092075, 1067594853, ")" ],
					"whole_roll_data_0000000002" : [ "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1967026302, 1071207084, "_x_x_x_x_bach_float64_x_x_x_x_", 4141860302, 3224363268, "_x_x_x_x_bach_float64_x_x_x_x_", 2506061878, 3217921291, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 721004750, 1071765753, "_x_x_x_x_bach_float64_x_x_x_x_", 2240804697, 3224910056, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1130297953, 1072192624, "_x_x_x_x_bach_float64_x_x_x_x_", 2240804697, 3224910056, "_x_x_x_x_bach_float64_x_x_x_x_", 3910413104, 3217772158, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2633880104, 1069350018, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 146784802, 1071002394, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3203702005, 1071298033, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1072504615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1066773507, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1322162732, 1069106950, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3069286709, 1070754770, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 699014517, 1072118473, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3956592593, 1072431382, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3948621133, 1079803275, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3271321971, 1068648747, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1080623104, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 2870275104, 1080286470, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2518706261, 1079719117, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2543445273, 1066135235, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 954245957, 1066948096, "_x_x_x_x_bach_float64_x_x_x_x_", 954245957, 1067996672, "_x_x_x_x_bach_float64_x_x_x_x_", 954245957, 1066948096, "_x_x_x_x_bach_float64_x_x_x_x_", 2056497830, 3220830307, "_x_x_x_x_bach_float64_x_x_x_x_", 4232276405, 1072102278, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 1414796587, 1084379981, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 87411174, 1072544562, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4106969941, 1062577953, "_x_x_x_x_bach_float64_x_x_x_x_", 4106969941, 1063626529, "_x_x_x_x_bach_float64_x_x_x_x_", 4106969941, 1062577953, "_x_x_x_x_bach_float64_x_x_x_x_", 1602803369, 3221145113, "_x_x_x_x_bach_float64_x_x_x_x_", 50021401, 1072543617, "(", "lowpass", "_x_x_x_x_bach_float64_x_x_x_x_", 200111116, 1082146376, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", 0, ")", "(", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1085737984, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1092199040, 100, "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 918092209, 1071287380, "_x_x_x_x_bach_float64_x_x_x_x_", 1965102157, 3224620859, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3544550610, 1072351705, "_x_x_x_x_bach_float64_x_x_x_x_", 2244927866, 3225429137, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 3217846725, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1312542006, 1071288785, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2633880104, 1069350018, "_x_x_x_x_bach_float64_x_x_x_x_", 113524576, 1071884043, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 146784802, 1071002394, "_x_x_x_x_bach_float64_x_x_x_x_", 113524576, 1071884043, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3203702005, 1071298033, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1751247145, 1071772277, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2556089657, 1071838853, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1072504615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1066773507, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1322162732, 1069106950, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3069286709, 1070754770, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 699014517, 1072118473, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3956592593, 1072431382, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3948621133, 1079803275, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3271321971, 1068648747, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1080623104, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 2870275104, 1080286470, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2518706261, 1079719117, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1642395494, 1066718881, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1634680926, 1067616440, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1634680926, 3215100088, "_x_x_x_x_bach_float64_x_x_x_x_", 3409653998, 3221143845, "_x_x_x_x_bach_float64_x_x_x_x_", 4090632180, 1072541416, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 1036289709, 1082162646, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1068870659, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3043264701, 1067450924, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3043264701, 3214934572, "_x_x_x_x_bach_float64_x_x_x_x_", 3008409697, 3221153582, "_x_x_x_x_bach_float64_x_x_x_x_", 1767075560, 1072562106, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2432669476, 1082215838, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3501669657, 1072924821, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3901342133, 1070061271, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3781768766, 1068324750, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3781768766, 3215808398, "_x_x_x_x_bach_float64_x_x_x_x_", 222716067, 3221093268, "_x_x_x_x_bach_float64_x_x_x_x_", 601020728, 1072452878, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2712220308, 1082667693, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 512097541, 1072495153, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 543983378, 1071672413, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 65955736, 1067793405, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 65955736, 3215277053, "_x_x_x_x_bach_float64_x_x_x_x_", 2347542830, 3221130955, "_x_x_x_x_bach_float64_x_x_x_x_", 1602368268, 1072519296, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 3326572430, 1082327306, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1527221651, 1072264940, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4142364174, 1066850606, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 4142364174, 3214334254, "_x_x_x_x_bach_float64_x_x_x_x_", 3933869492, 3221172420, "_x_x_x_x_bach_float64_x_x_x_x_", 277973151, 1072599661, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2896113628, 1082205176, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1921396570, 1073438114, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 87411174, 1072544562, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3894796094, 1067598839, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3894796094, 3215082487, "_x_x_x_x_bach_float64_x_x_x_x_", 1602803369, 3221145113, "_x_x_x_x_bach_float64_x_x_x_x_", 50021401, 1072543617, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 200111116, 1082146376, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 6, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226566656, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2626183523, 1067949758, "_x_x_x_x_bach_float64_x_x_x_x_", 992034366, 3226173807, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 354042744, 1068685624, "_x_x_x_x_bach_float64_x_x_x_x_", 726227430, 3225135550, "_x_x_x_x_bach_float64_x_x_x_x_", 2688855686, 1070139386, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2453835075, 1069006169, "_x_x_x_x_bach_float64_x_x_x_x_", 3446968953, 3226415076, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2787811732, 1069699026, "_x_x_x_x_bach_float64_x_x_x_x_", 132216273, 3226493415, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3670169814, 1070152472, "_x_x_x_x_bach_float64_x_x_x_x_", 3817779250, 3226449543, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2612164750, 1070398745, "_x_x_x_x_bach_float64_x_x_x_x_", 3537953540, 3224997681, "_x_x_x_x_bach_float64_x_x_x_x_", 4175120529, 1069389075, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3083580360, 1070574654, "_x_x_x_x_bach_float64_x_x_x_x_", 1362844663, 3226208274, "_x_x_x_x_bach_float64_x_x_x_x_", 2465105069, 1070213944, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3229540529, 1070859011, "_x_x_x_x_bach_float64_x_x_x_x_", 3446968953, 3226415076, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2444489226, 1070966507, "_x_x_x_x_bach_float64_x_x_x_x_", 726227430, 3225135550, "_x_x_x_x_bach_float64_x_x_x_x_", 4093206912, 1069990253, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1729256913, 1071109189, "_x_x_x_x_bach_float64_x_x_x_x_", 250413773, 3226104873, "_x_x_x_x_bach_float64_x_x_x_x_", 1284504459, 3216723591, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 279001076, 1071351553, "_x_x_x_x_bach_float64_x_x_x_x_", 3446968953, 3226415076, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1132222099, 1071625185, "_x_x_x_x_bach_float64_x_x_x_x_", 4174570773, 3226070405, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1576424796, 1071709201, "_x_x_x_x_bach_float64_x_x_x_x_", 2796332947, 3224928747, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1694897174, 1072024858, "_x_x_x_x_bach_float64_x_x_x_x_", 852946145, 3224721945, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 964821453, 1072135290, "_x_x_x_x_bach_float64_x_x_x_x_", 132216273, 3226493415, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2195724721, 1072322924, "_x_x_x_x_bach_float64_x_x_x_x_", 803468122, 3226562349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3024756488, 1072427492, "_x_x_x_x_bach_float64_x_x_x_x_", 2832067075, 3225932537, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3858461180, 1072600467, "_x_x_x_x_bach_float64_x_x_x_x_", 2765546622, 3226545115, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226566656, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 7, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226566656, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3291937814, 1067699576, "_x_x_x_x_bach_float64_x_x_x_x_", 2447238006, 3225484465, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4082211796, 1070199381, "_x_x_x_x_bach_float64_x_x_x_x_", 3803760476, 3226035938, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1288077872, 1070882466, "_x_x_x_x_bach_float64_x_x_x_x_", 1594566738, 3224790879, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2360926343, 1071717019, "_x_x_x_x_bach_float64_x_x_x_x_", 1840032709, 3226423690, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 160528698, 1072084472, "_x_x_x_x_bach_float64_x_x_x_x_", 2104465256, 3226277208, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 999181192, 1072268197, "_x_x_x_x_bach_float64_x_x_x_x_", 2181431070, 3224446208, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 740246203, 1072612195, "_x_x_x_x_bach_float64_x_x_x_x_", 992034366, 3226173807, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226566656, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 8, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3230640040, 1076977183, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3858186302, 1071609548, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1076101120, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3230640040, 1076977183, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1076887552, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3272146604, 1069491584, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1076887552, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2915629959, 1070053193, "_x_x_x_x_bach_float64_x_x_x_x_", 4245214395, 1076249932, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1551685785, 1071000091, "_x_x_x_x_bach_float64_x_x_x_x_", 4245214395, 1076249932, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2674287157, 1071130823, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2670438866, 1071544805, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 777079843, 1071669133, "_x_x_x_x_bach_float64_x_x_x_x_", 2329865139, 1076739268, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2678135447, 1071765417, "_x_x_x_x_bach_float64_x_x_x_x_", 1562955779, 1076721658, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1553609930, 1071841676, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2699575924, 1072052299, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 205058919, 1072181222, "_x_x_x_x_bach_float64_x_x_x_x_", 2225136657, 1076328089, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2576980378, 1072273817, "_x_x_x_x_bach_float64_x_x_x_x_", 932110982, 1076336121, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1235301314, 1072417122, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2726239081, 1072685490, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")", 0, ")", 0, ")", 0, ")" ],
					"whole_roll_data_count" : [ 3 ],
					"zoom" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1441.333374, 965.666504, 126.0, 22.0 ],
					"style" : "",
					"text" : "bach.prepend addslot"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 1441.333374, 937.333252, 93.0, 22.0 ],
					"saved_object_attributes" : 					{
						"versionnumber" : 80001
					}
,
					"style" : "",
					"text" : "bach.keys slots"
				}

			}
, 			{
				"box" : 				{
					"activeslot" : 17,
					"bwcompatibility" : 70911,
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-170",
					"maxclass" : "bach.slot",
					"numinlets" : 1,
					"numoutlets" : 3,
					"out" : "nn",
					"outlettype" : [ "", "", "bang" ],
					"patching_rect" : [ 1441.333374, 748.333252, 326.0, 178.666656 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 0.0 ],
					"versionnumber" : 80001,
					"whole_uislot_data_0000000000" : [ "slot", "(", "slotinfo", "(", 1, "(", "name", "Envelope", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 2, "(", "name", "Position", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 3, "(", "name", "Period", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 2576980378, 1069128089, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 4, "(", "name", "Duration", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 2576980378, 1069128089, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1080623104, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 5, "(", "name", "Filter", ")", "(", "type", "dynfilter", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1087735936, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 858993459, 1071854387, ")", "(", "representation", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 6, "(", "name", "slot float", ")", "(", "type", "float", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "default", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 7, "(", "name", "lyrics", ")", "(", "type", "text", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 8, "(", "name", "filelist", ")", "(", "type", "filelist", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 9, "(", "name", "vocoder", ")", "(", "type", "function", ")", "(", "key", 0, ")", "(", "range", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1075970048, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1076887552, ")", "(", "slope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "representation", ")", "(", "grid", ")", "(", "ysnap", ")", "(", "domain", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", "(", "domainslope", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 10, "(", "name", "slot 10", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 11, "(", "name", "slot 11", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 12, "(", "name", "slot 12", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 13, "(", "name", "slot 13", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 14, "(", "name", "slot 14", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 15, "(", "name", "slot 15", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 16, "(", "name", "slot 16", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 17, "(", "name", "slot 17", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 18, "(", "name", "slot 18", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 19, "(", "name", "slot 19", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 20, "(", "name", "slot 20", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 21, "(", "name", "slot 21", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 0, ")", "(", "access", "readandwrite", ")", ")", "(", 22, "(", "name", "slot 22", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 23, "(", "name", "slot 23", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 24, "(", "name", "slot 24", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 25, "(", "name", "slot 25", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 26, "(", "name", "slot 26", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 27, "(", "name", "slot 27", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 28, "(", "name", "slot 28", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 29, "(", "name", "slot 29", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", "(", 30, "(", "name", "slot 30", ")", "(", "type", "none", ")", "(", "key", 0, ")", "(", "temporalmode", "none", ")", "(", "extend", 0, ")", "(", "width", "duration", ")", "(", "height", "auto", ")", "(", "copywhensplit", 1, ")", "(", "access", "readandwrite", ")", ")", ")", "(", "slots", "(", 1, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 918092209, 1071287380, "_x_x_x_x_bach_float64_x_x_x_x_", 1965102157, 3224620859, "_x_x_x_x_bach_float64_x_x_x_x_", 894452709, 3218158970, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3544550610, 1072351705, "_x_x_x_x_bach_float64_x_x_x_x_", 2244927866, 3225429137, "_x_x_x_x_bach_float64_x_x_x_x_", 1060753843, 3217846725, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 3226337280, "_x_x_x_x_bach_float64_x_x_x_x_", 1889785610, 1070008893, ")", ")", "(", 2, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2633880104, 1069350018, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 146784802, 1071002394, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3203702005, 1071298033, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1751247145, 1071772277, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2556089657, 1071838853, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1882913663, 1072138446, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2014580180, 1072504615, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1071644672, ")", ")", "(", 3, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 341398360, 1079609974, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1066773507, "_x_x_x_x_bach_float64_x_x_x_x_", 2529426500, 1081335582, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1322162732, 1069106950, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1081344000, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 2291657110, 1069928119, "_x_x_x_x_bach_float64_x_x_x_x_", 2492043104, 1081192486, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3069286709, 1070754770, "_x_x_x_x_bach_float64_x_x_x_x_", 3746860750, 1081116729, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3509915994, 1070834663, "_x_x_x_x_bach_float64_x_x_x_x_", 2791660023, 1080394036, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 699014517, 1072118473, "_x_x_x_x_bach_float64_x_x_x_x_", 2355703663, 1080596054, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3956592593, 1072431382, "_x_x_x_x_bach_float64_x_x_x_x_", 2023376273, 1080124679, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3542626465, 1079129192, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 4, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3948621133, 1079803275, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3271321971, 1068648747, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1080623104, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 140462610, 1070460729, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3239161255, 1071167544, "_x_x_x_x_bach_float64_x_x_x_x_", 531338994, 1080465307, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1121226982, 1071670473, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3627288860, 1071905236, "_x_x_x_x_bach_float64_x_x_x_x_", 3396391418, 1080381148, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 526666070, 1072257380, "_x_x_x_x_bach_float64_x_x_x_x_", 2870275104, 1080286470, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 2518706261, 1079719117, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", "(", 5, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1642395494, 1066718881, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1634680926, 1067616440, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1634680926, 3215100088, "_x_x_x_x_bach_float64_x_x_x_x_", 3409653998, 3221143845, "_x_x_x_x_bach_float64_x_x_x_x_", 4090632180, 1072541416, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 1036289709, 1082162646, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4285071691, 1068870659, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3043264701, 1067450924, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3043264701, 3214934572, "_x_x_x_x_bach_float64_x_x_x_x_", 3008409697, 3221153582, "_x_x_x_x_bach_float64_x_x_x_x_", 1767075560, 1072562106, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2432669476, 1082215838, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3501669657, 1072924821, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3901342133, 1070061271, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3781768766, 1068324750, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3781768766, 3215808398, "_x_x_x_x_bach_float64_x_x_x_x_", 222716067, 3221093268, "_x_x_x_x_bach_float64_x_x_x_x_", 601020728, 1072452878, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2712220308, 1082667693, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 512097541, 1072495153, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 543983378, 1071672413, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 65955736, 1067793405, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 65955736, 3215277053, "_x_x_x_x_bach_float64_x_x_x_x_", 2347542830, 3221130955, "_x_x_x_x_bach_float64_x_x_x_x_", 1602368268, 1072519296, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 3326572430, 1082327306, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1527221651, 1072264940, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 4142364174, 1066850606, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 4142364174, 3214334254, "_x_x_x_x_bach_float64_x_x_x_x_", 3933869492, 3221172420, "_x_x_x_x_bach_float64_x_x_x_x_", 277973151, 1072599661, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 2896113628, 1082205176, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1921396570, 1073438114, ")", ")", ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 87411174, 1072544562, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 3894796094, 1067598839, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 3894796094, 3215082487, "_x_x_x_x_bach_float64_x_x_x_x_", 1602803369, 3221145113, "_x_x_x_x_bach_float64_x_x_x_x_", 50021401, 1072543617, "(", "bandpass", "_x_x_x_x_bach_float64_x_x_x_x_", 200111116, 1082146376, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, ")", ")", ")", ")", "(", 9, "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, "_x_x_x_x_bach_float64_x_x_x_x_", 1452179982, 1076231349, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 1506330930, 1070781400, "_x_x_x_x_bach_float64_x_x_x_x_", 1704517901, 1076632143, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 332327389, 1071438287, "_x_x_x_x_bach_float64_x_x_x_x_", 1940638023, 1076066316, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", "(", "_x_x_x_x_bach_float64_x_x_x_x_", 0, 1072693248, "_x_x_x_x_bach_float64_x_x_x_x_", 3634710564, 1076592849, "_x_x_x_x_bach_float64_x_x_x_x_", 0, 0, ")", ")", ")" ],
					"whole_uislot_data_count" : [ 1 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 941.0, 283.25, 81.0, 22.0 ],
					"style" : "",
					"text" : "route append"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 941.0, 149.208328, 29.5, 22.0 ],
					"style" : "",
					"text" : "t l l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 908.547852, 2061.053711, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 969.0, 2061.053711, 37.0, 22.0 ],
					"style" : "",
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 466.571442, 2399.714111, 45.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1206.833374, 655.620972, 45.0, 45.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 908.547852, 2129.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "sfrecord~ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 466.571442, 2239.773682, 48.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1210.333374, 496.602081, 48.0, 147.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[1]",
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 6.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ -3.0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-25",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 508.166656, 25.700012, 159.0, 22.0 ],
					"style" : "",
					"text" : "readappend @name audio2"
				}

			}
, 			{
				"box" : 				{
					"alignviewbounds" : 0,
					"autobounds" : 0,
					"autoupdate" : 120.0,
					"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"bufferchooser_bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"bufferchooser_fgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bufferchooser_position" : 1,
					"bufferchooser_shape" : "buttons",
					"bufferchooser_size" : 15,
					"bufferchooser_visible" : 1,
					"cursor_color" : [ 1.0, 0.0, 0.0, 1.0 ],
					"cursor_followmouse" : 0,
					"cursor_position" : -1.0,
					"cursor_shape" : "bar",
					"cursor_size" : 3,
					"cursor_visible" : 1,
					"domain_bounds" : [ -16.326531, 41449.795918 ],
					"domainruler_bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"domainruler_fgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"domainruler_grid" : 0,
					"domainruler_position" : 0,
					"domainruler_size" : 15,
					"domainruler_unit" : 0,
					"domainruler_visible" : 1,
					"domainscrollbar_color" : [ 1.0, 1.0, 1.0, 1.0 ],
					"domainscrollbar_size" : 10,
					"domainscrollbar_visible" : 1,
					"embed" : 0,
					"externalfiles" : 1,
					"id" : "obj-1",
					"layout" : 0,
					"maxclass" : "imubu",
					"name" : "Source",
					"numinlets" : 1,
					"numoutlets" : 1,
					"opacity" : 0.0,
					"opacityprogressive" : 0,
					"orientation" : 0,
					"outlettype" : [ "" ],
					"outputkeys" : 0,
					"outputmouse" : 0,
					"outputselection" : 0,
					"outputtimeselection" : 0,
					"outputvalues" : 0,
					"patching_rect" : [ 255.0, 69.0, 542.0, 325.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.0, 40.268829, 466.0, 274.0 ],
					"rangeruler_grid" : 0,
					"rangeruler_size" : 30,
					"rangeruler_visible" : 1,
					"region_bounds" : [ 0.0, 0.0 ],
					"region_color" : [ 0.8, 0.7, 0.7, 1.0 ],
					"region_visible" : 1,
					"split_color" : [ 1.0, 0.0, 0.0, 1.0 ],
					"split_size" : 2,
					"split_visible" : 1,
					"tabs_position" : 0,
					"tabs_size" : 20,
					"tabs_visible" : 1,
					"toolbar_bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"toolbar_position" : 1,
					"toolbar_size" : 30,
					"toolbar_visible" : 1,
					"useplaceholders" : 1,
					"windresize" : 0
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-2",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 321.5, 25.700012, 153.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 73.5, 12.268829, 153.0, 22.0 ],
					"style" : "",
					"text" : "readappend @name audio"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-157",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 3456.0, 3540.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 819.0, 313.533295, 141.0, 22.0 ],
					"style" : "",
					"text" : "combine a b @triggers 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-103",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 819.0, 348.899963, 169.0, 22.0 ],
					"style" : "",
					"text" : "readappend $1 @name audio"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 255.0, 21.700012, 49.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.0, 12.268829, 49.0, 22.0 ],
					"style" : "",
					"text" : "clearall"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1001.0, 222.499985, 95.0, 23.0 ],
					"style" : "",
					"text" : "prepend types"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"items" : [ "AIFF", ",", "APPL", "(application)", ",", "BMP", ",", "DATA", ",", "FLAC", ",", "fold", ",", "GIFf", ",", "JPEG", ",", "JSON", ",", "MPEG", ",", "Midi", ",", "MooV", ",", "PICT", ",", "PNG", ",", "TEXT", ",", "TIFF", ",", "VfW", "(AVI", "file)", ",", "WAVE", ",", "aPcs", "(audio", "plugin)", ",", "iLaF", "(Max", "external", "-", "Mac)", ",", "iLaX", "(Max", "external", "-", "Win)" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 963.0, 191.833328, 95.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 513.875, 200.268829, 95.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-70",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 948.375, 105.833328, 120.0, 50.0 ],
					"presentation" : 1,
					"presentation_linecount" : 3,
					"presentation_rect" : [ 508.875, 130.310486, 105.0, 50.0 ],
					"style" : "",
					"text" : "drop a folder here for source concatenation"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "dropfile",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 941.0, 94.083328, 134.75, 44.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 494.0, 112.352142, 134.75, 85.916672 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 941.0, 255.499985, 130.0, 23.0 ],
					"style" : "",
					"text" : "folder C74:/help/max"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 32.20706,
					"id" : "obj-96",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 654.666687, 400.268829, 146.666672, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 494.0, 224.268829, 146.666672, 42.0 ],
					"style" : "",
					"text" : "SOURCE"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"order" : 0,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"order" : 1,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 828.5, 378.233307, 808.583339, 378.233307, 808.583339, 58.0, 264.5, 58.0 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-126", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 1 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"order" : 2,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"order" : 0,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 1,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-167", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-167", 0 ],
					"midpoints" : [ 1450.833374, 996.666565, 1225.738039, 996.666565, 1225.738039, 786.666668, 264.5, 786.666668 ],
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 1 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 1,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"order" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 1 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"midpoints" : [ 1334.0, 182.870811, 1201.5, 182.870811 ],
					"source" : [ "obj-23", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"midpoints" : [ 1201.5, 380.899963, 1478.500004, 380.899963, 1478.500004, 58.0, 1499.5, 58.0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-29", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 1 ],
					"source" : [ "obj-3", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"order" : 1,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"midpoints" : [ 961.0, 182.870811, 828.5, 182.870811 ],
					"source" : [ "obj-32", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"order" : 1,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"order" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-37", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 2 ],
					"order" : 0,
					"source" : [ "obj-37", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"order" : 1,
					"source" : [ "obj-37", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 1 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-38", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"order" : 0,
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 1 ],
					"order" : 1,
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"order" : 2,
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-4", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 1 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 3 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"order" : 0,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 1 ],
					"order" : 1,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"order" : 2,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"order" : 1,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 0,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 1 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 1 ],
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-69", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 1 ],
					"order" : 1,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"order" : 2,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"order" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 1 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"order" : 0,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"order" : 1,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 1 ],
					"source" : [ "obj-79", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 1 ],
					"source" : [ "obj-8", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"order" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"order" : 1,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 1 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-79" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-7" : [ "live.gain~[2]", "live.gain~", 0 ],
			"obj-53::obj-8" : [ "live.gain~[3]", "live.gain~", 0 ],
			"obj-55" : [ "live.gain~[4]", "live.gain~", 0 ],
			"obj-39" : [ "live.gain~[5]", "live.gain~", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "bach.prepend.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "PolyConcatSource.maxpat",
				"bootpath" : "~/Desktop/SPLS_prod/codes/audio/WWW_computer_music_design_tools",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.slot2filtercoeff.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.times.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.x2dx.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.filternull.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.filter.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ej.line.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/ejies/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/ejies/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.slot2curve.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "PolyConcatTarget.maxpat",
				"bootpath" : "~/Desktop/SPLS_prod/codes/audio/WWW_computer_music_design_tools",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "vocvoice_an~.maxpat",
				"bootpath" : "~/Desktop/SPLS_prod/codes/audio/WWW_computer_music_design_tools",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "vocvoice_syn~.maxpat",
				"bootpath" : "~/Desktop/SPLS_prod/codes/audio/WWW_computer_music_design_tools",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bach.lookup.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/bach/patchers",
				"patcherrelativepath" : "../../../../../Documents/Max 7/Packages/bach/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.slot.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.keys.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.args.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.portal.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.join.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.reg.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.wrap.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.roll.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.concat~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.playkeys.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.trans.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.pick.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.expr.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.iter.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.collect.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.flat.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.eq.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.gt.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.nth.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.slice.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.lace.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.mapelem.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.is.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.neq.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.group.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bach.rot.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mubu.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "Max_class_18",
				"default" : 				{
					"fontsize" : [ 18.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "blue",
				"default" : 				{
					"clearcolor" : [ 0.317647, 0.654902, 0.976471, 0.0 ],
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color1" : [ 0.317647, 0.654902, 0.976471, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 0.76 ],
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39
					}
,
					"color" : [ 0.0, 0.078431, 0.321569, 1.0 ],
					"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"textcolor_inverse" : [ 0.0, 0.078431, 0.321569, 1.0 ],
					"patchlinecolor" : [ 0.0, 0.078431, 0.321569, 0.9 ],
					"fontface" : [ 0 ],
					"textcolor" : [ 0.0, 0.078431, 0.321569, 1.0 ],
					"textjustification" : [ 0 ],
					"accentcolor" : [ 0.0, 0.078431, 0.321569, 1.0 ],
					"fontsize" : [ 12.0 ],
					"elementcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBrown-1",
				"default" : 				{
					"accentcolor" : [ 0.654902, 0.572549, 0.376471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjCyan-1",
				"default" : 				{
					"accentcolor" : [ 0.029546, 0.773327, 0.821113, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "panelViolet",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.372549, 0.196078, 0.486275, 0.2 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
