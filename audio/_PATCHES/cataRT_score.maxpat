{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 116.0, 45.0, 1264.0, 986.0 ],
		"bgcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 15,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"color" : [ 0.485368, 0.706563, 0.767416, 1.0 ],
					"fontsize" : 19.662323,
					"id" : "obj-136",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 498.0, 186.0, 584.0, 191.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-7",
									"linecount" : 3,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 10.0, 116.0, 182.0, 47.0 ],
									"style" : "",
									"text" : "Default UDP parameters: \n - host: 127.0.0.1 (localhost)\n - port: 8480"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 4,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 10.0, 47.0, 581.0, 60.0 ],
									"style" : "",
									"text" : "This patch allows to write envelopes and automations for cataRT standalone application, saving them into presets.\n\nIt sends all parameters to cataRT application using OSC protocol through local network and standard port."
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 19.792146,
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 10.0, 9.495495, 175.0, 29.0 ],
									"style" : "",
									"text" : "score for cataRT "
								}

							}
 ],
						"lines" : [  ]
					}
,
					"patching_rect" : [ 1489.333378, 88.000003, 167.0, 30.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1468.666626, 101.333374, 93.0, 30.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p readme"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 29.550717,
					"id" : "obj-135",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1587.833374, 105.999992, 194.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1542.833374, 255.416702, 132.0, 40.0 ],
					"style" : "",
					"text" : "start/stop"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-134",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1218.666748, 1226.999878, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 1228.0, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-133",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1228.66687, 169.333267, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 174.666656, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-132",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1220.66687, 318.333374, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 327.333313, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-131",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1220.66687, 486.000031, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1218.000244, 489.000031, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-130",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1218.000244, 626.999939, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1218.000244, 627.333374, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-128",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1220.0, 785.666565, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1218.000244, 780.333252, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-121",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2014.666626, 163.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1273.333374, 1203.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-122",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1958.666626, 164.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 1204.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1942.000122, 202.666672, 43.0, 22.0 ],
					"presentation_rect" : [ 1942.000122, 202.666672, 0.0, 0.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-124",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2041.333374, 198.0, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 1151.333374, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-126",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1942.000122, 233.606689, 91.0, 22.0 ],
					"presentation_rect" : [ 1942.000122, 233.606689, 0.0, 0.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-106",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 181.66658, 129.059967, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 87.333374, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-113",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 126.666458, 129.059967, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1280.666504, 149.333389, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-118",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 63.333313, 129.059967, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 149.333389, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 63.333313, 168.726654, 82.33313, 22.0 ],
					"presentation_rect" : [ 34.833393, 543.666687, 0.0, 0.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-120",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 63.333313, 199.666672, 91.0, 22.0 ],
					"presentation_rect" : [ 34.833393, 574.606689, 0.0, 0.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-98",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 357.166656, 183.999985, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 239.333328, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-99",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 302.166534, 183.999985, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1280.666626, 303.333313, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-102",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 238.833405, 183.999985, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 303.333313, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 238.833405, 223.666672, 82.33313, 22.0 ],
					"presentation_rect" : [ 232.166733, 533.333374, 0.0, 0.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-105",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 238.833405, 254.606674, 91.0, 22.0 ],
					"presentation_rect" : [ 232.166733, 564.273376, 0.0, 0.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-79",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 718.5, 177.666672, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 404.333344, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-89",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 663.499878, 177.666672, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1280.666504, 465.000031, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-92",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 600.166748, 177.666672, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 465.000031, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 600.166748, 217.333344, 82.33313, 22.0 ],
					"presentation_rect" : [ 600.166748, 220.000015, 0.0, 0.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-97",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 600.166748, 248.273361, 91.0, 22.0 ],
					"presentation_rect" : [ 600.166748, 250.940033, 0.0, 0.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-55",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 973.166687, 177.666672, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 543.333374, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-62",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 918.166565, 177.666672, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1281.333374, 603.333374, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-63",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 854.833435, 177.666672, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1218.000244, 603.333374, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 854.833435, 217.333344, 82.33313, 22.0 ],
					"presentation_rect" : [ 854.833435, 220.000015, 0.0, 0.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-77",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 854.833435, 248.273361, 91.0, 22.0 ],
					"presentation_rect" : [ 854.833435, 250.940033, 0.0, 0.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-43",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1230.5, 177.666672, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1218.000244, 695.333313, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-44",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1175.499878, 177.666672, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1281.333374, 752.666626, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-49",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1112.166748, 177.666672, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1218.000244, 752.666626, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1112.166748, 217.333344, 82.33313, 22.0 ],
					"presentation_rect" : [ 1109.333374, 217.333344, 0.0, 0.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-51",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1112.166748, 248.273361, 91.0, 22.0 ],
					"presentation_rect" : [ 1109.333374, 248.273361, 0.0, 0.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-31",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1222.666748, 1058.999878, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 1088.333252, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 16.368821,
					"id" : "obj-26",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1252.000037, 926.666694, 199.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 940.333252, 77.0, 25.0 ],
					"style" : "",
					"text" : "set range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.160636,
					"id" : "obj-24",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 517.666687, 7.696983, 282.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 47.333328, 192.0, 33.0 ],
					"style" : "",
					"text" : "main parameters"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.160636,
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1635.0, 259.696991, 282.0, 33.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 1474.5, 633.333374, 133.0, 60.0 ],
					"style" : "",
					"text" : "other parameters"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 24.160636,
					"id" : "obj-15",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1633.333382, 685.333354, 282.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1474.5, 1002.0, 77.000122, 33.0 ],
					"style" : "",
					"text" : "preset"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 2,
					"fontsize" : 60.063899,
					"id" : "obj-13",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1809.333374, 73.333336, 770.0, 74.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 720.000122, 6.333328, 483.0, 74.0 ],
					"style" : "",
					"text" : "scores for cataRT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 726.666687, 113.0, 50.0, 22.0 ],
					"style" : "",
					"text" : "s on-off"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-95",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1495.666626, 147.0, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 853.333313, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-82",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1440.666504, 147.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1288.0, 916.333252, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-84",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1377.333374, 147.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 916.333252, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1377.333374, 186.666672, 82.33313, 22.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-88",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1377.333374, 217.606689, 91.0, 22.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-59",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1660.0, 147.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1288.0, 1064.333252, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-54",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1604.0, 148.0, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 1064.333252, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1587.333496, 186.666672, 43.0, 22.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"attr" : "range",
					"id" : "obj-47",
					"maxclass" : "attrui",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1686.666748, 182.0, 75.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1217.333374, 999.333313, 145.0, 44.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 29.550717,
					"id" : "obj-46",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 396.166656, 45.0, 194.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1468.666626, 402.583252, 157.0, 40.0 ],
					"style" : "",
					"text" : "global time"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 682.0, 113.0, 36.0, 22.0 ],
					"style" : "",
					"text" : "sel 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 682.0, 82.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1468.666626, 239.333328, 72.166748, 72.166748 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 682.0, 54.0, 43.0, 22.0 ],
					"style" : "",
					"text" : "sel 32"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "int", "int", "int", "int" ],
					"patching_rect" : [ 682.0, 18.0, 50.5, 22.0 ],
					"style" : "",
					"text" : "key"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 404.916656, 108.333336, 29.5, 22.0 ],
					"style" : "",
					"text" : "0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1192.333374, 499.999878, 47.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1528.166748, 705.302856, 47.0, 20.0 ],
					"style" : "",
					"text" : "radius\n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1562.333374, 489.666656, 71.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1528.666748, 753.636292, 71.0, 20.0 ],
					"style" : "",
					"text" : "transp rand"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1725.333374, 493.333344, 51.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1529.333496, 798.636292, 54.0, 20.0 ],
					"style" : "",
					"text" : "reverse "
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1898.333374, 527.666565, 45.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1526.5, 844.969482, 78.0, 20.0 ],
					"style" : "",
					"text" : "trigger mode"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-5",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1587.333496, 217.606689, 91.0, 22.0 ],
					"style" : "",
					"text" : "setrange $1 $2"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-129",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1138.666748, 499.999878, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1474.5, 705.302856, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1138.666748, 531.999878, 109.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/radius $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 4,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-124",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1576.333496, 343.333374, 97.0, 22.0 ],
									"style" : "",
									"text" : "setrange 0.6 0.7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1085.333252, 654.199951, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-122",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 662.333313, 631.199951, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-121",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 818.333313, 597.199951, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-120",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1007.333313, 597.199951, 82.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 200"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-119",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1249.333252, 743.0, 50.0, 22.0 ],
									"style" : "",
									"text" : "0.05"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-113",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1639.333496, 650.000122, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-106",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1639.333496, 685.199829, 109.0, 22.0 ],
									"style" : "",
									"text" : "/corpus1/radius $1"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-99",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1549.333496, 624.333252, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-102",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1466.333496, 624.333252, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1466.333496, 659.666626, 102.0, 22.0 ],
									"style" : "",
									"text" : "pack f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-105",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1466.333496, 694.199829, 112.0, 22.0 ],
									"style" : "",
									"text" : "/catart/select $1 $2"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.305882, 0.717647, 0.67451, 1.0 ],
									"id" : "obj-97",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1466.333496, 725.199829, 137.0, 22.0 ],
									"style" : "",
									"text" : "udpsend localhost 8481"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 990.333313, 654.199951, 75.0, 22.0 ],
									"style" : "",
									"text" : "speedlim 20"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-88",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 505.833374, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 542.500122, 111.0, 22.0 ],
									"style" : "",
									"text" : "/corpus1/trigger $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "preset",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "preset", "int", "preset", "int" ],
									"patching_rect" : [ 1051.666748, 215.0, 100.0, 40.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1521.000244, 256.333374, 29.5, 22.0 ],
									"style" : "",
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 232.666748, 542.500122, 120.0, 22.0 ],
									"style" : "",
									"text" : "/corpus1/duration $1"
								}

							}
, 							{
								"box" : 								{
									"addpoints" : [ 0.0, 248.801224, 0, 97.517662, 178.04567, 0, 279.077881, 319.557251, 0, 354.609711, 494.677063, 0, 455.673553, 945.743713, 0, 583.33313, 1131.477051, 0, 657.801208, 892.677063, 0, 737.58844, 945.743713, 0, 854.609741, 627.34375, 0, 1000.0, 319.556793, 0 ],
									"id" : "obj-79",
									"maxclass" : "function",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "", "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 232.666748, 422.266724, 200.0, 100.0 ],
									"range" : [ 10.0, 2000.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
									"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
									"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
									"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
									"bgfillcolor_proportion" : 0.39,
									"bgfillcolor_type" : "gradient",
									"gradient" : 1,
									"id" : "obj-62",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 280.333496, 318.606689, 84.0, 22.0 ],
									"style" : "",
									"text" : "setrange 0 15"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 460.333435, 530.25, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-55",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 460.333435, 562.5, 122.0, 22.0 ],
									"style" : "",
									"text" : "/corpus1/quantsel $1"
								}

							}
, 							{
								"box" : 								{
									"addpoints" : [ 0.0, 0.0, 2, 113.474716, 0.0, 0, 131.914337, 2.813341, 0, 212.765305, 2.053345, 0, 308.509979, 5.253345, 0, 378.722839, 7.293341, 0, 418.43927, 3.933343, 0, 489.361053, 2.453345, 0, 537.233398, 5.853345, 0, 590.424866, 2.653345, 0, 622.339783, 7.253345, 0, 686.169556, 8.453345, 0, 755.318481, 6.653345, 0, 909.573792, 4.253345, 0, 1000.0, 0.0, 0 ],
									"id" : "obj-59",
									"maxclass" : "function",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "", "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 460.333435, 422.266724, 200.0, 100.0 ],
									"range" : [ 0.0, 15.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-49",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 743.499939, 538.499878, 99.0, 22.0 ],
									"style" : "",
									"text" : "/corpus1/gain $1"
								}

							}
, 							{
								"box" : 								{
									"addpoints" : [ 0.0, -23.920067, 0, 153.369247, -6.320065, 0, 174.645615, -2.213369, 0, 227.837097, -3.093369, 0, 252.660034, -2.800065, 0, 334.220062, -4.853369, 0, 376.773254, -2.213369, 0, 445.922211, -4.853369, 0, 499.113678, -3.093369, 0, 531.028564, -3.093369, 0, 632.092407, -4.853369, 0, 743.794556, -6.613369, 0, 945.92218, -7.493369, 0, 1000.0, -14.533369, 0 ],
									"id" : "obj-50",
									"maxclass" : "function",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "", "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 743.499939, 412.666626, 200.0, 100.0 ],
									"range" : [ -60.0, 6.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-48",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1527.666748, 194.0, 33.0, 22.0 ],
									"style" : "",
									"text" : "stop"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-46",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1191.333496, 290.000061, 84.0, 22.0 ],
									"style" : "",
									"text" : "setrange 0. 1."
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-45",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1496.333496, 506.333252, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-44",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1273.333252, 506.333252, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-41",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 990.333313, 523.000122, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-37",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 990.333313, 343.333374, 111.0, 22.0 ],
									"style" : "",
									"text" : "setrange 0.01 0.27"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-36",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 990.333313, 552.000122, 109.0, 22.0 ],
									"style" : "",
									"text" : "/corpus1/radius $1"
								}

							}
, 							{
								"box" : 								{
									"addpoints" : [ 0.0, 0.01, 0, 117.021278, 0.112844, 0, 175.531921, 0.043511, 0, 329.787231, 0.147511, 0, 420.212769, 0.071244, 0, 526.595764, 0.154444, 0, 659.574463, 0.185644, 0, 797.872314, 0.248044, 0, 925.531921, 0.244578, 0, 1000.0, 0.092044, 0 ],
									"id" : "obj-34",
									"maxclass" : "function",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "", "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 990.333313, 412.666626, 200.0, 100.0 ],
									"range" : [ 0.01, 0.27 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1436.000244, 282.0, 47.0, 22.0 ],
									"style" : "",
									"text" : "/ 1000."
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 21.053934,
									"format" : 6,
									"id" : "obj-25",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1433.500244, 313.606689, 72.0, 32.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-26",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 1436.000244, 164.0, 49.0, 22.0 ],
									"style" : "",
									"text" : "* 1000."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-27",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 1436.000244, 100.0, 24.0, 24.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-30",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1436.000244, 134.0, 50.0, 22.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-31",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1436.000244, 194.0, 67.0, 22.0 ],
									"style" : "",
									"text" : "0, 1000 $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 1436.000244, 236.0, 79.0, 22.0 ],
									"style" : "",
									"text" : "ej.line 1. 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1273.333252, 548.666626, 49.0, 22.0 ],
									"style" : "",
									"text" : "pack f f"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1273.333252, 583.999878, 112.0, 22.0 ],
									"style" : "",
									"text" : "/catart/select $1 $2"
								}

							}
, 							{
								"box" : 								{
									"addpoints" : [ 0.0, 0.650667, 0, 106.38298, 0.649333, 0, 228.723404, 0.637333, 0, 297.872345, 0.641333, 0, 351.063843, 0.618667, 0, 425.531921, 0.632, 0, 485.106567, 0.625333, 0, 569.148926, 0.621333, 2, 643.617004, 0.645333, 0, 728.723389, 0.654667, 0, 781.914917, 0.64, 0, 906.383179, 0.665333, 0, 953.19165, 0.634933, 0, 1000.0, 0.6, 0 ],
									"id" : "obj-14",
									"maxclass" : "function",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "", "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1496.333496, 393.0, 200.0, 100.0 ],
									"range" : [ 0.6, 0.7 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"addpoints" : [ 0.0, 0.0, 0, 69.148933, 0.133333, 0, 143.61702, 0.146667, 0, 212.765961, 0.146667, 0, 250.0, 0.146667, 0, 345.74469, 0.24, 0, 409.574463, 0.56, 0, 452.127655, 0.373333, 0, 553.191467, 0.24, 0, 627.659546, 0.413333, 0, 712.76593, 0.4, 0, 781.915039, 0.392, 0, 862.766113, 0.413333, 0, 926.595886, 0.509333, 0, 977.659729, 0.669333, 0, 1000.0, 0.0, 0 ],
									"id" : "obj-15",
									"maxclass" : "function",
									"numinlets" : 1,
									"numoutlets" : 4,
									"outlettype" : [ "float", "", "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 1273.333252, 388.0, 200.0, 100.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"color" : [ 0.305882, 0.717647, 0.67451, 1.0 ],
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 990.333313, 685.199829, 137.0, 22.0 ],
									"style" : "",
									"text" : "udpsend localhost 8481"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"source" : [ "obj-105", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"source" : [ "obj-106", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"source" : [ "obj-113", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-120", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-121", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-122", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-123", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-124", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-44", 0 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-31", 0 ],
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-27", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"source" : [ "obj-31", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"order" : 0,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 2,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"order" : 1,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"order" : 3,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-50", 0 ],
									"order" : 4,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"order" : 5,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-79", 0 ],
									"order" : 6,
									"source" : [ "obj-33", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-34", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-120", 0 ],
									"source" : [ "obj-36", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-34", 0 ],
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-36", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 1 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"source" : [ "obj-46", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"order" : 1,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"order" : 0,
									"source" : [ "obj-48", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-121", 0 ],
									"source" : [ "obj-49", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-49", 0 ],
									"source" : [ "obj-50", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-122", 0 ],
									"source" : [ "obj-55", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-62", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-79", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"order" : 0,
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"order" : 1,
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-95", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-96", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 1 ],
									"source" : [ "obj-99", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 829.0, 1250.333374, 65.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p Score_2"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-9",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1467.0, 663.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-8",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1398.0, 663.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1398.0, 707.333252, 88.0, 22.0 ],
					"style" : "",
					"text" : "pak f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1398.0, 736.333252, 112.0, 22.0 ],
					"style" : "",
					"text" : "/catart/select $1 $2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1992.0, 424.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-116",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1508.166626, 489.666656, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1474.5, 753.636292, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1467.0, 527.666565, 167.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/transposition_std $1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-115",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1841.666748, 229.940002, 84.0, 22.0 ],
					"style" : "",
					"text" : "setrange 1 12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1850.000122, 427.25, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1850.000122, 454.166626, 122.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/quantsel $1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-110",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1975.000122, 269.382416, 70.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.0, 1151.333252, 70.0, 24.0 ],
					"style" : "",
					"text" : "quantsel",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 4.08, 0, 198.581207, 8.968889, 0, 404.254974, 5.057778, 0, 574.467773, 9.36, 0, 666.666443, 4.471112, 0, 822.694702, 10.142222, 0, 1000.0, 6.426667, 0 ],
					"bgcolor" : [ 0.380896, 0.406538, 0.487159, 0.52 ],
					"id" : "obj-111",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1850.000122, 300.333344, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 1151.333374, 1200.0, 150.0 ],
					"range" : [ 1.0, 12.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 726.666687, 531.999878, 128.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/chainlen 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1846.333374, 527.666565, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1474.5, 844.969482, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1670.5, 493.333344, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1474.5, 798.636292, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1846.333374, 564.333252, 111.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/trigger $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1670.5, 527.666565, 117.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/reverse $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 869.500061, 496.0, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 451.666626, 193.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "/ 1000."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1138.666748, 39.5, 208.333344, 33.0 ],
					"style" : "",
					"text" : "1-beat\n2-bow (reverse 90)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 560.0, 59.0, 33.0, 22.0 ],
					"style" : "",
					"text" : "stop"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 21.053934,
					"format" : 6,
					"id" : "obj-83",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 431.0, 229.940002, 72.0, 32.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "preset", "int", "preset", "int" ],
					"patching_rect" : [ 957.0, 36.0, 100.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1474.5, 1031.636108, 169.166824, 49.333336 ],
					"preset_data" : [ 						{
							"number" : 1,
							"data" : [ 4, "obj-2", "function", "clear", 7, "obj-2", "function", "add", 0.0, -60.0, 0, 7, "obj-2", "function", "add", 3.546208, -14.23998, 0, 7, "obj-2", "function", "add", 83.333443, 5.92002, 0, 7, "obj-2", "function", "add", 386.524933, 7.84002, 0, 7, "obj-2", "function", "add", 535.461121, -7.51998, 0, 7, "obj-2", "function", "add", 663.120667, -42.079979, 0, 7, "obj-2", "function", "add", 730.496582, -2.71999, 0, 7, "obj-2", "function", "add", 921.985962, -2.71999, 0, 7, "obj-2", "function", "add", 1000.0, -60.0, 0, 5, "obj-2", "function", "domain", 1000.0, 6, "obj-2", "function", "range", -60.0, 12.0, 5, "obj-2", "function", "mode", 0, 4, "obj-12", "function", "clear", 7, "obj-12", "function", "add", 0.0, 1991.156128, 0, 7, "obj-12", "function", "add", 124.113525, 1513.556152, 0, 7, "obj-12", "function", "add", 326.24118, 876.756104, 0, 7, "obj-12", "function", "add", 400.709259, 1142.089478, 0, 7, "obj-12", "function", "add", 687.943298, 770.622742, 0, 7, "obj-12", "function", "add", 884.751831, 1221.689453, 0, 7, "obj-12", "function", "add", 1000.0, 1858.48938, 0, 5, "obj-12", "function", "domain", 1000.0, 6, "obj-12", "function", "range", 10.0, 2000.0, 5, "obj-12", "function", "mode", 0, 4, "obj-21", "function", "clear", 7, "obj-21", "function", "add", 0.0, -0.53332, 0, 7, "obj-21", "function", "add", 1000.0, -1.81332, 0, 5, "obj-21", "function", "domain", 1000.0, 6, "obj-21", "function", "range", -24.0, 24.0, 5, "obj-21", "function", "mode", 0, 4, "obj-28", "function", "clear", 7, "obj-28", "function", "add", 0.0, 291.555664, 0, 7, "obj-28", "function", "add", 1000.0, 296.889008, 0, 5, "obj-28", "function", "domain", 1000.0, 6, "obj-28", "function", "range", 0.0, 400.0, 5, "obj-28", "function", "mode", 0, 4, "obj-32", "function", "clear", 7, "obj-32", "function", "add", 0.0, 231.111649, 0, 7, "obj-32", "function", "add", 523.049194, 284.444977, 0, 7, "obj-32", "function", "add", 804.964111, 631.111633, 0, 7, "obj-32", "function", "add", 1000.0, 1671.111694, 0, 5, "obj-32", "function", "domain", 1000.0, 6, "obj-32", "function", "range", 0.0, 2000.0, 5, "obj-32", "function", "mode", 0, 4, "obj-42", "function", "clear", 7, "obj-42", "function", "add", 0.0, 0.462222, 0, 7, "obj-42", "function", "add", 58.510639, 0.262222, 0, 7, "obj-42", "function", "add", 127.659576, 0.302222, 0, 7, "obj-42", "function", "add", 175.531921, 0.155556, 0, 7, "obj-42", "function", "add", 218.085114, 0.328889, 0, 7, "obj-42", "function", "add", 271.276581, 0.208889, 0, 7, "obj-42", "function", "add", 319.148926, 0.102222, 0, 7, "obj-42", "function", "add", 388.297882, 0.328889, 0, 7, "obj-42", "function", "add", 452.127655, 0.262222, 0, 7, "obj-42", "function", "add", 574.468079, 0.062222, 0, 7, "obj-42", "function", "add", 702.127686, 0.208889, 0, 7, "obj-42", "function", "add", 787.23407, 0.115556, 0, 7, "obj-42", "function", "add", 851.063843, 0.208889, 0, 7, "obj-42", "function", "add", 898.936157, 0.248889, 0, 7, "obj-42", "function", "add", 952.127686, 0.062222, 0, 7, "obj-42", "function", "add", 1000.0, 0.555556, 0, 5, "obj-42", "function", "domain", 1000.0, 6, "obj-42", "function", "range", 0.0, 1.0, 5, "obj-42", "function", "mode", 0, 4, "obj-58", "function", "clear", 7, "obj-58", "function", "add", 10.638298, 0.542222, 0, 7, "obj-58", "function", "add", 42.553192, 0.888889, 0, 7, "obj-58", "function", "add", 111.702126, 0.582222, 0, 7, "obj-58", "function", "add", 191.489365, 0.355556, 0, 7, "obj-58", "function", "add", 292.553192, 0.728889, 0, 7, "obj-58", "function", "add", 351.063843, 0.355556, 0, 7, "obj-58", "function", "add", 420.212769, 0.715556, 0, 7, "obj-58", "function", "add", 526.595764, 0.742222, 0, 7, "obj-58", "function", "add", 595.74469, 0.568889, 0, 7, "obj-58", "function", "add", 718.085083, 0.462222, 0, 7, "obj-58", "function", "add", 755.319153, 0.862222, 0, 7, "obj-58", "function", "add", 1000.0, 1.0, 0, 5, "obj-58", "function", "domain", 1000.0, 6, "obj-58", "function", "range", 0.0, 1.0, 5, "obj-58", "function", "mode", 0, 5, "obj-60", "flonum", "float", 120.0, 5, "obj-83", "flonum", "float", 1.0, 5, "obj-103", "number", "int", 10, 5, "obj-107", "number", "int", 2, 4, "obj-111", "function", "clear", 7, "obj-111", "function", "add", 0.0, 4.08, 0, 7, "obj-111", "function", "add", 198.581207, 8.968889, 0, 7, "obj-111", "function", "add", 404.254974, 5.057778, 0, 7, "obj-111", "function", "add", 574.467773, 9.36, 0, 7, "obj-111", "function", "add", 666.666443, 4.471112, 0, 7, "obj-111", "function", "add", 822.694702, 10.142222, 0, 7, "obj-111", "function", "add", 1000.0, 6.426667, 0, 5, "obj-111", "function", "domain", 1000.0, 6, "obj-111", "function", "range", 1.0, 12.0, 5, "obj-111", "function", "mode", 0, 5, "obj-112", "number", "int", 6, 5, "obj-116", "flonum", "float", 3.4, 5, "obj-114", "number", "int", 0, 5, "obj-8", "flonum", "float", 0.805, 5, "obj-9", "flonum", "float", 0.68, 5, "obj-129", "flonum", "float", 0.01 ]
						}
, 						{
							"number" : 2,
							"data" : [ 4, "obj-2", "function", "clear", 7, "obj-2", "function", "add", 0.0, -60.0, 0, 7, "obj-2", "function", "add", 35.461102, -1.333315, 0, 7, "obj-2", "function", "add", 115.248337, -18.933315, 0, 7, "obj-2", "function", "add", 184.397278, -1.333315, 0, 7, "obj-2", "function", "add", 269.503662, -15.413315, 0, 7, "obj-2", "function", "add", 333.333435, 0.426685, 0, 7, "obj-2", "function", "add", 391.844086, -9.253315, 0, 7, "obj-2", "function", "add", 460.993011, -0.453315, 0, 7, "obj-2", "function", "add", 535.461121, -11.893315, 0, 7, "obj-2", "function", "add", 625.886658, -21.573315, 0, 7, "obj-2", "function", "add", 663.120667, -7.493315, 0, 7, "obj-2", "function", "add", 764.184509, -20.693316, 0, 7, "obj-2", "function", "add", 828.014282, -3.093315, 0, 7, "obj-2", "function", "add", 907.801514, -20.693316, 0, 7, "obj-2", "function", "add", 998.227051, -0.453315, 0, 7, "obj-2", "function", "add", 1000.0, -60.0, 0, 5, "obj-2", "function", "domain", 1000.0, 6, "obj-2", "function", "range", -60.0, 6.0, 5, "obj-2", "function", "mode", 0, 4, "obj-12", "function", "clear", 7, "obj-12", "function", "add", 0.0, 611.422791, 0, 7, "obj-12", "function", "add", 140.070969, 372.622772, 0, 7, "obj-12", "function", "add", 304.9646, 293.022766, 0, 7, "obj-12", "function", "add", 485.815643, 425.689423, 0, 7, "obj-12", "function", "add", 687.943298, 319.556091, 0, 7, "obj-12", "function", "add", 826.241211, 399.156097, 0, 7, "obj-12", "function", "add", 1000.0, 266.489441, 0, 5, "obj-12", "function", "domain", 1000.0, 6, "obj-12", "function", "range", 10.0, 2000.0, 5, "obj-12", "function", "mode", 0, 4, "obj-21", "function", "clear", 7, "obj-21", "function", "add", 0.0, -12.69332, 0, 7, "obj-21", "function", "add", 505.319153, -22.933319, 0, 7, "obj-21", "function", "add", 1000.0, -9.49332, 0, 5, "obj-21", "function", "domain", 1000.0, 6, "obj-21", "function", "range", -24.0, 24.0, 5, "obj-21", "function", "mode", 0, 4, "obj-28", "function", "clear", 7, "obj-28", "function", "add", 0.0, 83.555664, 0, 7, "obj-28", "function", "add", 423.758636, 62.222332, 0, 7, "obj-28", "function", "add", 1000.0, 94.222328, 0, 5, "obj-28", "function", "domain", 1000.0, 6, "obj-28", "function", "range", 0.0, 400.0, 5, "obj-28", "function", "mode", 0, 4, "obj-32", "function", "clear", 7, "obj-32", "function", "add", 0.0, 1457.77832, 0, 7, "obj-32", "function", "add", 198.581131, 1484.444946, 0, 7, "obj-32", "function", "add", 661.347107, 1431.111694, 0, 7, "obj-32", "function", "add", 1000.0, 1671.111694, 0, 5, "obj-32", "function", "domain", 1000.0, 6, "obj-32", "function", "range", 0.0, 2000.0, 5, "obj-32", "function", "mode", 0, 4, "obj-42", "function", "clear", 7, "obj-42", "function", "add", 0.0, 0.277333, 0, 7, "obj-42", "function", "add", 58.510639, 0.157333, 0, 7, "obj-42", "function", "add", 127.659576, 0.181333, 0, 7, "obj-42", "function", "add", 175.531921, 0.093333, 0, 7, "obj-42", "function", "add", 218.085114, 0.197333, 0, 7, "obj-42", "function", "add", 271.276581, 0.125333, 0, 7, "obj-42", "function", "add", 319.148926, 0.061333, 0, 7, "obj-42", "function", "add", 388.297882, 0.197333, 0, 7, "obj-42", "function", "add", 452.127655, 0.157333, 0, 7, "obj-42", "function", "add", 574.468079, 0.037333, 0, 7, "obj-42", "function", "add", 702.127686, 0.125333, 0, 7, "obj-42", "function", "add", 735.815857, 0.338667, 0, 7, "obj-42", "function", "add", 778.369019, 0.221333, 0, 7, "obj-42", "function", "add", 898.936157, 0.149333, 0, 7, "obj-42", "function", "add", 952.127686, 0.037333, 0, 7, "obj-42", "function", "add", 1000.0, 0.333333, 0, 5, "obj-42", "function", "domain", 1000.0, 6, "obj-42", "function", "range", 0.0, 0.6, 5, "obj-42", "function", "mode", 0, 4, "obj-58", "function", "clear", 7, "obj-58", "function", "add", 10.638298, 0.542222, 0, 7, "obj-58", "function", "add", 42.553192, 0.888889, 0, 7, "obj-58", "function", "add", 111.702126, 0.582222, 0, 7, "obj-58", "function", "add", 191.489365, 0.355556, 0, 7, "obj-58", "function", "add", 292.553192, 0.728889, 0, 7, "obj-58", "function", "add", 351.063843, 0.355556, 0, 7, "obj-58", "function", "add", 420.212769, 0.715556, 0, 7, "obj-58", "function", "add", 526.595764, 0.742222, 0, 7, "obj-58", "function", "add", 595.74469, 0.568889, 0, 7, "obj-58", "function", "add", 718.085083, 0.688889, 0, 7, "obj-58", "function", "add", 755.319153, 0.862222, 0, 7, "obj-58", "function", "add", 847.518005, 0.6, 0, 7, "obj-58", "function", "add", 1000.0, 1.0, 0, 5, "obj-58", "function", "domain", 1000.0, 6, "obj-58", "function", "range", 0.0, 1.0, 5, "obj-58", "function", "mode", 0, 5, "obj-60", "flonum", "float", 120.0, 5, "obj-83", "flonum", "float", 0.095667, 5, "obj-103", "number", "int", 90, 5, "obj-107", "number", "int", 0, 4, "obj-111", "function", "clear", 7, "obj-111", "function", "add", 0.0, 4.08, 0, 7, "obj-111", "function", "add", 198.581207, 8.968889, 0, 7, "obj-111", "function", "add", 404.254974, 5.057778, 0, 7, "obj-111", "function", "add", 574.467773, 9.36, 0, 7, "obj-111", "function", "add", 666.666443, 4.471112, 0, 7, "obj-111", "function", "add", 822.694702, 10.142222, 0, 7, "obj-111", "function", "add", 1000.0, 6.426667, 0, 5, "obj-111", "function", "domain", 1000.0, 6, "obj-111", "function", "range", 1.0, 12.0, 5, "obj-111", "function", "mode", 0, 5, "obj-112", "number", "int", 6, 5, "obj-116", "flonum", "float", 1.2, 5, "obj-114", "number", "int", 0, 5, "obj-8", "flonum", "float", 0.805, 5, "obj-9", "flonum", "float", 0.68, 5, "obj-129", "flonum", "float", 0.03 ]
						}
, 						{
							"number" : 3,
							"data" : [ 4, "obj-2", "function", "clear", 7, "obj-2", "function", "add", 0.0, -60.0, 0, 7, "obj-2", "function", "add", 35.461102, -1.333315, 0, 7, "obj-2", "function", "add", 184.397278, -1.333315, 0, 7, "obj-2", "function", "add", 333.333435, 0.426685, 0, 7, "obj-2", "function", "add", 460.993011, -0.453315, 0, 7, "obj-2", "function", "add", 828.014282, -3.093315, 0, 7, "obj-2", "function", "add", 921.985962, -2.799991, 0, 7, "obj-2", "function", "add", 1000.0, -60.0, 0, 5, "obj-2", "function", "domain", 1000.0, 6, "obj-2", "function", "range", -60.0, 6.0, 5, "obj-2", "function", "mode", 0, 4, "obj-12", "function", "clear", 7, "obj-12", "function", "add", 0.0, 611.422791, 0, 7, "obj-12", "function", "add", 304.9646, 1593.155762, 0, 7, "obj-12", "function", "add", 523.049744, 885.60022, 0, 7, "obj-12", "function", "add", 813.829895, 1557.778076, 0, 7, "obj-12", "function", "add", 1000.0, 266.489441, 0, 5, "obj-12", "function", "domain", 1000.0, 6, "obj-12", "function", "range", 10.0, 2000.0, 5, "obj-12", "function", "mode", 0, 4, "obj-21", "function", "clear", 7, "obj-21", "function", "add", 0.0, 2.240006, 0, 7, "obj-21", "function", "add", 500.000122, -1.173327, 0, 7, "obj-21", "function", "add", 1000.0, -3.733327, 0, 5, "obj-21", "function", "domain", 1000.0, 6, "obj-21", "function", "range", -24.0, 24.0, 5, "obj-21", "function", "mode", 0, 4, "obj-28", "function", "clear", 7, "obj-28", "function", "add", 1.772966, 400.0, 0, 7, "obj-28", "function", "add", 1000.0, 360.888947, 0, 5, "obj-28", "function", "domain", 1000.0, 6, "obj-28", "function", "range", 0.0, 400.0, 5, "obj-28", "function", "mode", 0, 4, "obj-32", "function", "clear", 7, "obj-32", "function", "add", 0.0, 595.555786, 0, 7, "obj-32", "function", "add", 198.581314, 631.111328, 0, 7, "obj-32", "function", "add", 666.666443, 631.111328, 0, 7, "obj-32", "function", "add", 1000.0, 737.778015, 0, 5, "obj-32", "function", "domain", 1000.0, 6, "obj-32", "function", "range", 0.0, 2000.0, 5, "obj-32", "function", "mode", 0, 4, "obj-42", "function", "clear", 7, "obj-42", "function", "add", 0.0, 0.136, 0, 7, "obj-42", "function", "add", 1000.0, 0.232, 0, 5, "obj-42", "function", "domain", 1000.0, 6, "obj-42", "function", "range", 0.0, 0.6, 5, "obj-42", "function", "mode", 0, 4, "obj-58", "function", "clear", 7, "obj-58", "function", "add", 0.0, 0.511111, 0, 7, "obj-58", "function", "add", 1000.0, 0.848889, 0, 5, "obj-58", "function", "domain", 1000.0, 6, "obj-58", "function", "range", 0.0, 1.0, 5, "obj-58", "function", "mode", 0, 5, "obj-60", "flonum", "float", 120.0, 5, "obj-83", "flonum", "float", 0.7325, 5, "obj-103", "number", "int", 10, 5, "obj-107", "number", "int", 3, 4, "obj-111", "function", "clear", 7, "obj-111", "function", "add", 0.0, 595.555786, 0, 7, "obj-111", "function", "add", 198.581314, 631.111328, 0, 7, "obj-111", "function", "add", 666.666443, 631.111328, 0, 7, "obj-111", "function", "add", 1000.0, 737.778015, 0, 5, "obj-111", "function", "domain", 1000.0, 6, "obj-111", "function", "range", 0.0, 2000.0, 5, "obj-111", "function", "mode", 0, 5, "obj-112", "number", "int", 652, 5, "obj-116", "flonum", "float", 8.2, 5, "obj-114", "number", "int", 0, 5, "obj-8", "flonum", "float", 0.805, 5, "obj-9", "flonum", "float", 0.68, 4, "<invalid>", "function", "clear", 7, "<invalid>", "function", "add", 0.0, 0.0, 0, 7, "<invalid>", "function", "add", 69.148933, 0.133333, 0, 7, "<invalid>", "function", "add", 143.61702, 0.146667, 0, 7, "<invalid>", "function", "add", 212.765961, 0.146667, 0, 7, "<invalid>", "function", "add", 250.0, 0.146667, 0, 7, "<invalid>", "function", "add", 345.74469, 0.24, 0, 7, "<invalid>", "function", "add", 409.574463, 0.56, 0, 7, "<invalid>", "function", "add", 452.127655, 0.373333, 0, 7, "<invalid>", "function", "add", 553.191467, 0.24, 0, 7, "<invalid>", "function", "add", 627.659546, 0.413333, 0, 7, "<invalid>", "function", "add", 712.76593, 0.4, 0, 7, "<invalid>", "function", "add", 781.915039, 0.392, 0, 7, "<invalid>", "function", "add", 862.766113, 0.413333, 0, 7, "<invalid>", "function", "add", 926.595886, 0.509333, 0, 7, "<invalid>", "function", "add", 977.659729, 0.669333, 0, 7, "<invalid>", "function", "add", 1000.0, 0.0, 0, 5, "<invalid>", "function", "domain", 1000.0, 6, "<invalid>", "function", "range", 0.0, 1.0, 5, "<invalid>", "function", "mode", 0, 4, "<invalid>", "function", "clear", 7, "<invalid>", "function", "add", 0.0, 0.650667, 0, 7, "<invalid>", "function", "add", 106.38298, 0.649333, 0, 7, "<invalid>", "function", "add", 228.723404, 0.637333, 0, 7, "<invalid>", "function", "add", 297.872345, 0.641333, 0, 7, "<invalid>", "function", "add", 351.063843, 0.618667, 0, 7, "<invalid>", "function", "add", 425.531921, 0.632, 0, 7, "<invalid>", "function", "add", 485.106567, 0.625333, 0, 7, "<invalid>", "function", "add", 569.148926, 0.621333, 2, 7, "<invalid>", "function", "add", 643.617004, 0.645333, 0, 7, "<invalid>", "function", "add", 728.723389, 0.654667, 0, 7, "<invalid>", "function", "add", 781.914917, 0.64, 0, 7, "<invalid>", "function", "add", 906.383179, 0.665333, 0, 7, "<invalid>", "function", "add", 953.19165, 0.634933, 0, 7, "<invalid>", "function", "add", 1000.0, 0.6, 0, 5, "<invalid>", "function", "domain", 1000.0, 6, "<invalid>", "function", "range", 0.6, 0.7, 5, "<invalid>", "function", "mode", 0, 5, "<invalid>", "flonum", "float", 90.0, 5, "<invalid>", "flonum", "float", 1.0, 4, "<invalid>", "function", "clear", 7, "<invalid>", "function", "add", 0.0, 0.01, 0, 7, "<invalid>", "function", "add", 117.021278, 0.112844, 0, 7, "<invalid>", "function", "add", 175.531921, 0.043511, 0, 7, "<invalid>", "function", "add", 329.787231, 0.147511, 0, 7, "<invalid>", "function", "add", 420.212769, 0.071244, 0, 7, "<invalid>", "function", "add", 526.595764, 0.154444, 0, 7, "<invalid>", "function", "add", 659.574463, 0.185644, 0, 7, "<invalid>", "function", "add", 797.872314, 0.248044, 0, 7, "<invalid>", "function", "add", 925.531921, 0.244578, 0, 7, "<invalid>", "function", "add", 1000.0, 0.092044, 0, 5, "<invalid>", "function", "domain", 1000.0, 6, "<invalid>", "function", "range", 0.01, 0.27, 5, "<invalid>", "function", "mode", 0, 5, "<invalid>", "flonum", "float", 0.092044, 5, "<invalid>", "flonum", "float", 0.0, 5, "<invalid>", "flonum", "float", 0.6, 4, "<invalid>", "function", "clear", 7, "<invalid>", "function", "add", 0.0, -23.920067, 0, 7, "<invalid>", "function", "add", 153.369247, -6.320065, 0, 7, "<invalid>", "function", "add", 174.645615, -2.213369, 0, 7, "<invalid>", "function", "add", 227.837097, -3.093369, 0, 7, "<invalid>", "function", "add", 252.660034, -2.800065, 0, 7, "<invalid>", "function", "add", 334.220062, -4.853369, 0, 7, "<invalid>", "function", "add", 376.773254, -2.213369, 0, 7, "<invalid>", "function", "add", 445.922211, -4.853369, 0, 7, "<invalid>", "function", "add", 499.113678, -3.093369, 0, 7, "<invalid>", "function", "add", 531.028564, -3.093369, 0, 7, "<invalid>", "function", "add", 632.092407, -4.853369, 0, 7, "<invalid>", "function", "add", 743.794556, -6.613369, 0, 7, "<invalid>", "function", "add", 945.92218, -7.493369, 0, 7, "<invalid>", "function", "add", 1000.0, -14.533369, 0, 5, "<invalid>", "function", "domain", 1000.0, 6, "<invalid>", "function", "range", -60.0, 6.0, 5, "<invalid>", "function", "mode", 0, 4, "<invalid>", "function", "clear", 7, "<invalid>", "function", "add", 0.0, 0.0, 2, 7, "<invalid>", "function", "add", 113.474716, 0.0, 0, 7, "<invalid>", "function", "add", 131.914337, 2.813341, 0, 7, "<invalid>", "function", "add", 212.765305, 2.053345, 0, 7, "<invalid>", "function", "add", 308.509979, 5.253345, 0, 7, "<invalid>", "function", "add", 378.722839, 7.293341, 0, 7, "<invalid>", "function", "add", 418.43927, 3.933343, 0, 7, "<invalid>", "function", "add", 489.361053, 2.453345, 0, 7, "<invalid>", "function", "add", 537.233398, 5.853345, 0, 7, "<invalid>", "function", "add", 590.424866, 2.653345, 0, 7, "<invalid>", "function", "add", 622.339783, 7.253345, 0, 7, "<invalid>", "function", "add", 686.169556, 8.453345, 0, 7, "<invalid>", "function", "add", 755.318481, 6.653345, 0, 7, "<invalid>", "function", "add", 909.573792, 4.253345, 0, 7, "<invalid>", "function", "add", 1000.0, 0.0, 0, 5, "<invalid>", "function", "domain", 1000.0, 6, "<invalid>", "function", "range", 0.0, 15.0, 5, "<invalid>", "function", "mode", 0, 5, "<invalid>", "number", "int", 0, 4, "<invalid>", "function", "clear", 7, "<invalid>", "function", "add", 0.0, 248.801224, 0, 7, "<invalid>", "function", "add", 97.517662, 178.04567, 0, 7, "<invalid>", "function", "add", 279.077881, 319.557251, 0, 7, "<invalid>", "function", "add", 354.609711, 494.677063, 0, 7, "<invalid>", "function", "add", 455.673553, 945.743713, 0, 7, "<invalid>", "function", "add", 583.33313, 1131.477051, 0, 7, "<invalid>", "function", "add", 657.801208, 892.677063, 0, 7, "<invalid>", "function", "add", 737.58844, 945.743713, 0, 7, "<invalid>", "function", "add", 854.609741, 627.34375, 0, 7, "<invalid>", "function", "add", 1000.0, 319.556793, 0, 5, "<invalid>", "function", "domain", 1000.0, 6, "<invalid>", "function", "range", 10.0, 2000.0, 5, "<invalid>", "function", "mode", 0, 5, "<invalid>", "number", "int", 3, 5, "<invalid>", "flonum", "float", 0.051, 5, "<invalid>", "flonum", "float", 0.5, 5, "<invalid>", "flonum", "float", 0.074 ]
						}
, 						{
							"number" : 4,
							"data" : [ 4, "obj-2", "function", "clear", 7, "obj-2", "function", "add", 0.0, -60.0, 0, 7, "obj-2", "function", "add", 3.546208, -18.053314, 0, 7, "obj-2", "function", "add", 83.333443, 0.426685, 0, 7, "obj-2", "function", "add", 234.042679, -11.013325, 0, 7, "obj-2", "function", "add", 386.524933, 2.186685, 0, 7, "obj-2", "function", "add", 595.744812, 0.720009, 0, 7, "obj-2", "function", "add", 695.035583, -15.706658, 0, 7, "obj-2", "function", "add", 780.141968, -1.626658, 0, 7, "obj-2", "function", "add", 936.170349, -2.799991, 0, 7, "obj-2", "function", "add", 1000.0, -60.0, 0, 5, "obj-2", "function", "domain", 1000.0, 6, "obj-2", "function", "range", -60.0, 6.0, 5, "obj-2", "function", "mode", 0, 4, "obj-12", "function", "clear", 7, "obj-12", "function", "add", 0.0, 1991.156128, 0, 7, "obj-12", "function", "add", 189.716415, 1699.289185, 0, 7, "obj-12", "function", "add", 452.127777, 1840.800293, 0, 7, "obj-12", "function", "add", 750.000122, 1345.511353, 0, 7, "obj-12", "function", "add", 1000.0, 1663.911377, 0, 5, "obj-12", "function", "domain", 1000.0, 6, "obj-12", "function", "range", 10.0, 2000.0, 5, "obj-12", "function", "mode", 0, 4, "obj-21", "function", "clear", 7, "obj-21", "function", "add", 0.0, -0.53332, 0, 7, "obj-21", "function", "add", 514.184509, 12.480006, 0, 7, "obj-21", "function", "add", 1000.0, -1.81332, 0, 5, "obj-21", "function", "domain", 1000.0, 6, "obj-21", "function", "range", -24.0, 24.0, 5, "obj-21", "function", "mode", 0, 4, "obj-28", "function", "clear", 7, "obj-28", "function", "add", 0.0, 291.555664, 0, 7, "obj-28", "function", "add", 498.226868, 375.111176, 0, 7, "obj-28", "function", "add", 1000.0, 296.889008, 0, 5, "obj-28", "function", "domain", 1000.0, 6, "obj-28", "function", "range", 0.0, 400.0, 5, "obj-28", "function", "mode", 0, 4, "obj-32", "function", "clear", 7, "obj-32", "function", "add", 0.0, 231.111649, 0, 7, "obj-32", "function", "add", 0.0, 275.555786, 0, 5, "obj-32", "function", "domain", 1000.0, 6, "obj-32", "function", "range", 0.0, 2000.0, 5, "obj-32", "function", "mode", 0, 4, "obj-42", "function", "clear", 7, "obj-42", "function", "add", 0.0, 0.277333, 0, 7, "obj-42", "function", "add", 58.510639, 0.157333, 0, 7, "obj-42", "function", "add", 127.659576, 0.181333, 0, 7, "obj-42", "function", "add", 175.531921, 0.093333, 0, 7, "obj-42", "function", "add", 218.085114, 0.197333, 0, 7, "obj-42", "function", "add", 271.276581, 0.125333, 0, 7, "obj-42", "function", "add", 319.148926, 0.061333, 0, 7, "obj-42", "function", "add", 388.297882, 0.197333, 0, 7, "obj-42", "function", "add", 452.127655, 0.157333, 0, 7, "obj-42", "function", "add", 574.468079, 0.037333, 0, 7, "obj-42", "function", "add", 702.127686, 0.125333, 0, 7, "obj-42", "function", "add", 787.23407, 0.069333, 0, 7, "obj-42", "function", "add", 851.063843, 0.125333, 0, 7, "obj-42", "function", "add", 898.936157, 0.149333, 0, 7, "obj-42", "function", "add", 952.127686, 0.037333, 0, 7, "obj-42", "function", "add", 1000.0, 0.333333, 0, 5, "obj-42", "function", "domain", 1000.0, 6, "obj-42", "function", "range", 0.0, 0.6, 5, "obj-42", "function", "mode", 0, 4, "obj-58", "function", "clear", 7, "obj-58", "function", "add", 10.638298, 0.542222, 0, 7, "obj-58", "function", "add", 42.553192, 0.888889, 0, 7, "obj-58", "function", "add", 111.702126, 0.582222, 0, 7, "obj-58", "function", "add", 191.489365, 0.355556, 0, 7, "obj-58", "function", "add", 292.553192, 0.728889, 0, 7, "obj-58", "function", "add", 351.063843, 0.355556, 0, 7, "obj-58", "function", "add", 420.212769, 0.715556, 0, 7, "obj-58", "function", "add", 526.595764, 0.742222, 0, 7, "obj-58", "function", "add", 595.74469, 0.568889, 0, 7, "obj-58", "function", "add", 718.085083, 0.462222, 0, 7, "obj-58", "function", "add", 755.319153, 0.862222, 0, 7, "obj-58", "function", "add", 1000.0, 1.0, 0, 5, "obj-58", "function", "domain", 1000.0, 6, "obj-58", "function", "range", 0.0, 1.0, 5, "obj-58", "function", "mode", 0, 5, "obj-60", "flonum", "float", 120.0, 5, "obj-83", "flonum", "float", 0.0755, 5, "obj-103", "number", "int", 9, 5, "obj-107", "number", "int", 4, 4, "obj-111", "function", "clear", 7, "obj-111", "function", "add", 0.0, 4.08, 0, 7, "obj-111", "function", "add", 198.581207, 8.968889, 0, 7, "obj-111", "function", "add", 404.254974, 5.057778, 0, 7, "obj-111", "function", "add", 574.467773, 9.36, 0, 7, "obj-111", "function", "add", 666.666443, 4.471112, 0, 7, "obj-111", "function", "add", 822.694702, 10.142222, 0, 7, "obj-111", "function", "add", 1000.0, 6.426667, 0, 5, "obj-111", "function", "domain", 1000.0, 6, "obj-111", "function", "range", 1.0, 12.0, 5, "obj-111", "function", "mode", 0, 5, "obj-112", "number", "int", 5, 5, "obj-116", "flonum", "float", 0.0, 5, "obj-114", "number", "int", 0, 5, "obj-8", "flonum", "float", 0.805, 5, "obj-9", "flonum", "float", 0.68, 5, "obj-129", "flonum", "float", 0.035 ]
						}
 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 451.666626, 75.0, 49.0, 22.0 ],
					"style" : "",
					"text" : "* 1000."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 451.666626, 11.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.305882, 0.717647, 0.67451, 1.0 ],
					"id" : "obj-76",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 890.166748, 828.333252, 137.0, 22.0 ],
					"style" : "",
					"text" : "udpsend localhost 8480"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-75",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1088.666748, 151.0, 75.0, 22.0 ],
					"style" : "",
					"text" : "range 0 500"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 832.333374, 151.0, 84.0, 22.0 ],
					"style" : "",
					"text" : "setrange 0 80"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.572549, 0.27451, 0.027451, 1.0 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"gradient" : 1,
					"id" : "obj-73",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 576.0, 151.0, 79.0, 22.0 ],
					"style" : "",
					"text" : "range -24 24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1088.666748, 439.499969, 97.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/rate $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1345.0, 434.333252, 49.0, 22.0 ],
					"style" : "",
					"text" : "pack f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1345.0, 476.333252, 112.0, 22.0 ],
					"style" : "",
					"text" : "/catart/select $1 $2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 832.333374, 439.499969, 106.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/xfade $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 576.0, 439.499969, 145.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/transposition $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 319.666656, 439.499969, 120.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/duration $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 63.333313, 439.499969, 99.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/gain $1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 38.010217,
					"format" : 6,
					"id" : "obj-60",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 451.666626, 45.0, 95.0, 51.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1468.666626, 445.079742, 157.0, 51.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-57",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1690.0, 282.715759, 78.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 999.333313, 78.0, 24.0 ],
					"style" : "",
					"text" : "position y",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 0.453333, 0, 42.553192, 0.888889, 0, 111.702126, 0.582222, 0, 191.489365, 0.355556, 0, 292.553192, 0.728889, 0, 351.063843, 0.355556, 0, 420.212769, 0.715556, 0, 526.595764, 0.742222, 0, 595.74469, 0.568889, 0, 718.085083, 0.462222, 0, 755.319153, 0.862222, 0, 1000.0, 1.0, 0 ],
					"bgcolor" : [ 0.560317, 0.570942, 0.601066, 0.51 ],
					"id" : "obj-58",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1568.0, 313.666687, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 999.333313, 1200.0, 150.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 451.666626, 105.0, 67.0, 22.0 ],
					"style" : "",
					"text" : "0, 1000 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 451.666626, 147.0, 42.0, 22.0 ],
					"style" : "",
					"text" : "ej.line"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 11.0, 282.715759, 37.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-39",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1467.0, 282.715759, 78.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 847.333313, 78.0, 24.0 ],
					"style" : "",
					"text" : "position x",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 0.462222, 0, 58.510639, 0.262222, 0, 127.659576, 0.302222, 0, 175.531921, 0.155556, 0, 218.085114, 0.328889, 0, 271.276581, 0.208889, 0, 319.148926, 0.102222, 0, 388.297882, 0.328889, 0, 452.127655, 0.262222, 0, 574.468079, 0.062222, 0, 702.127686, 0.208889, 0, 787.23407, 0.115556, 0, 851.063843, 0.208889, 0, 898.936157, 0.248889, 0, 952.127686, 0.062222, 0, 1000.0, 0.555556, 0 ],
					"bgcolor" : [ 0.45098, 0.513725, 0.521569, 0.52 ],
					"id" : "obj-42",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1345.0, 313.666687, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 847.333313, 1200.0, 150.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-29",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1236.333374, 282.715759, 41.000004, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 695.333313, 41.000004, 24.0 ],
					"style" : "",
					"text" : "rate",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 231.111649, 0, 523.049194, 284.444977, 0, 804.964111, 631.111633, 0, 1000.0, 1671.111694, 0 ],
					"bgcolor" : [ 0.282353, 0.309804, 0.266667, 0.5 ],
					"id" : "obj-32",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1088.666748, 313.666687, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 695.333313, 1200.0, 150.0 ],
					"range" : [ 0.0, 2000.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 979.0, 282.715759, 41.000004, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 543.333374, 41.000004, 24.0 ],
					"style" : "",
					"text" : "fade",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 291.555664, 0, 1000.0, 296.889008, 0 ],
					"bgcolor" : [ 0.498039, 0.462745, 0.376471, 0.54 ],
					"id" : "obj-28",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 832.333374, 313.666687, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 543.333374, 1200.0, 150.0 ],
					"range" : [ 0.0, 400.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-18",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 676.0, 282.715759, 100.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 391.333344, 100.0, 24.0 ],
					"style" : "",
					"text" : "transposition",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, -0.53332, 0, 161.616167, 18.751999, 0, 393.939406, -16.064002, 0, 1000.0, -1.81332, 0 ],
					"bgcolor" : [ 0.375889, 0.380647, 0.363084, 0.56 ],
					"id" : "obj-21",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 576.0, 313.666687, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 391.333344, 1200.0, 150.0 ],
					"range" : [ -24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-7",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 451.666626, 282.715759, 68.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 239.333328, 68.0, 24.0 ],
					"style" : "",
					"text" : "duration",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 1991.156128, 0, 124.113525, 1513.556152, 0, 326.24118, 876.756104, 0, 400.709259, 1142.089478, 0, 687.943298, 770.622742, 0, 884.751831, 1221.689453, 0, 1000.0, 1858.48938, 0 ],
					"bgcolor" : [ 0.37684, 0.398617, 0.461834, 0.5 ],
					"id" : "obj-12",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 319.666656, 313.666687, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 239.333328, 1200.0, 150.0 ],
					"range" : [ 10.0, 2000.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 5850.0, 3359.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 15.867916,
					"id" : "obj-4",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 209.999969, 282.715759, 41.000004, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 87.333328, 41.000004, 24.0 ],
					"style" : "",
					"text" : "gain",
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, -60.0, 0, 3.546208, -14.23998, 0, 83.333443, 5.92002, 0, 386.524933, 7.84002, 0, 535.461121, -7.51998, 0, 663.120667, -42.079979, 0, 730.496582, -2.71999, 0, 921.985962, -2.71999, 0, 1000.0, -60.0, 0 ],
					"bgcolor" : [ 0.462745, 0.470588, 0.490196, 0.5 ],
					"id" : "obj-2",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 63.333313, 313.666687, 200.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 87.333328, 1200.0, 150.0 ],
					"range" : [ -60.0, 12.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 869.500061, 531.999878, 235.0, 22.0 ],
					"style" : "",
					"text" : "/corpus1/radius 0.03, /corpus1/pan_std 26"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"knobcolor" : [ 0.317647, 0.788235, 0.976471, 0.59 ],
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"orientation" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 71.666656, 28.0, 238.0, 44.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 14.0, 87.333374, 1200.0, 1214.0 ],
					"size" : 1000.0,
					"style" : "velvet"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 1 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 1 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"order" : 1,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-41", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 1 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"order" : 5,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"order" : 6,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"order" : 4,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 3,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"order" : 2,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 1,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"order" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"order" : 0,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"order" : 7,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"order" : 9,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"order" : 5,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 4,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"order" : 3,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 2,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"order" : 8,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"order" : 1,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"order" : 6,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 1 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 1 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 1 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 1,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 0,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 1 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 1 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"order" : 1,
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"order" : 0,
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 1 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "ej.line.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/ejies/patchers",
				"patcherrelativepath" : "../../Documents/Max 7/Packages/ejies/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
