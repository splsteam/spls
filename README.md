# README #

### www computer music design tools ###

version 1 - 26-02-2018

www computer design tools is a collection of Max patches developed for the realisation of music and sounds for the interactive web-based soundwork "Web Wall Whispers" (www).

Description
The aim of these tools is to build up instruments and digital sound processing engines, focused on sounds manipulation and recombination (such as concatenative and granular synthesis and hybridization), driven by custom, suitably scores, in order to write and store parameters for these instruments, allowing to replicate  determinate sound gestures.

“WWW Computer Music Design Tools” includes the following patches:
cataRT_score.maxpath
vocoder.maxpat
vocoder_multi_preset_interp.maxpat
granulator.maxpat
hybridator.maxpat
binaural_concat.maxpat

### Requirements ###

* Hardware used
MacBook Pro 15" 2016, 2.7 GHz Intel Core i7, 16 GB 2133 MHz LPDDR3

* OS required
Mac OSX 10.7 (or later) / Windows 7 (or later)

* Software requirements:
Max7, version 3.4.5 or higher - https://cycling74.com/downloads
cataRT app standalone, version 1.6.0 - http://forumnet.ircam.fr/product/catart-standalone-en/

Max packages and externals needed:

bach, v. 0.8.0.1 beta (available on Max Package Manager, free)

ejies, v.  3.2.4 (available on Max Package Manager, free)

MuBu for Max, v. 1.9.4 (available on Max Package Manager, free)

Spat, v. 4 (available on http://forumnet.ircam.fr/)

yin~, part of Max Sound Box, (available on http://forumnet.ircam.fr/)


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
